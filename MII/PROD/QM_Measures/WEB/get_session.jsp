<%@page import = "java.util.Date" session="true"%>
<HTML> 
    <HEAD>
        <TITLE>Session in JSP (Codemiles)</TITLE>
    </HEAD> 

    <BODY>
      
        <h1>Session in JSP</h1>
        Your session ID is :  <%=session.getId()%>
        <br>
        Session created at  <%=new Date(session.getCreationTime())%>
        <br>
        Last time of activity <%=new Date(session.getLastAccessedTime())%>
        <br>
         
    </BODY> 
</HTML>