/* jshint -W117, -W098 */ 
// definizione tabelle dati
//**  datatables.js
//*******************************************
//**  Autore: Luca Adanti - IT-Link
//**
//**  Ver: 2.4.2
//**
//*******************************************
//** Ulteriori interventi di:
//** Giancarlo Pisoni
//** Bruno Rosati
//*******************************************

//Inizializza le tabelle dati caricate via XML ( e non con json)
function initDT (  ) {

	// Tabella lista misure in popup
	$('#wsmis').dataTable( {
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bDestroy": false,
		"bRetrieve" : true,
		"bProcessing": false,
		"bServerSide": false,
		"bJQueryUI": true,
		"bSort": true,
		"bSortClasses": false,
		"bPaginate": false,
		"bStateSave": false,
		"bAutoWidth": false,
		"sScrollX": "100%",
		"sScrollY": "350",
		"bScrollCollapse": false,
		"aaSorting": [[ 0, "desc" ]],
		"aoColumnDefs": [ { "bVisible": false,  "aTargets": [] },
										 {"bSortable": false, "aTargets": [ 0 ] },
										 {"sClass": "td-center", "aTargets": [ 0 , 3, 4]},
										 {"sType": "numeric", "aTargets": [ 0 ]},
										 {"sSortDataType": "dom-text", "aTargets": [ 0,1,2,3,4 ] },
										 {"bSearchable": true, "aTargets": [ 4 ]}],
		"oLanguage": {"sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
	} );

	// Tabella lista misure in popup
	$('#wsFile').dataTable( {
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"bDestroy": false,
		"bRetrieve" : true,
		"bProcessing": false,
		"bServerSide": false,
		"bJQueryUI": true,
		"bSort": true,
		"bSortClasses": false,
		"bPaginate": false,
		"bStateSave": false,
		"bAutoWidth": false,
		"sScrollX": "100%",
		"sScrollY": "250",
		"bScrollCollapse": false,
		"aaSorting": [[ 0, "asc" ]],
		"aoColumnDefs": [ { "bVisible": false,  "aTargets": [3 ] },
										 {"sType": "numeric", "aTargets": [ 0 ]},],
		"oLanguage": {"sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
	} );

	// Tabella lista caratteristiche da piano di lavoro

	$('#chars').bind('filter', function () { charsFiltered(); }).dataTable( {
		"iDisplayLength": 20,
		"aLengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Tutti"]],
		"bFilter": true,
		"bInfo": true,
		"bDestroy": false,
		"bProcessing": true,
		"bSortClasses": false,
		"bRetrieve" : true,
		"bServerSide": false,
		"bJQueryUI": true,
		"bSort": true,
		"bPaginate": true,
		"bStateSave": false,
		"sScrollX": "100%",
		"sScrollY": "200",
		"bScrollCollapse": false,
		"bAutoWidth": false,
		"colReorder": true,
		/*"sDom":  'Rlfrtip' ,*/
		//	  "aoColumnDefs": [ { "bVisible": false,  "aTargets": [2,3, 4, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 23,27 ] },   //  [3956] User: giancarlo  Date: 01/12/2014
		//	  "aoColumnDefs": [ { "bVisible": false,  "aTargets": [2,3, 4, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 23, 24, 27 ] },   //  [3956] User: giancarlo  Date: 01/12/2014
		"aoColumnDefs": [ { "bVisible": false,  "aTargets": [2,3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 23, 24, 25, 27 ] },   //  [3956] User: giancarlo  Date: 01/12/2014
										 {"bSortable": false, "aTargets": [ 1, 2, 4 ] },
										 {"sClass": "td-center", "aTargets": [ 5 , 7]},
										 {"sType": "numeric", "aTargets": [ 0 ]},
										 {"sSortDataType": "dom-text", "aTargets": [ 0 ,1, 2, 4 ] },
										 {"bSearchable": true, "aTargets": [17, 26 ]}],
		"oLanguage": {"sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"		  },
	} );

	//$('#chars').dataTable( { 'C<"clear">lfrtip' } );



	// Tabella lista risultati caratteristica
	$('#charDocs').dataTable( {
		"iDisplayLength": -1,
		"aLengthMenu": [[5, 10, -1], [5, 10, "Tutti"]],
		"bFilter": true,
		"bInfo": true,
		"bDestroy": false,
		"bProcessing": true,
		"bRetrieve" : true,
		"bServerSide": false,
		"bJQueryUI": true,
		"bSort": true,
		"bPaginate": true,
		"bStateSave": false,
		"sScrollY": "150",
		"bScrollCollapse": true,
		"bAutoWidth": true,
		"aoColumnDefs": [ { "bVisible": false,  "aTargets": [2,3,4,5 ] }],
		"oLanguage": {"sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"		  }
	} );

	// Tabella lista risultati ricerca ordini di produzione
	$('#operlist').dataTable( {
		"iDisplayLength": -1,
		"aLengthMenu": [[5, 10, -1], [5, 10, "Tutti"]],
		"bFilter": false,
		"bInfo": false,
		"bDestroy":true,
		"bProcessing": true,
		"bServerSide": false,
		"bJQueryUI": true,
		"bSort": false,
		"bPaginate": true,
		"bStateSave": false,
		"sScrollY": "180",
		"bScrollCollapse": true,
		"oLanguage": {
			"sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
	});

	// Tabella lista risultati ricerca ordini e fase
	$('#charlist').dataTable( {
		"iDisplayLength": 5,
		"aLengthMenu": [[5, 10, -1], [5, 10, "Tutti"]],
		"bFilter": false,
		"bInfo": false,
		"bDestroy":true,
		"bProcessing": true,
		"bServerSide": false,
		"bJQueryUI": true,
		"bSort": false,
		"bPaginate": true,
		"bStateSave": false,
		"sScrollY": "180",
		"bScrollCollapse": true,
		"oLanguage": {
			"sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
	} );

	// Tabella lista caratteristiche cicliche
	$('#tabCycle').dataTable( {
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"bDestroy": false,
		"bRetrieve" : true,
		"bProcessing": false,
		"bServerSide": false,
		"bJQueryUI": true,
		"bSort": true,
		"bSortClasses": false,
		"bPaginate": false,
		"bStateSave": false,
		"bAutoWidth": false,
		"sScrollX": "100%",
		"sScrollY": "350",
		"bScrollCollapse": false,
		"aaSorting": [[ 0, "desc" ]], //alla vecchia: "aaSorting": [[ 0, "asc" ]],
		/*"aoColumnDefs": [ { "bVisible": false,  "aTargets": [ ] },
							{"sType": "numeric", "aTargets": [ 0 ]},], */
		"oLanguage": {"sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
	} );
	// Tabella lista caratteristiche cicliche
	$('#tabSeq').dataTable( {
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"bDestroy": false,
		"bRetrieve" : true,
		"bProcessing": false,
		"bServerSide": false,
		"bJQueryUI": true,
		"bSort": true,
		"bSortClasses": false,
		"bPaginate": false,
		"bStateSave": false,
		"bAutoWidth": false,
		"sScrollX": "100%",
		"sScrollY": "200",
		"bScrollCollapse": false,
		"aaSorting": [[ 0, "asc" ]],
		/*"aoColumnDefs": [ { "bVisible": false,  "aTargets": [ ] },
							{"sType": "numeric", "aTargets": [ 0 ]},], */
		"oLanguage": {"sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
	} );

	// Tabella lista caratteristiche in scadenza
	$('#tabTime').dataTable( {
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"bDestroy": false,
		"bRetrieve" : true,
		"bProcessing": false,
		"bServerSide": false,
		"bJQueryUI": true,
		"bSort": true,
		"bSortClasses": false,
		"bPaginate": false,
		"bStateSave": false,
		"bAutoWidth": false,
		"sScrollX": "100%",
		"sScrollY": "300",
		"bScrollCollapse": false,
		"aaSorting": [[ 0, "asc" ]],
		"aoColumnDefs": [ { "bVisible": false,  "aTargets": [ ] },
										 {"sType": "numeric", "aTargets": [ 0 ]}],
		"oLanguage": {"sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
	} );

	// Tabella Equipment
	$('#tabEquip').dataTable( {
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"bDestroy": false,
		"bRetrieve" : true,
		"bProcessing": false,
		"bServerSide": false,
		"bJQueryUI": true,
		"bSort": true,
		"bSortClasses": false,
		"bPaginate": false,
		"bStateSave": false,
		"bAutoWidth": false,
		"sScrollX": "100%",
		"sScrollY": "250",
		"bScrollCollapse": false,
		"aaSorting": [[ 0, "asc" ]],
		"aoColumnDefs": [ { "bVisible": false,  "aTargets": [6,7,8 ] },
										 {"sType": "numeric", "aTargets": [ 0 ]}],
		"oLanguage": {"sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
	} );

	// Tabella punti misura Equipment
	$('#tabEqpoint').dataTable( {
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"bDestroy": true,
		"bRetrieve" : false,
		"bProcessing": false,
		"bServerSide": false,
		"bJQueryUI": true,
		"bSort": false,
		"bSortClasses": false,
		"bPaginate": false,
		"bStateSave": false,
		"bAutoWidth": false,
		"sScrollX": "100%",
		"sScrollY": "150",
		"bScrollCollapse": false,
		/* "aaSorting": [[ 0, "asc" ]],
			 "aoColumnDefs": [ { "bVisible": false,  "aTargets": [ ] },
							{"sType": "numeric", "aTargets": [ 0 ]}],*/
		"oLanguage": {"sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
	} );

	$('#charResult').dataTable( {
		"iDisplayLength": -1,
		"aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, "Tutti"]],
		"bFilter": false,
		"bInfo": false,
		"bDestroy": false,
		"bProcessing": true,
		"bRetrieve" : true,
		"bServerSide": false,
		"bJQueryUI": true,
		"bSort": true,
		"bPaginate": true,
		"bStateSave": false,
		"sScrollX": "100%",
		"sScrollY": "170",
		"bScrollCollapse": false,
		"bAutoWidth": false,
		"aoColumnDefs": [ { "bVisible": false,  "aTargets": [8, 5, 6, 7, 10, 11 ] },
										 {"sClass": "td-center", "aTargets": [ 3 , 4]},
										 {"sSortDataType": "dom-text", "aTargets": [ 0 ,1, 2, 4 ] },
										 {"sType": "date", "aTargets": [ 0,1 ]}],
		"oLanguage": {"sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"		  }
	} );
}
