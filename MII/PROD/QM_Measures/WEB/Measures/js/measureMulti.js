/* jshint -W117, -W098 */ 
// Script gestione acquisizioni multiple misure
//**  measureMulti.js
//*******************************************
//**  Autore: Luca Adanti - IT-Link
//**
//**  Ver: 4.4.2
//**
//*******************************************
//** Ulteriori interventi di:
//** Giancarlo Pisoni
//** Bruno Rosati
//** Luca Adanti : last 02/08/2018
//** Antonio Masseretti : AM20191030 gestita chiusura notifiche


//*******************************************

// Funzione per verificare i campioni da acquisire in modalità multipla.
function fnCycleChars() {

	var dupes = {};
	dupes.length = 0;
	var singles = [];
	singles.length = 0;
	var calc = false;
	var checkToolOk = true;
    var bAllFiles = true;
    var bCorrectProcedure = true;
    var sMessage = "";
    
    var sFirstRow = $('#chars').dataTable().fnGetData(0);
    var acceptDate = sFirstRow[26].substr(0, 10);
    var newAcceptDate = acceptDate.split("/").reverse().join("-");
    var bNotADate = isNaN(Date.parse(newAcceptDate));
    if (sFirstRow[3] === "Accettazione" && bNotADate) {
        fnMessageOK("Per poter registrare le misurazioni è necessario inserire il documento materiale/bolla materia prima", "Errore");
        return;
    }
    
	$('#chars tbody tr').each(function (index, Row) {
		var $Row = $(Row);
		var Sample = $('#chars').dataTable().fnGetData(Row)[5];
		var sCalc = $('#chars').dataTable().fnGetData(Row)[7];
		var stool = $('#chars').dataTable().fnGetData(Row)[27];
		var cTipo = $('#chars').dataTable().fnGetData(Row)[3];
		var sOper = $('#chars').dataTable().fnGetData(Row)[0];
		var sProDeb = $('#chars').dataTable().fnGetData(Row)[29];

		if (stool !== "STRUMENTO-FILE") {
            bAllFiles = false;
        }
        
        if(parseInt(sOper) > 2999) {
            if (parseInt(sProDeb) != 0 && typeof(sProDeb)  != "undefined" ) {
                bCorrectProcedure = false;
                sMessage = $('#chars').dataTable().fnGetData(Row)[30];
            }
        }
        
        selChan = $.trim($('#chars').dataTable().fnGetData(Row)[23].substr(1, 2));
	//AM-2019-05-29 if (selChan != 10 && sCalc != 'Si' && cTipo == "Misura")
        if (selChan != 20 && sCalc != 'Si' && cTipo == "Misura") {
            checkToolOk = fnCheckTool(stool, $('#chars').dataTable().fnGetData(Row)[22]);
        }
        
        if (!checkToolOk) {
            return false;
        }
        
        if (stool.indexOf("FILE") < 0) {
            if (dupes[Sample] > 0) {
                dupes[Sample] += 1;
                if (dupes[Sample] == 2) {
                    singles.push(Sample);
                }
            } else {
                dupes[Sample] = 1;
            }
        }
        if (sCalc == 'Si') {
            calc = true;
        }
	});
    
    if (bAllFiles) {
        fnMessageOK("Effettuare l'acquisizione da file", "Errore");
        checkToolOk = false;
    }
    
     if (!bCorrectProcedure) {
        fnMessageOK(sMessage, "Errore");
        checkToolOk = false;
    }

	if (!checkToolOk) {
		return;
	}

	if (calc) {
		// Non è possibile procedere in modo multiplo per campioni ma solo per caratteristiche.
		fnMultiModeChars(singles);
		return;
	} else {
		// Scelta se procedere per campioni o per caratteristiche.
		var cButtons = [];

		cButtons.push({
			id: "multi-camp",
			text: "1 - per Campione",
			click: function () {
				$(this).dialog("close");
				fnMultiModeChars(singles);
			}
		});

		cButtons.push({
			id: "multi-char",
			text: "2 - per Caratteristica",
			click: function () {
				$(this).dialog("close");
				fnMultiModeSamples(0);
			}
		});

		cButtons.push({
			id: "multi-canc",
			text: "3 - Annulla",
			click: function () {
				$("#Result").dialog("close");
				fnDisableHK_MultiDialog();
				objPopupTimer.tAutoContinue.play();
			}
		});

		//Noty.closeAll(); AM20191030
    fmCloseMessagePopup();
        objPopupTimer.tAutoContinue.stop();
        objPopupTimer.tAutoContinue = $.timer(function () {
            objPopupTimer.function();
        }, 250, false);

		$("#Result").dialog({
			/*resizable: false,*/
			height: 220,
			//			   width: 470,
			width: 570,
			modal: true,
			title: "Selezione modalità",
			autoOpen: false,
			resizable: true,
			closeOnEscape: false,
			draggable: true
		});


		$("#Result P").text("Come si vuole procedere all'acquisizione:");
		$("#Result").dialog("option", "buttons", cButtons);
		fnEnableHK_MultiDialog(); // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
		$("#Result").dialog("open");
		/* Work-around per eventi touch */
		$("#multi-camp").on('touchend', function (e) {
			$("#multi-camp").click();
			e.preventDefault();
			return;
		});
		$("#multi-char").on('touchend', function (e) {
			$("#multi-char").click();
			e.preventDefault();
			return;
		});
		$("#multi-canc").on('touchend', function (e) {
			$("#multi-canc").click();
			e.preventDefault();
			return;
		});
		return;
	}
}
//}

// Funzione per eseguire l'acquisizione multipla per campione.
function fnMultiModeChars(singles) {
	if ($(singles).size() < 1) {
		//Non ci sono elementi per fare il campionamento ed esce
		fnMessageOK("Non è possibile effettuare il campionamento delle operazioni");
		return;
	}

	if ($(singles).size() > 1) {
		var cButtons = [];
		var sCount = $(singles).size();
		//for (var i = 0; i<sCount; i++)
		$(singles).each(function (i, cSingle) {
			cButtons.push({
				id: "btnModeChars" + (i + 1),
				text: "Camp. da " + cSingle,
				click: function () {
					$(this).dialog("close");
					fnCycleCharsDet(cSingle, 0, 0);
				}
			});
		});

		cButtons.push({
			id: "btnModeCharsCancel",
			text: "Annulla",
			click: function () {
				$(this).dialog("close");
				fnClearMeasures();
				objPopupTimer.tAutoContinue.play();
			}
		});
		$("#Result").dialog({
			/*resizable: false,*/
			height: 180,
			width: 420,
			modal: true,
			title: "Numero campioni non omogenei.",
			autoOpen: false,
			resizable: true,
			closeOnEscape: false,
			draggable: true
		});

		$("#Result P").text("Seleziona il numero di campioni da acquisire:");
		$("#Result").dialog("option", "buttons", cButtons);
		$("#Result").dialog("open");
		$(singles).each(function (i, cSingle) {
			/* Work-around per eventi touch */
			$("#btnModeChars" + (i + 1)).bind('touchend', function (e) {
				$("#btnModeChars" + (i + 1)).click();
				e.preventDefault();
				return;
			});
		});

		/* Work-around per eventi touch */
		$("#btnModeCharsCancel").bind('touchend', function (e) {
			$("#btnModeCharsCancel").click();
			e.preventDefault();
			return;
		});
	}

	if ($(singles).size() == 1) {
		fnCycleCharsDet(Number(singles[0]), 0, 0);
	}

}

// Funzione per eseguire l'acquisizione multipla per caratteristica.
function fnMultiModeSamples(curRow) {
	if (window.console) {
		console.log("fnMultiModeSamples -> curRow: " + curRow);
	}


	objTimer.function = function () {
		console.log("Timer: " + objTimer.id + " " + new Date());
		//fnCycleCharsDet (Samples, iCount, iRow + 1 );
		if ($("#Approv").val() === "" && document.getElementById("Avvio").checked === true) {} else {
			$("#next-ok-c").click();
		}
	};
	objTimer.tAutoContinue.play();
	console.debug("Timer " + objTimer.id + ": on - fnMultiModeSamples");

	if (curRow === 0) {
		fnClearState();
		if ($('#tabSeq').find('tbody tr').size() > 0) {
			$('#tabSeq').dataTable().fnClearTable();
		}
		cicRows.length = 0;
		$('#chars tbody tr').each(function (index, Row) {
			var $Row = $(Row);
			//if (window.console) console.log( "fnCycleCharsDet -> Strumento: " +  $('#chars').dataTable().fnGetData(Row)[21] );
			//if (window.console) console.log( "fnCycleCharsDet -> Caratter.: " +  $Row.find('td:nth(3)').text() );
			if ($('#chars').dataTable().fnGetData(Row)[27].indexOf('FILE') < 0) {
				cicRows.push(Row);
			}
		});
		boolWork = true;
		$("#charsSeq").dialog({
			/*resizable: false,*/
			height: 440,
			width: 680,
			modal: true,
			title: "Acquisizione (" + 1 + " di " + $(cicRows).size() + ") caratteristiche",
			autoOpen: false,
			resizable: true,
			closeOnEscape: false,
			draggable: true,
			buttons: [
				{
					id: "multi-stepchar-blc",
					text: "7 - Blocca Postaz.", // [3956] User: giancarlo  Date: 27/11/2014  Purpose: managing of shortcut keys
					click: function () {
						fnLockWs();
					}
				},
				{
					id: "next-ok-c",
					text: "► Prossimo", // [3956] User: giancarlo  Date: 27/11/2014  Purpose: managing of shortcut keys
					click: function () {
						fnCycleCharsDet(Samples, iCount, iRow + 1);
					}
				},
				{
					id: "next-cancel",
					text: "9 - Annulla", // [3956] User: giancarlo  Date: 27/11/2014  Purpose: managing of shortcut keys
					click: function () {
						objTimer.tAutoContinue.stop();
						console.debug("Timer " + objTimer.id + ": stopped");
						$(this).dialog("close");
						fnClearMeasures();
						fnEnableHK_MainWindow(); // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
						boolWork = false;
					}
				}
			]
		});
		fnEnableHK_MultiCharStep(); // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
		//	  shortcut.add("Enter", function() {$("#next-ok-c").click(); shortcut.remove("Enter");});			// User: giancarlo  Date: 27/10/2016

		$("#charsSeq").dialog("open");
		/* Work-around per eventi touch */
		$("#multi-stepchar-blc").bind('touchend', function (e) {
			$("#multi-stepchar-blc").click();
			e.preventDefault();
			return;
		});
		$("#next-ok-c").bind('touchend', function (e) {
			$("#next-ok-c").click();
			e.preventDefault();
			return;
		});
		$("#next-cancel").bind('touchend', function (e) {
			$("#next-cancel").click();
			e.preventDefault();
			return;
		});
	}

	$("#charsSeq").dialog("option", "title", "Acquisizione (" + (curRow) + " di " + $(cicRows).size() + ") caratteristiche");
	if (curRow < $(cicRows).size()) {
		$("#charsSeq p").text("Prossima caratteristica: " + $('#chars').dataTable().fnGetData(cicRows[curRow])[1]);
	}
	$('#next-ok-c').unbind('click');
	if (curRow > 0 && curRow <= $(cicRows).size()) {
		$("#next-ok-c span").text("► Prossimo");

		//	   shortcut.add("Enter", function() {$("#next-ok-c span").click(); shortcut.remove("Enter");  }); 	// User: giancarlo  Date: 27/10/2016

		//if (bfnCancel) $('#tabSeq').dataTable().fnUpdate( "Annullato", curRow-1, 2 );
		//else $('#tabSeq').dataTable().fnUpdate( "Acquisito", curRow-1, 2 );
		var cRow = [];
		if (bfnCancel) {
			cRow.push($('#chars').dataTable().fnGetData(cicRows[curRow - 1])[0], $('#chars').dataTable().fnGetData(cicRows[curRow - 1])[1], "Annullato");
		} else {
			cRow.push($('#chars').dataTable().fnGetData(cicRows[curRow - 1])[0], $('#chars').dataTable().fnGetData(cicRows[curRow - 1])[1], "Acquisito");
		}
		$('#tabSeq').dataTable().fnAddData(cRow);
	} else {
		fnSelectSingle(cicRows[curRow], curRow + 1);
	}
	charSelected = cicRows[curRow];
	if (curRow == $(cicRows).size()) {
		objTimer.tAutoContinue.stop();
		console.debug("Timer " + objTimer.id + ": stopped");

		$("#next-ok-c").bind('click', function () {
			fnEnableHK_MainWindow(); // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
			objPopupTimer.tAutoContinue.play();
			$("#charsSeq").dialog("close");
		});
		$("#next-ok-c span").text("8 - Chiudi");
		$("#next-cancel").button("disable");
		$("#charsSeq p").text("Acquisizione caratteristiche completata: salvare per terminare");
		$("#charsSeq").dialog("option", "title", "Operazione ternimata.");
	} else {
		//var cRow = [];
		//cRow.push($('#chars').dataTable().fnGetData(cicRows[curRow])[0], $('#chars').dataTable().fnGetData(cicRows[curRow])[1], "in process");
		//$('#tabSeq').dataTable().fnAddData(cRow);

		$("#next-ok-c").bind('click', function () {
			fnSelectSingle(cicRows[curRow], curRow + 1);
			//fnMultiModeSamples(curRow+1);
		});
	}
}

// Funzione per gestire l'inserimento ricorsivo delle caratteristiche e misure a campione
function fnCycleCharsDet(Samples, iCount, iRow) {

	if (window.console) {
		console.log("fnCycleCharsDet -> Samples: " + Samples + " -> iCount: " + iCount + " -> iRow: " + iRow);
	}

	/*tAutoContinue = $.timer(function () {
				fnCycleCharsDet (Samples, iCount, iRow + 1 );
		}, 5000, false);*/
	/*objTimer = {
				id: "timer",
				tAutoContinue: $.timer(function () {
						if (window.console) console.log("Timer: " + this.id + " " + new Date());
				}, 5000, false)
		};*/
	console.debug("Timer " + objTimer.id + ": on - fnCycleCharsDet");
	objTimer.function = function () {
		console.log("Timer: " + objTimer.id + " " + new Date());
		//fnCycleCharsDet (Samples, iCount, iRow + 1 );
		$("#next-ok").click();
	};
	objTimer.tAutoContinue.play();

	if (iCount === 0 && iRow === 0) {
		if ($('#tabCycle').find('tbody tr').size() > 0) {
			$('#tabCycle').dataTable().fnClearTable();
		}
		cicRows.length = 0;
		fnClearState();
		$('#chars tbody tr').each(function (index, Row) {
			var $Row = $(Row);
			//if (window.console) console.log( "fnCycleCharsDet -> Strumento: " +  $('#chars').dataTable().fnGetData(Row)[21] );
			//if (window.console) console.log( "fnCycleCharsDet -> Caratter.: " +  $Row.find('td:nth(3)').text() );
			if ($Row.find('td:nth(2)').text() == Samples && $('#chars').dataTable().fnGetData(Row)[27].indexOf('FILE') < 0) {
				cicRows.push(Row);
				//cicRows.unshift(Row);  istruzione non corretta By Luca
			}
		});
		boolWork = true;
		$("#charsCycle").dialog({
			//resizable: false,
			height: 600,
			width: 680,
			modal: true,
			title: "Acquisizione (" + (iCount + 1) + " di " + Samples + ")",
			autoOpen: false,
			resizable: true,
			closeOnEscape: false,
			draggable: true,
			buttons: [
				{
					id: "multi-step-blc", // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
					text: "7 - Blocca Postaz.",
					click: function () {
						fnLockWs();
					}
				},
				{
					id: "next-ok",
					text: "► Prossimo",
					click: function () {
						fnCycleCharsDet(Samples, iCount, iRow + 1);
					}
				},
				{
					id: "multi-step-ann", // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
					text: "9 - Annulla",
					click: function () {
						objTimer.tAutoContinue.stop();
						console.debug("Timer " + objTimer.id + ": stopped");
						$("#PopupConfirm P").text("Annullare tutto il processo di acquizione?");
						$("#PopupConfirm").dialog({
							resizable: false,
							height: 200,
							modal: true,
							buttons: [
								{
									id: "btnConfYes1",
									text: "Sì",
									click: function () {
										//misRefr.stop();
										fnClearMeasures();
										fnClearState();
										$(this).dialog("close");
										$('#charsCycle').dialog("close");
										fnEnableHK_MainWindow(); // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
										boolWork = false;
									}
								},
								{
									id: "btnConfNo1",
									text: "No",
									click: function () {
										$(this).dialog("close");
									}
								}
							]
						});
						/* Work-around per eventi touch */
						$("#btnConfYes1").bind('touchend', function (e) {
							$("#btnConfYes1").click();
							e.preventDefault();
							return;
						});
						$("#btnConfNo1").bind('touchend', function (e) {
							$("#btnConfNo1").click();
							e.preventDefault();
							return;
						});
					}
				}
			]
		});

		//	shortcut.add("Enter", function() {$("#next-ok").click(); shortcut.remove("Enter"); });				// User: giancarlo  Date: 27/10/2016

		fnEnableHK_MultiCampStep(); // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
		$("#charsCycle").dialog("open");
		/* Work-around per eventi touch */
		$("#multi-step-blc").bind('touchend', function (e) {
			$("#multi-step-blc").click();
			e.preventDefault();
			return;
		});
		$("#next-ok").bind('touchend', function (e) {
			$("#next-ok").click();
			e.preventDefault();
			return;
		});
		$("#multi-step-ann").bind('touchend', function (e) {
			$("#multi-step-ann").click();
			e.preventDefault();
			return;
		});
		$('#tabCycle').dataTable().fnDraw(true);
		//tabCycle
	}
	//Gestione del titolo
	$("#charsCycle").dialog("option", "title", "Acquisizione (" + (iCount + 1) + " di " + Samples + ")");

	$('#next-ok').unbind('click');

	// Gestione dell'evento del pulsante prossimo
	console.log(
		"dimensione di cicRows - 1: " + $(cicRows).size() - 1 + " - iRow: " + iRow + " - (Samples-1): " + (Samples - 1) +
		" - iCount:" + iCount + " \nIl pulsante cambia testo quando dimensione di cicRows - 1 è uguale a iRow e (Samples - 1) è uguale a iCount"
	);
	if ($(cicRows).size() - 1 == iRow && Samples - 1 == iCount) {
		objTimer.tAutoContinue.stop();
		console.debug("Timer " + objTimer.id + ": stopped");

		$("#next-ok").click(function () {
			// arrivato all'ultima caratteristica dell'ultimo campione
			//fnCycleCharsDet (Samples, iCount, iRow + 1 );
			//alert("Salva");
			fnSaveMeasMulti();
			fnEnableHK_MainWindow(); // [3956] User: giancarlo  Date: 04/12/2014  Purpose: managing of shortcut keys
			$("#charsCycle").dialog("close");
		});
		$("#next-ok span").text("8 - Salva");
		$("#charsCycle p").text("Acquisizione caratteristiche completata: salvare per terminare");
	} else if ($(cicRows).size() - 1 == iRow && Samples - 1 > iCount) {
		// arrivato all'ultima caratteristica rincomincia dalla prima della prossima riga
		$("#next-ok").click(function () {
			fnCycleCharsDet(Samples, iCount + 1, 0);
		});
	} else {
		$("#next-ok").click(function () {
			fnCycleCharsDet(Samples, iCount, iRow + 1);
		});
	}

	// Gestione del testo della caratteristica corrente
	var sline = $('#chars').dataTable().fnGetData(cicRows[iRow]);
	charSelected = cicRows[iRow];
	$("#charsCycle p").text("Acquisizione caratteristica: " + sline[1] + " (" + sline[0] + ")");

	var cRow = [];
	cRow.push(iCount + 1, sline[0], sline[1], "", "in process");
	//cRow.unshift(iCount+1, sline[0], sline[1], "", "in process"); istruzione non corretta by Luca
	//console.log(cRow);
	$('#tabCycle').dataTable().fnAddData(cRow);
	//$('#tabCycle').dataTable().fnAddDataToStart(cRow);


	if (sline[3] == "Accettazione" && sline[10] != "X") {
		//$('#setOrd').button("enable");
	} else if (sline[3] == "Misura") {
		selTolInf = sline[21];
		selTolSup = sline[20];
		selChan = $.trim(sline[23].substr(1, 2));
		if (sline[7] == "Si") {
			fnCalcMis(iCount, Samples, iRow, $(cicRows).size(), sline[0], sline[24]);
		} else {
			fngetSingleMis(iCount, Samples, iRow, $(cicRows).size(), sline[0], sline[1]);
		}
	} else if (sline[3] == "Qualitativo") {
		//$('#setMisC').button("enable");
		//$("#ordAcc P").text(sline[1] + " " + sline[6]);
		CAT_TYPE = sline[13];
		PSEL_SET = sline[14];
		SEL_SET = sline[15];
		fnMultiValChar(iCount, Samples, iRow, $(cicRows).size(), sline[0], sline[1]);
	} else {
		//$('#setMisV').button("enable");
		//$("#ordAcc P").text(sline[1] + " " + sline[6]);
		fnMultiQualChar(iCount, Samples, iRow, $(cicRows).size(), sline[0], sline[1]);
	}
}

// Funzione per acquisire misura singola su campione multiple
function fngetSingleMis(curSample, Samples, curRow, Rows, curChar, curCharDesc) {

	fnSetChan(selChan, "STARTM", curChar);
	$("#selChar").val(curChar);
	var qXml = qBasePath + "GetCatListQR&Param.1=" + $.trim(localIP);


	objTimer.tAutoContinue.stop();
	console.debug("Timer " + objTimer.id + ": stopped");

	var misRefr = $.timer(function () {
		fnMeasDataSingle(curChar, curSample);
	}, 950, false);
	misRefr.play();
	boolWork = true;
	$("#misColl").dialog({
		resizable: false,
		closeOnEscape: false,
		title: "campione " + (Number(curSample) + 1) + " (di " + Samples + ") : " + curCharDesc + " (" + curChar + ")",
		height: 600,
		width: 750, //650,	// [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
		modal: true,
		buttons: [
			{
				id: "del-mis", // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
				text: "1 - Cancella misura",
				click: function () {
					//misRefr.pause();
					fnDelMis(misRefr);
					//misRefr.play();
				}
			},

			{
				id: "multi-camp-can", // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
				text: "2 - Canale",
				click: function () {
					$("#dxpanel").toggle("slide", {
						direction: 'right'
					});
				}
			},

			{
				id: "multi-camp-man", // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
				text: "3 - Manuale",
				click: function () {
					fnMeasManual(true, curChar, misRefr);
				}
			},

			{
				id: "button-ok", // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
				text: "► Continua",
				click: function () {
					misRefr.stop();
					fnSaveMeasTmp(curSample, curRow, Rows, curChar, Samples);
					fnSetChan("0", "STOPM", "");
					objTimer.tAutoContinue.play();
					console.debug("Timer " + objTimer.id + ": on - fngetSingleMis");
					$(this).dialog("close");
				}
			},

			{
				id: "multi-camp-ann", // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
				text: "5 - Annulla",
				click: function () {
					$("#PopupConfirm P").text("Annullare tutto il processo di acquizione?");
					$("#PopupConfirm").dialog({
						resizable: false,
						height: 200,
						modal: true,
						buttons: [{
							id: "btnConfYes",
							text: "Sì",
							click: function () {
								objTimer.tAutoContinue.stop();
								console.debug("Timer " + objTimer.id + ": stopped");
								misRefr.stop();
								fnClearMeasures();
								fnClearState();
								fnSetChan("0", "STOPM", "");
								$(this).dialog("close");
								$('#misColl').dialog("close");
								$('#charsCycle').dialog("close");
								boolWork = false;
								fnEnableHK_MainWindow();
							}
						}, {
							id: "btnConfNo",
							text: "No",
							click: function () {
								$(this).dialog("close");
							}
						}]
					});
					/* Work-around per eventi touch */
					$("#btnConfYes").bind('touchend', function (e) {
						$("#btnConfYes").click();
						e.preventDefault();
						return;
					});
					$("#btnConfNo").bind('touchend', function (e) {
						$("#btnConfNo").click();
						e.preventDefault();
						return;
					});
				}
			}
		]
	});

	/* Work-around per eventi touch */
	$("#del-mis").bind('touchend', function (e) {
		$("#del-mis").click();
		e.preventDefault();
		return;
	});
	$("#multi-camp-can").bind('touchend', function (e) {
		$("#multi-camp-can").click();
		e.preventDefault();
		return;
	});
	$("#multi-camp-man").bind('touchend', function (e) {
		$("#multi-camp-man").click();
		e.preventDefault();
		return;
	});
	$("#button-ok").bind('touchend', function (e) {
		$("#button-ok").click();
		e.preventDefault();
		return;
	});
	$("#multi-camp-ann").bind('touchend', function (e) {
		$("#multi-camp-ann").click();
		e.preventDefault();
		return;
	});

	$("#wsmis_filter").css("display", "none");
	$("#button-ok").button("disable");
	$("#del-mis").button("disable");
	fnEnableHK_MultiCampDialog(); // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
}


// Funzione per salvare temporaneamente la misura in MII
function fnSaveMeasTmp(curSample, curRow, Rows, curChar, Samples) {
	var rowCount = $('#wsmis tbody tr').length - 1 - Number(curSample);
	var misVal = $('#wsmis tbody tr:nth(' + rowCount + ')').find('td:nth(2)').text();
	var tRow = Rows * curSample + curRow;
	$('#tabCycle').dataTable().fnUpdate(misVal, tRow, 3);
	$('#tabCycle').dataTable().fnUpdate("Acquisito", tRow, 4);
	fnEnableHK_MultiCampStep(); // [3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
	$("#misColl").dialog("close");
}


// Funzione per salvare temporaneamente i campioni qualitativi in MII
function fnSaveSingTmp(resQual, curSample, curRow, Rows, curChar) {

	var tRow = Rows * curSample + curRow;
	$('#tabCycle').dataTable().fnUpdate(resQual, tRow, 3);
	$('#tabCycle').dataTable().fnUpdate("Acquisito", tRow, 4);
	if ($("#misColl").is(':visible')) {
		$("#misColl").dialog("close");
	}
}

// Funzione per inserire la validazione da standard ricorsiva con accettato rifiutato
function fnMultiValChar(curSample, Samples, curRow, Rows, curChar, curCharDesc) {
	$("#ordAcc").find(".info-block").hide();
	$("#ordAcc").find("P").show();
	$("#ordAcc").find("P").text("Acquisizione di (" + curChar + ") " + curCharDesc);
	$("#accOrder").val("");
	bfnCancel = false;
	// configura la dialog
	$("#ordAcc").dialog({
		height: 180,
		width: 420,
		modal: true,
		title: "Valutazione qualitativa (" + (curSample + 1) + " di " + Samples + ")",
		autoOpen: false,
		resizable: true,
		closeOnEscape: false,
		draggable: true,
		open: function () {
			objTimer.tAutoContinue.stop();
			console.debug("Timer " + objTimer.id + ": stopped");
		}
	});

	var cButtons = [];
	cButtons.push({
		id: "btnAceptConf",
		text: "Accetta",
		click: function () {
			fnStoreChars("", "A", "", "", $("#accOrder").val(), curChar);
			fnSaveSingTmp("A", curSample, curRow, Rows, curChar);
			$("#ordAcc").dialog("close");
			fnEnableHK_MultiCampStep(); //User: giancarlo  Date: 17/11/2016
			objTimer.tAutoContinue.play();
		}
	});
	cButtons.push({
		id: "btnRejectConf",
		text: "Rifiuta",
		click: function () {
			fnStoreChars("", "R", "", "", $("#accOrder").val(), curChar);
			fnSaveSingTmp("R", curSample, curRow, Rows, curChar);
			$("#ordAcc").dialog("close");
			fnEnableHK_MultiCampStep(); //User: giancarlo  Date: 17/11/2016
			objTimer.tAutoContinue.play();
		}
	});

	$("#ordAcc").dialog("option", "buttons", cButtons);
	$("#ordAcc").dialog("open");

	/* Work-around per eventi touch */
	$("#btnAceptConf").bind('touchend', function (e) {
		$("#btnAceptConf").click();
		e.preventDefault();
		return;
	});
	$("#btnRejectConf").bind('touchend', function (e) {
		$("#btnRejectConf").click();
		e.preventDefault();
		return;
	});
}


// Funzione per inserire la validazione da catalogo ricorsiva
function fnMultiQualChar(curSample, Samples, curRow, Rows, curChar, curCharDesc) {

	objTimer.tAutoContinue.stop();
	console.debug("Timer " + objTimer.id + ": stopped");

	bfnCancel = false;
	$("#ordAcc").find(".info-block").hide();
	$("#ordAcc").find("P").show();
	$("#accOrder").val("");

	boolWork = true;
	// configura la dialog
	$("#ordAcc").dialog({
		height: 180,
		width: 420,
		modal: true,
		title: "Valutazione qualitativa (" + (curSample + 1) + " di " + Samples + ")",
		autoOpen: false,
		resizable: true,
		closeOnEscape: false,
		draggable: true
	});
	//if (iCount == 0) {
	//var lButtons = $( "#ordAcc" ).dialog("option", "buttons");
	var qXml = qBasePath + "GetCatListQR&Param.1=" + PSEL_SET;
	qXml = qXml + "&Param.2=" + SEL_SET;
	qXml = qXml + "&Param.3=" + CAT_TYPE;
	var data = fnGetAjaxData(qXml, false);
	var $catlist = $(data).find("Row");
	//Cicla per tutte le righe
	var cButtons = [];
	$catlist.each(function (index, cat) {
		var $cat = $(cat);
		var group = $cat.children('CODE_GROUP').text();
		var code = $cat.children('CODE').text();
		var ctxt = $cat.children('CODE_TEXT').text();
		var cret = $cat.children('CODE_VALUATION').text();
		cButtons.push({
			id: "btn_conf_" + (index + 1),
			text: ctxt,
			click: function () {
				//fnSaveSamples (cret, group, code, $("#accOrder").val(),iCount + 1 );
				fnStoreChars("", cret, group, code, $("#accOrder").val(), curChar);
				fnSaveSingTmp(cret, curSample, curRow, Rows, curChar);
				$(this).dialog("close");
				fnEnableHK_MultiCampStep(); //User: giancarlo  Date: 17/11/2016
				objTimer.tAutoContinue.play();
			}
		});
	});
	cButtons.push({
		id: "btn_cnl",
		text: "Annulla",
		click: function () {
			bfnCancel = true;
			$(this).dialog("close");
			boolWork = false;
			fnEnableHK_MultiCampStep(); //User: giancarlo  Date: 17/11/2016
			objTimer.tAutoContinue.stop();
		}
	});

	$("#ordAcc").dialog("option", "buttons", cButtons);
	$("#ordAcc").dialog("open");

	$catlist.each(function (index, cat) {
		/* Work-around per eventi touch */
		$("#btn_conf_" + (index + 1)).bind('touchend', function (e) {
			$("#btn_conf_" + (index + 1)).click();
			e.preventDefault();
			return;
		});
	});

	/* Work-around per eventi touch */
	$("#btn_cnl").bind('touchend', function (e) {
		$("#btn_cnl").click();
		e.preventDefault();
		return;
	});
}

// funziona per accodare i risultati qualitativi e quantitativi
function fnStoreChars(Result, Valutaz, Code_Group, Code, Remark, curChar) {
	var qreg = qDataPath + "SetMisValueQR&Param.1=" + $.trim(localIP) + "&Param.2=" + Result;
	qreg = qreg + "&Param.3=0";
	qreg = qreg + "&Param.4=" + curChar;
	qreg = qreg + "&Param.5=" + Valutaz;
	qreg = qreg + "&Param.6=" + Code_Group;
	qreg = qreg + "&Param.7=" + Code;
	qreg = qreg + "&Param.8=" + Remark;
	exeQuery(qreg, "", "Errore in accodamento risultato");

}


// Funzione per salvare in SAP le misure
function fnSaveMeasMulti() {
	if ($("#Approv").val() === "" && document.getElementById("Avvio").checked === true) // User: giancarlo  Date: 10/04/2017	[8017]
	{
		console.debug("Avvio il benestare da fnSaveMeasMulti");
		$("#cid-approv").dialog({
			resizable: false,
			position: [((window.innerWidth / 2) - 150), 10],
			height: 180,
			width: 300,
			modal: true,
			closeOnEscape: false,
			buttons: {
				OK: function () {
					if (fnCheckCID_approv($("#txtCID_approv").val())) {
						$("#Approv").val(fnCheckApprov($("#LotId").val(), $("#Oper").val(), $("#txtCID_approv").val()));
						fnSaveMeasMulti_commit();
						$(this).dialog("close");
					}
				}
			}
		});
	} else {
		fnSaveMeasMulti_commit();
	}
}

function fnSaveMeasMulti_commit() {
	objTimer.tAutoContinue.stop();
	console.debug("Timer " + objTimer.id + ": stopped");

	var params = qBasePath + "SaveMeasMultiQR&Param.1=" + $("#LotId").val();
	params = params + "&Param.2=" + $("#Oper").val();
	params = params + "&Param.3=" + currentCID;
	params = params + "&Param.4=" + $.trim(localIP);
    
    let ord = $("#Order").val();
    let oper = $("#Oper").val();
    let matid = $("#Material").val();
    let matdesc = $("#MatDesc").val();
    let qty = $("#Qty").val();
    let lotID = $("#LotId").val();

	var result = fnGetAjaxData(params, false);
	if ($(result).find('Row').children("CodeResult").text() == "00") {
		//alert("Operazione completata con successo");
		fnMessageOK("Operazione completata con successo");
        fnDisplayOrder(Number(ord), oper, lotID, matid, matdesc.replace(/\"/g, '#'), qty, true);
		objPopupTimer.tAutoContinue.play();
		//location.reload();
	} else {
		fnMessageOK($(result).find('Row').children("CodeDescription").text());
	}
	//    params =  qBasePath + "GetCharQR&Param.1=" + $( "#LotId" ).val() + "&Param.2=" + $("#Oper").val();
	params = qBasePath + "GetCharQR_opti&Param.1=" + $("#LotId").val() + "&Param.2=" + $("#Oper").val(); //User: giancarlo  Date: 25/10/2016
	fnGetXMLData(params, '#chars', true, true);
	boolWork = false;
}