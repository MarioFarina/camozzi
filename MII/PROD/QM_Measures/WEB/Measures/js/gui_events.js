/* jshint -W117, -W098 */ 
// Script gestione gesture touch 
//**  gui_events.js
//*******************************************
//**  Autore: Giancarlo Pisoni - Camozzi
//**
//**  Ver: 3.2.0
//**
//*******************************************
//** Ulteriori interventi di:
//** Luca Adanti - IT-Link
//** Bruno Rosati - IT-Link
//*******************************************

function fnPanelLeftToggle() {
  //      $("#sxpanel").toggle("slide", {
  //         direction: 'left'
  //      });

  $("#sxpanel").toggle();

  $("#del-item").height($("#sx_del").height() - 4);

  if ($("#sxpanel").is(':visible')) {
    LeftPanelShown = "X";
    fnEnableHK_WorkListPanel();
  } else {
    LeftPanelShown = " ";
    fnDisableHK_WorkListPanel();
  }

  //     if (LeftPanelShown != "X")
  //     {
  //       LeftPanelShown = "X";
  //       fnEnableHK_WorkListPanel();
  //     }
  //     else
  //     {
  //       LeftPanelShown = " ";
  //       fnDisableHK_WorkListPanel();
  //     }

}


function fnPanelUpToggle() {
  $("#orderSearch").accordion({
    active: 1
  });
  $("#operlist").dataTable().fnDraw();
  $("#charlist").dataTable().fnDraw();
  $("#operlist").dataTable().fnClearTable();
  $("#charlist").dataTable().fnClearTable();
  $("#ui-accordion-orderSearch-panel-1").css("height", "auto");
  $("#ui-accordion-orderSearch-panel-0").css("height", "auto");
  $("#ui-accordion-orderSearch-panel-2").css("height", "auto");
  $("#ui-accordion-orderSearch-panel-3").css("height", "auto");

  //      $("#tpanel").toggle("slide", {
  //         direction: 'up'
  //      });
  $("#tpanel").toggle();

  $("#txtOrdOpe").val("");
  $("#txtOrdOpe").focus();

  if (UpperPanelShown != "X") {
    UpperPanelShown = "X";
    fnEnableHK_OrderSearchPanel();
  } else {
    UpperPanelShown = " ";
    fnDisableHK_OrderSearchPanel();
  }

}


function fnLogoutAsk() {
  $("#PopupConfirm P").text("Effetturare il logout dalla postazione?");
  $("#PopupConfirm").dialog({
    resizable: false,
    height: 200,
    modal: true,
    buttons: {
      Sì: function () {
        $(this).dialog("close");
        $('#sx_pan').focus();
        fnGetCID();
      },
      No: function () {
        $(this).dialog("close");
      }
    }
  });
}


function fnSetWorkstationName() {
  $("#setWsAddr").dialog({
    resizable: false,
    height: 190,
    width: 330,
    modal: true,
    buttons: {
      Elimina: function () {
        $(this).dialog("close");
        $.removeCookie('WsIpAddr');
        $.ajax({
          "url": "/XMII/CM/QM_Measures/getIpAddr.jsp",
          "dataType": "html",
          "type": "GET",
          "cache": false,
          "success": function (data) {
            $("#info").html($.trim(data));
            localIP = $.trim(data);
            currentIP = localIP;
            fnOrOpList();
            //fnMeasData($.trim(localIP));
          }
        });
      },
      Salva: function () {
        $(this).dialog("close");
        var wsvalue = $('#idAddr').val();
        if ($.trim(wsvalue) !== '') {
          $.cookie('WsIpAddr', $.trim(wsvalue), {
            expires: 365
          });
          $("#info").html(wsvalue);
          localIP = wsvalue;
          currentIP = wsvalue;
        }
      },
      Annulla: function () {
        $(this).dialog("close");
      }
    }
  });
}

function fnButtonFilter(valFilter) {
    var sline;
    
    if(valFilter === undefined || valFilter === "")
        valFilter = "ALL";
    
    if (valFilter != "ALL") {
        $('#chars').dataTable().fnSettings()._iDisplayLength = -1;
        $('#chars').dataTable().fnDraw();
        $('#chars').dataTable().fnFilter(valFilter, 17);
        if (window.console) {
            console.log("righe filtrate:" + $('#chars tbody tr').length);
        }
        if ($('#chars').dataTable().fnSettings().fnRecordsDisplay() > 1) {
            $('#misCycle').button("enable");
        } else {
            $('#misCycle').button("disable");
        }
        
        if(valFilter === "Altro"){
            $('#misCycle').button("disable");
        }
        
        for (var i = 0; i < $('#chars').dataTable().fnGetData().length; i++) {
            sline = $('#chars').dataTable().fnGetData(i);
            if (sline[17] == valFilter) {
                MinCharIndex = i;
                break;
            }
        }
        
        for (i = $('#chars').dataTable().fnGetData().length - 1; i >= 0; i--) {
            sline = $('#chars').dataTable().fnGetData(i);
            if (sline[17] == valFilter) {
                MaxCharIndex = i;
                break;
            }
        }
        charsGrpSelected = valFilter;
    } else {
        $('#chars input').val("");
        $("#chars").dataTable().fnFilter('', 17);
        $("#chars").dataTable().fnFilter('', 26);
        $('#misCycle').button("disable");
        $('#chars').dataTable().fnDraw(true);
        
        MinCharIndex = 0;
        MaxCharIndex = $('#chars').dataTable().fnGetData().length - 1;
        charsGrpSelected = "";
    }
}

function fnDocButtonFilter(valFilter) {
    var sline;
    
    if(valFilter === undefined || valFilter === "")
        valFilter = "allDoc";
    
    if (valFilter != "allDoc") {
        $('#charDocs').dataTable().fnSettings()._iDisplayLength = -1;
        $('#charDocs').dataTable().fnDraw();
        $('#charDocs').dataTable().fnFilter(valFilter, 2);
        if (window.console) {
            console.log("righe filtrate:" + $('#charDocs tbody tr').length);
        }
    } else {
        $('#charDocs input').val("");
        $("#charDocs").dataTable().fnFilter('', 2);
        $('#charDocs').dataTable().fnDraw(true);
    }
}

function fnButtonFilterSHL() {
  if (document.getElementById("Setup").checked) {
    document.getElementById("ALL").focus();
    document.getElementById("ALL").click();
  }
  if (document.getElementById("Avvio").checked) {
    document.getElementById("Setup").focus();
    document.getElementById("Setup").click();
  }
  if (document.getElementById("Processo").checked) {
    document.getElementById("Avvio").focus();
    document.getElementById("Avvio").click();
  }
  if (document.getElementById("Delibera").checked) {
    document.getElementById("Processo").focus();
    document.getElementById("Processo").click();
  }
  if (document.getElementById("Altro").checked) {
    document.getElementById("Delibera").focus();
    document.getElementById("Delibera").click();
  }
}


function fnButtonFilterSHR() {
  if (document.getElementById("Delibera").checked) {
    document.getElementById("Altro").focus();
    document.getElementById("Altro").click();
  }
  if (document.getElementById("Processo").checked) {
    document.getElementById("Delibera").focus();
    document.getElementById("Delibera").click();
  }
  if (document.getElementById("Avvio").checked) {
    document.getElementById("Processo").focus();
    document.getElementById("Processo").click();
  }
  if (document.getElementById("Setup").checked) {
    document.getElementById("Avvio").focus();
    document.getElementById("Avvio").click();
  }
  if (document.getElementById("ALL").checked) {
    document.getElementById("Setup").focus();
    document.getElementById("Setup").click();
  }
}


function fnButtonCtrlDown() {
  if (CurrCharIndex < MaxCharIndex) {
    CurrCharIndex = CurrCharIndex + 1;
  } else {
    CurrCharIndex = MaxCharIndex;
  }
  if (CurrCharIndex < MinCharIndex) {
    CurrCharIndex = MinCharIndex;
  }

  var sline = $('#chars').dataTable().fnGetData(CurrCharIndex);
  fnSelectCharLine(sline);

  charSelected = CurrCharIndex;
}


function fnButtonCtrlUp() {
  if (CurrCharIndex > MinCharIndex) {
    CurrCharIndex = CurrCharIndex - 1;
  } else {
    CurrCharIndex = MinCharIndex;
  }
  if (CurrCharIndex > MaxCharIndex) {
    CurrCharIndex = MaxCharIndex;
  }

  var sline = $('#chars').dataTable().fnGetData(CurrCharIndex);
  fnSelectCharLine(sline);

  charSelected = CurrCharIndex;
}


function fnSelectCharLine(sline) {
  selChar = sline[0];
  if (sline[7] != "Si") {
    $('#misSingle').button("enable");
  }
  if (sline[18].length > 8) {
    selKeyMethod = sline[18];
    $('#getMet').button("enable");
  }

  // charSelected = sline;
  $("#selChar").val(sline[0]);
  $("#CharDesc").val(sline[1]);
  // CurrCharIndex = this._DT_RowIndex;
  if (sline[27].indexOf("FILE") > 0) {
    $('#getFile').button("enable");
  }
  if (sline[3] == "Misura") {
    $('#resChart').button("enable");
  }

}


function fnAskDeleteWorklistItem(id) {
  //  var  id3 = "00" + id;
  var id_elem = '#Li-a-' + (id - 1);
  var temp = $('#orderList').find(id_elem).text();

  //  [3956] User: giancarlo  Date: 01/12/2014  Purpose: managing of shortcut keys
  //var id3 = $('#orderList').find(id_elem)[0].outerHTML;
  var tmp_index = id3.search("listid") + 8;
  var id3 = id3.substring(tmp_index, tmp_index + 3);

  $("#PopupConfirm P").text("Eliminare l'elemento " + temp + "?");
  $("#PopupConfirm").dialog({
    resizable: false,
    height: 200,
    width: 500,
    modal: true,
    buttons: {
      Sì: function () {
        $(this).dialog("close");
        fnDelWLItem(id3);
      },
      No: function () {
        $(this).dialog("close");
      }
    }
  });
}