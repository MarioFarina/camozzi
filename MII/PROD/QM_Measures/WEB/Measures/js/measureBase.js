/* jshint -W117, -W098 */ 
// Script con funzionalità per acquisizione delle misure singole
//**  measureBase.js
//*******************************************
//**  Autore: Luca Adanti - IT-Link
//**
//**  Ver: 4.4.2
//**
//*******************************************
//** Ulteriori interventi di:
//** Giancarlo Pisoni
//** Bruno Rosati
//** Luca Adanti : last 02/08/2018
//** Antonio Masseretti : AM20190529
//** Antonio Masseretti : AM20191030 gestita chiusura notifiche
//*******************************************


// Funzione visualizzare messaggi di conferma
function fnSelectSingle(curRowChar, MultiArgs) {
    
   //Noty.closeAll(); AM20191030
    fmCloseMessagePopup();
    objPopupTimer.tAutoContinue.stop();
    objPopupTimer.tAutoContinue = $.timer(function () {
        objPopupTimer.function();
    }, 250, false);

	charSelected = curRowChar;
	var sline = $('#chars').dataTable().fnGetData(curRowChar);
	if (sline[0] !== "") {
		/* restituisce un array con i valori della riga intera */

		// tipo sline[3]
		// Obligatory = sline[10];
		// mChar = sline[11];
		minSamples = sline[5];
		CAT_TYPE = sline[13];
		PSEL_SET = sline[14];
		SEL_SET = sline[15];
		selKeyMethod = sline[18];
		selMethod = sline[6];
		selTolInf = sline[21];
		selTolSup = sline[20];
		selCharDesc = sline[1];

		$("#selChar").val(sline[0]);

		// User: giancarlo Date: 29/01/2014 Purpose: additional notes info in acq. popup
		//		 $("#XRnotes_text").text(fnGetInspCharNotes ($('#LotId').val(), $('#Oper').val(), $('#selChar').val()));
		//
        if(parseInt(sline[0]) > 999) {
            var sFirstRow = $('#chars').dataTable().fnGetData(0);
            var acceptDate = sFirstRow[26].substr(0, 10);
            var newAcceptDate = acceptDate.split("/").reverse().join("-");
            var bNotADate = isNaN(Date.parse(newAcceptDate));
            if (sFirstRow[3] === "Accettazione" && bNotADate) {
                fnMessageOK("Per poter registrare le misurazioni è necessario inserire il documento materiale/bolla materia prima", "Errore");
                return;
            }
        }
        
        if(parseInt(sline[0]) > 2999) {
            if (parseInt(sFirstRow[29]) != 0  && typeof(sFirstRow[29])  != "undefined" ) {
                fnMessageOK(sFirstRow[30], "Errore");
                return;
            }
        }
        
        if (sline[3] == "Accettazione" && sline[10] != "X") {
			fngetAccOrder("merci", MultiArgs);
		} else if (sline[3] == "Qualitativo") {
			$("#ordAcc P").text(sline[1] + " " + sline[6]);
			fncycleVal(0, MultiArgs);
		} else if (sline[3] == "Misura") {
			selChan = $.trim(sline[23].substr(1, 2));
			//AM20190529 if(selChan == 10
			if (selChan == 20 || sline[27].indexOf("FILE") > 0) {
				fngetMeasures(MultiArgs);
			} else {
				if (fnCheckTool(sline[27], sline[22])) {
					fngetMeasures(MultiArgs);
				}
			}
		} else { /* da catalogo */
			$("#ordAcc P").text(sline[1] + " " + sline[6]);
			fncycleQual(0, MultiArgs);
		}
	}
}

// Funzione per inserire il codice Accettazione e Ordine
function fngetAccOrder(mode, MultiArgs) {
	bfnCancel = false;
	var dTitle = "Accettazione merce";
	if (mode == "merci") {
        fnSetChan("0", "MANSTART", "");
		$("#ordAcc").find(".info-block").show();
		$("#ordAcc").find("P").hide();
		dTitle = selCharDesc; //"Accettazione merce / Note";

	} else {
		$("#ordAcc").find(".info-block").hide();
		$("#ordAcc").find("P").show();
		dTitle = "Valutazione qualitativa " + mode;
	}


	$("#accOrder").val("");
	var qXml = qBasePath + "GetCatListQR&Param.1=" + PSEL_SET;
	qXml = qXml + "&Param.2=" + SEL_SET;
	qXml = qXml + "&Param.3=" + CAT_TYPE;
	var data = fnGetAjaxData(qXml, false);
	var $catlist = $(data).find("Row");
	// configura la dialog
	$("#ordAcc").dialog({
		height: 200, //180,
		width: 550, //420,
		modal: true,
		title: dTitle,
		autoOpen: false,
		resizable: true,
		closeOnEscape: false,
		draggable: true
	});
	var lButtons = $("#ordAcc").dialog("option", "buttons");
	var cButtons = [];
	//Cicla per tutte le righe
	$catlist.each(function (index, cat) {
		var $cat = $(cat);
		var group = $cat.children('CODE_GROUP').text();
		var code = $cat.children('CODE').text();
		var ctxt = $cat.children('CODE_TEXT').text();
		var cret = $cat.children('CODE_VALUATION').text();
		ctxt = (index + 1) + ' - ' + ctxt; //[3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
		cButtons.push({
			id: "btn_conf_" + (index + 1), //[3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
			text: ctxt,
			click: function () {
                if (mode == "merci"){
                    fnSetChan("0", "MANEND", "");
                }
				//alert ( 'codice:' + code  + ' ritorno: ' + cret);
				fnSaveAcc(cret, group, code, $("#accOrder").val());
				$(this).dialog("close");
				fnDisableHK_AccettDialog(data); //[3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
				$("#ordAcc").dialog("destroy");
				if (MultiArgs !== null) {
					fnMultiModeSamples(MultiArgs);
				}
			}
		});
	});
	cButtons.push({
		id: "btn_cnl",
		text: "0 - Annulla",
		click: function () {
            if (mode == "merci"){
                fnSetChan("0", "MANEND", "");
            }
			bfnCancel = true;
			boolWork = false;
			$(this).dialog("close");
			fnDisableHK_AccettDialog(data); //[3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
		}
	});
	$("#ordAcc").dialog("option", "buttons", cButtons);

	/* Work-around per eventi touch */
	$("#btn_cnl").bind('touchend', function (e) {
		$("#btn_cnl").click();
		e.preventDefault();
		return;
	});

	$("#ordAcc").dialog("open");
	$catlist.each(function (index, cat) {
		/* Work-around per eventi touch */
		$("#btn_conf_" + (index + 1)).bind('touchend', function (e) {
			$("#btn_conf_" + (index + 1)).click();
			e.preventDefault();
			return;
		});

	});

	fnEnableHK_AccettDialog(data); //[3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
}

// Funzione per inserire la validazione da standard ricorsiva con accettato rifiutato
function fncycleVal(iCount, MultiArgs) {
	$("#ordAcc").find(".info-block").hide();
	$("#ordAcc").find("P").show();
	$("#accOrder").val("");
	bfnCancel = false;

	boolWork = true;
	$("#ordAcc").dialog({
		height: 180,
		width: 420,
		modal: true,
		title: "Valutazione qualitativa (" + (iCount + 1) + " di " + minSamples + ")",
		autoOpen: false,
		resizable: true,
		closeOnEscape: false,
		draggable: true,
		open: function () {
			objTimer.tAutoContinue.stop();
			console.debug("Timer " + objTimer.id + ": stopped");
		}
	});


	var cButtons = [];
	cButtons.push({
		id: "btnAcept",
		text: "Accetta",
		click: function () {
			fnSaveChars("A", $("#accOrder").val(), iCount + 1);
			if (iCount + 1 < minSamples) {
				$("#ordAcc").dialog("destroy");
				fncycleVal(iCount + 1, MultiArgs);

			} else {
				$(this).dialog("close");
				if (MultiArgs !== null) {
					fnMultiModeSamples(MultiArgs);
				}
			}
		}


	});
	cButtons.push({
		id: "btnReject",
		text: "Rifiuta",
		click: function () {
			fnSaveChars("R", $("#accOrder").val(), iCount + 1);
			if (iCount + 1 < minSamples) {
				$("#ordAcc").dialog("destroy");
				fncycleVal(iCount + 1, MultiArgs);

			} else {
				$(this).dialog("close");
				if (MultiArgs !== null) {
					fnMultiModeSamples(MultiArgs);
				}
			}
		}


	});
	cButtons.push({
		id: "btn_cnl",
		text: "Annulla",
		click: function () {
			bfnCancel = true;
			boolWork = false;
			$(this).dialog("close");
			if (MultiArgs !== null) {
				fnMultiModeSamples(MultiArgs);
			}
		}
	});
	$("#ordAcc").dialog("option", "buttons", cButtons);
	/* Work-around per eventi touch */
	$("#btn_cnl").bind('touchend', function (e) {
		$("#btn_cnl").click();
		e.preventDefault();
		return;
	});
	$("#btnReject").bind('touchend', function (e) {
		$("#btnReject").click();
		e.preventDefault();
		return;
	});
	$("#btnAcept").bind('touchend', function (e) {
		$("#btnAcept").click();
		e.preventDefault();
		return;
	});

	$("#ordAcc").dialog("open");

}

// Funzione per inserire la validazione da catalogo ricorsiva
function fncycleQual(iCount, MultiArgs) {
	bfnCancel = false;
	$("#ordAcc").find(".info-block").hide();
	$("#ordAcc").find("P").show();
	$("#accOrder").val("");
	boolWork = true;
	// configura la dialog
	$("#ordAcc").dialog({
		height: 180,
		width: 420,
		modal: true,
		title: "Valutazione qualitativa (" + (iCount + 1) + " di " + minSamples + ")",
		autoOpen: false,
		resizable: true,
		closeOnEscape: false,
		draggable: true
	});
	//if (iCount == 0) {
	//var lButtons = $( "#ordAcc" ).dialog("option", "buttons");
	var qXml = qBasePath + "GetCatListQR&Param.1=" + PSEL_SET;
	qXml = qXml + "&Param.2=" + SEL_SET;
	qXml = qXml + "&Param.3=" + CAT_TYPE;
	var data = fnGetAjaxData(qXml, false);
	var $catlist = $(data).find("Row");
	//Cicla per tutte le righe
	var cButtons = [];
	$catlist.each(function (index, cat) {
		var $cat = $(cat);
		var group = $cat.children('CODE_GROUP').text();
		var code = $cat.children('CODE').text();
		var ctxt = $cat.children('CODE_TEXT').text();
		var cret = $cat.children('CODE_VALUATION').text();
		cButtons.push({
			id: "btn_catalog_" + (index + 1),
			text: ctxt,
			click: function () {
				fnSaveSamples(cret, group, code, $("#accOrder").val(), iCount + 1);
				/*$( this ).dialog( "close" );
				$( "#ordAcc" ).dialog("destroy");*/
				if (iCount + 1 < minSamples) {
					$("#ordAcc").dialog("destroy");
					fncycleQual(iCount + 1, MultiArgs);

				} else {
					fnSaveSamplesComplete(cret, group, code, $("#accOrder").val(), iCount + 1);
					$(this).dialog("close");
					if (MultiArgs !== null) {
						fnMultiModeSamples(MultiArgs);
					}
				}
			}


		});

	});
	cButtons.push({
		id: "btn_cnl",
		text: "Annulla",
		click: function () {
			bfnCancel = true;
			$(this).dialog("close");
			if (MultiArgs !== null) {
				fnMultiModeSamples(MultiArgs);
			}
		}
	});
	$("#ordAcc").dialog("option", "buttons", cButtons);
	/* Work-around per eventi touch */
	$("#btn_cnl").bind('touchend', function (e) {
		$("#btn_cnl").click();
		e.preventDefault();
		return;
	});

	$("#ordAcc").dialog("open");
	$catlist.each(function (index, cat) {
		/* Work-around per eventi touch */
		$("#btn_catalog_" + (index + 1)).bind('touchend', function (e) {
			$("#btn_catalog_" + (index + 1)).click();
			e.preventDefault();
			return;
		});
	});


}

// Funzione per inserire misura collettiva
function fngetMeasures(MultiArgs) {

	fnClearMeasures();
    objTimer.tAutoContinue.stop();
	console.debug("Timer " + objTimer.id + ": stopped");

	bfnCancel = false;
	fnSetChan(selChan, "STARTM", $("#selChar").val());
	boolWork = true;
	var qXml = qBasePath + "GetCatListQR&Param.1=" + $.trim(localIP);
	$('#wsmis').dataTable().fnFilter('', 4);
	var misRefr = $.timer(function () {
		fnMeasData();
	}, 950, false);
	misRefr.play();

	//   [3956] User: giancarlo  Date: 10/07/2014  Purpose: managing of shortcut keys
	fnEnableHK_SingolaDialog();

	$("#misColl").dialog({
		resizable: false,
		title: "Acquisizione di " + minSamples + " misure: " + selCharDesc,
		height: 600,
		width: 650,
		modal: true,
		buttons: [{
				id: "del-mis",
				text: "Cancella misura",
				click: function () {
					//misRefr.pause();
					fnDelMis(misRefr);
					//misRefr.play();
				}
		},
			{
				id: "btnChannelMan",
				text: "Canale",
				click: function () {
					$("#dxpanel").toggle("slide", {
						direction: 'right'
					});
				}
							},
			{
				id: "man-mis", //[3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
				text: "1 - Manuale",
				click: function () {
					fnMeasManual(true, $("#selChar").val(), misRefr);
				}
							},
			{
				id: "button-ok",
				text: "2 - Salva",
				click: function () {
					misRefr.stop();
					if (bMeasCount) {
						$("#PopupConfirm P").text("Attenzione: misure in eccesso. Salvare ugualmente?");
						$("#PopupConfirm").dialog({
							resizable: false,
							height: 200,
							modal: true,
							buttons: [{
									id: "btn-miscancel-yes",
									text: "Sì",
									click: function () {
										$(this).dialog("close");
										fnDisableHK_SingolaDialog(); //[3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
										fnSaveMeasures();
										fnSetChan("0", "STOPM", "");
                                        
                                        objPopupTimer.tAutoContinue.play();
                                        
                                        $("#misColl").dialog("close");
										if (MultiArgs !== null) {
											fnMultiModeSamples(MultiArgs);
										}
									}
											},
								{
									id: "btn-miscancel-no",
									text: "No",

									click: function () {
										$(this).dialog("close");
                                        objPopupTimer.tAutoContinue.play();
										fnDisableHK_SingolaDialog(); //[3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
										misRefr.play();
									}
																}]
						});
						/* Work-around per eventi touch */
						$("#btn-miscancel-yes").bind('touchend', function (e) {
							$("#btn-miscancel-yes").click();
							e.preventDefault();
							return;
						});
						$("#btn-miscancel-no").bind('touchend', function (e) {
							$("#btn-miscancel-no").click();
							e.preventDefault();
							return;
						});




					} else {
						fnDisableHK_SingolaDialog(); //[3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
						fnSaveMeasures();
						fnSetChan("0", "STOPM", "");
                        
                        objPopupTimer.tAutoContinue.play();

						$(this).dialog("close");
						if (MultiArgs !== null) {
							fnMultiModeSamples(MultiArgs);
						}
					}
				}

							},
			{
				id: "button-esc", //[3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
				text: "3 - Annulla",
				click: function () {
					objTimer.tAutoContinue.stop();
					console.debug("Timer " + objTimer.id + ": stopped");
					bfnCancel = true;
					misRefr.stop();
					fnClearMeasures();
					fnSetChan("0", "STOPM", "");
					boolWork = false;
					$(this).dialog("close");
					fnDisableHK_SingolaDialog(); //[3956] User: giancarlo  Date: 25/11/2014  Purpose: managing of shortcut keys
					if (MultiArgs !== null && MultiArgs !== undefined) {
						fnMultiModeSamples(MultiArgs);
					}
                    objPopupTimer.tAutoContinue.play();
				}
							}
						 ]

	});
	$("#wsmis_filter").css("display", "none");
	$("#button-ok").button("disable");
	$("#del-mis").button("disable");
	/* Work-around per eventi touch */
	$("#del-mis").bind('touchend', function (e) {
		$("#del-mis").click();
		e.preventDefault();
		return;
	});
	$("#btnChannelMan").bind('touchend', function (e) {
		$("#btnChannelMan").click();
		e.preventDefault();
		return;
	});
	$("#man-mis").bind('touchend', function (e) {
		$("#man-mis").click();
		e.preventDefault();
		return;
	});
	$("#button-ok").bind('touchend', function (e) {
		$("#button-ok").click();
		e.preventDefault();
		return;
	});
	$("#button-esc").bind('touchend', function (e) {
		$("#button-esc").click();
		e.preventDefault();
		return;
	});
}

// Funzione per eliminare la misura errata
function fnDelMis(misRefr) {
	misRefr.pause();
	$("#PopupConfirm P").text("Eliminare la misura con ID = " + selMisID + "?");
	$("#PopupConfirm").dialog({
		resizable: false,
		height: 200,
		modal: true,
		buttons: [{
			id: "btnDelMisConfYes",
			text: "Sì",
			click: function () {
				var qreg = qDataPath + "ClearSingleItemQR&Param.1=MEAS_" + $.trim(localIP) + "&Param.2=" + selMisID;
				exeQuery(qreg, "", "Errore eliminazione misura.");
				misRefr.play();
				$(this).dialog("close");
			}
		}, {
			id: "btnDelMisConfNo",
			text: "No",

			click: function () {
				$(this).dialog("close");
				misRefr.play();
			}
		}]
	});
	/* Work-around per eventi touch */
	$("#btnDelMisConfYes").bind('touchend', function (e) {
		$("#btnDelMisConfYes").click();
		e.preventDefault();
		return;
	});
	$("#btnDelMisConfNo").bind('touchend', function (e) {
		$("#btnDelMisConfYes").click();
		e.preventDefault();
		return;


	});
}

// Funzione per cancellare le misure
function fnClearMeasures() {
    var qreg = qDataPath + "ClearWsInfoQR&Param.1=" + "MEAS_" + $.trim(currentIP);
	exeQuery(qreg, "", "");
	$('#wsmis').dataTable().fnClearTable();
}


// Funzione per salvare in SAP il campione qualitativo da catalogo per accettazione merci
function fnSaveAcc(Eval, Group, Code, Remark) {
	if ($("#Approv").val() === "" && document.getElementById("Avvio").checked === true) // User: giancarlo  Date: 10/04/2017	[8017]
	{
		console.debug("Avvio il benestare da fnSaveAcc");
		$("#cid-approv").dialog({
			resizable: false,
			position: [((window.innerWidth / 2) - 150), 10],
			height: 180,
			width: 300,
			modal: true,
			closeOnEscape: false,
			buttons: {
				OK: function () {
					if (fnCheckCID_approv($("#txtCID_approv").val())) {
						$("#Approv").val(fnCheckApprov($("#LotId").val(), $("#Oper").val(), $("#txtCID_approv").val()));
						fnSaveAcc_commit(Eval, Group, Code, Remark);
						$(this).dialog("close");
					}
				}
			}
		});
	} else {
		fnSaveAcc_commit(Eval, Group, Code, Remark);
	}
}

function fnSaveAcc_commit(Eval, Group, Code, Remark) {
	var params = qBasePath + "SaveSamplesAccQR&Param.1=" + $("#LotId").val();
	params = params + "&Param.2=" + $("#Oper").val();
	params = params + "&Param.3=" + $("#selChar").val();
	params = params + "&Param.4=" + currentCID;
	params = params + "&Param.5=" + Eval;
	params = params + "&Param.6=" + Group;
	params = params + "&Param.7=" + Code;
	params = params + "&Param.8=" + Remark;
	params = params + "&Param.9=0";
    
    let ord = $("#Order").val();
    let oper = $("#Oper").val();
    let matid = $("#Material").val();
    let matdesc = $("#MatDesc").val();
    let qty = $("#Qty").val();
    let lotID = $("#LotId").val();

	var result = fnGetAjaxData(params, false);
	if ($(result).find('Row').children("CodeResult").text() == "00") {
		fnMessageOK("Operazione completata con successo");
        fnDisplayOrder(Number(ord), oper, lotID, matid, matdesc.replace(/\"/g, '#'), qty, true);
	} else {
		fnMessageOK($(result).find('Row').children("CodeDescription").text());
	}
	fnClearState();
	boolWork = false;
}


// Funzione per salvare in SAP il campione qualitativo da catalogo
function fnSaveSamples(Eval, Group, Code, Remark, cCount) {
	if ($("#Approv").val() === "" && document.getElementById("Avvio").checked === true) // User: giancarlo  Date: 10/04/2017	[8017]
	{
		console.debug("Avvio il benestare da fnSaveSamples");
		$("#cid-approv").dialog({
			resizable: false,
			position: [((window.innerWidth / 2) - 150), 10],
			height: 180,
			width: 300,
			modal: true,
			closeOnEscape: false,
			buttons: {
				OK: function () {
					if (fnCheckCID_approv($("#txtCID_approv").val())) {
						$("#Approv").val(fnCheckApprov($("#LotId").val(), $("#Oper").val(), $("#txtCID_approv").val()));
						fnSaveSamples_commit(Eval, Group, Code, Remark, cCount);
						$(this).dialog("close");
					}
				}
			}
		});
	} else {
		fnSaveSamples_commit(Eval, Group, Code, Remark, cCount);
	}
}

function fnSaveSamples_commit(Eval, Group, Code, Remark, cCount) {
	var params = qBasePath + "SaveSamplesQR&Param.1=" + $("#LotId").val();
	params = params + "&Param.2=" + $("#Oper").val();
	params = params + "&Param.3=" + $("#selChar").val();
	params = params + "&Param.4=" + currentCID;
	params = params + "&Param.5=" + Eval;
	params = params + "&Param.6=" + Group;
	params = params + "&Param.7=" + Code;
	params = params + "&Param.8=" + Remark;
	params = params + "&Param.9=" + cCount;
    
    let ord = $("#Order").val();
    let oper = $("#Oper").val();
    let matid = $("#Material").val();
    let matdesc = $("#MatDesc").val();
    let qty = $("#Qty").val();
    let lotID = $("#LotId").val();

	var result = fnGetAjaxData(params, false);
	if ($(result).find('Row').children("CodeResult").text() == "00") {
		fnMessageOK("Operazione completata con successo");
        fnDisplayOrder(Number(ord), oper, lotID, matid, matdesc.replace(/\"/g, '#'), qty, true);
	} else {
		fnMessageOK($(result).find('Row').children("CodeDescription").text());
	}
	// if ($(result).find('Row').children("CodeResult").text() == "00") fnMessageOK("Operazione completata con successo");
	//else fnMessageOK($(result).find('Row').children("CodeDescription").text());
	fnClearState();
	boolWork = false;
}


// Funzione per salvare in SAP il campione qualitativo da standard senza catalogo
function fnSaveChars(Eval, Remark, cCount) {
	if ($("#Approv").val() === "" && document.getElementById("Avvio").checked === true) // User: giancarlo  Date: 10/04/2017	[8017]
	{ console.debug("Avvio il benestare da fnSaveChars");
		$("#cid-approv").dialog({
			resizable: false,
			position: [((window.innerWidth / 2) - 150), 10],
			height: 180,
			width: 300,
			modal: true,
			closeOnEscape: false,
			buttons: {
				OK: function () {
					if (fnCheckCID_approv($("#txtCID_approv").val())) {
						$("#Approv").val(fnCheckApprov($("#LotId").val(), $("#Oper").val(), $("#txtCID_approv").val()));
						fnSaveChars_commit(Eval, Remark, cCount);
						$(this).dialog("close");
					}
				}
			}
		});
	} else {
		fnSaveChars_commit(Eval, Remark, cCount);
	}
}

function fnSaveChars_commit(Eval, Remark, cCount) {
    var params = qBasePath + "SaveCharsQR&Param.1=" + $("#LotId").val();
	params = params + "&Param.2=" + $("#Oper").val();
	params = params + "&Param.3=" + $("#selChar").val();
	params = params + "&Param.4=" + currentCID;
	params = params + "&Param.5=" + Eval;
	params = params + "&Param.6=" + Remark;
	params = params + "&Param.7=" + cCount;
    
    let ord = $("#Order").val();
    let oper = $("#Oper").val();
    let matid = $("#Material").val();
    let matdesc = $("#MatDesc").val();
    let qty = $("#Qty").val();
    let lotID = $("#LotId").val();

	var result = fnGetAjaxData(params, false);

	if ($(result).find('Row').children("CodeResult").text() == "00") {
		fnMessageOK("Operazione completata con successo");
        fnDisplayOrder(Number(ord), oper, lotID, matid, matdesc.replace(/\"/g, '#'), qty, true);
	} else {
		fnMessageOK($(result).find('Row').children("CodeDescription").text());
	}
	//    params =  qBasePath + "GetCharQR&Param.1=" + $( "#LotId" ).val() + "&Param.2=" + $("#Oper").val();
	params = qBasePath + "GetCharQR_opti&Param.1=" + $("#LotId").val() + "&Param.2=" + $("#Oper").val(); //User: giancarlo Date: 25/10/2016
	fnGetXMLData(params, '#chars', true, true);
	fnClearState();
	boolWork = false;
}


// Funzione per salvare in SAP le misure
function fnSaveMeasures() {
	if ($("#Approv").val() === "" && document.getElementById("Avvio").checked === true) // User: giancarlo  Date: 10/04/2017	[8017]
	{ console.debug("Avvio il benestare da fnSaveMeasures");
		objTimer.tAutoContinue.stop();
		$("#cid-approv").dialog({
			resizable: false,
			position: [((window.innerWidth / 2) - 150), 10],
			height: 180,
			width: 300,
			modal: true,
			closeOnEscape: false,
			buttons: {
				OK: function () {
					if (fnCheckCID_approv($("#txtCID_approv").val())) {
						$("#Approv").val(fnCheckApprov($("#LotId").val(), $("#Oper").val(), $("#txtCID_approv").val()));
						fnSaveMeasures_commit();
						$(this).dialog("close");
					}
				}
			}
		});
	} else {
		objTimer.tAutoContinue.play();
		console.debug("Timer " + objTimer.id + ": on fnSaveMeasures");
		fnSaveMeasures_commit();
	}
}

function fnSaveMeasures_commit() {
	var params = qBasePath + "SaveMesauresQR&Param.1=" + $("#LotId").val();
	params = params + "&Param.2=" + $("#Oper").val();
	params = params + "&Param.3=" + $("#selChar").val();
	params = params + "&Param.4=" + currentCID;
	params = params + "&Param.5=" + $.trim(localIP);
    
    let ord = $("#Order").val();
    let oper = $("#Oper").val();
    let matid = $("#Material").val();
    let matdesc = $("#MatDesc").val();
    let qty = $("#Qty").val();
    let lotID = $("#LotId").val();
    
    var result = fnGetAjaxData(params, false);
        
	if ($(result).find('Row').children("CodeResult").text() == "00") {
		fnMessageOK($(result).find('Row').children("CodeDescription").text());
        fnDisplayOrder(Number(ord), oper, lotID, matid, matdesc.replace(/\"/g, '#'), qty, true);
	} else {
		fnMessageOK($(result).find('Row').children("CodeDescription").text());
	}
    
	fnClearState();
    objPopupTimer.tAutoContinue.play();
	//    params =  qBasePath + "GetCharQR&Param.1=" + $( "#LotId" ).val() + "&Param.2=" + $("#Oper").val();
	params = qBasePath + "GetCharQR_opti&Param.1=" + $("#LotId").val() + "&Param.2=" + $("#Oper").val(); //User: giancarlo  Date: 25/10/2016
	fnGetXMLData(params, '#chars', true, true);
	boolWork = false;
}


// User: giancarlo Date: 29/01/2014 Purpose: additional notes info in acq. popup

function fnGetInspCharNotes(LotId, Oper, SelChar) {

	var qParams = qBasePath + "GetSAPQMParametersQR&Param.1=" + LotId;
	qParams += "&Param.2=" + Oper;
	qParams += "&Param.3=" + SelChar;

	var data = fnGetAjaxData(qParams, false);

	return $(data).find("Row").children('TNOTES').text();
}


//	User: giancarlo  Date: 10/04/2017	[8017]		--------------------

// Funzione Richiesta codice operatore per benestare a produrre
function fnCheckApprov(i_lot, i_oper, i_setval) {
	var qParams = "";
	if (i_setval === '') {
		qParams = qBasePath + "GetProdOrdApprovQR&Param.1=" + i_lot;
		qParams = qParams + "&Param.2=" + i_oper;

		var approved_saved = "";
		var data = fnGetAjaxData(qParams, false);
		approved_saved = $(data).find("Row").children('APPROVED_BY').text();
		return approved_saved;
	}
	qParams = qBasePath + "SetProdOrdApprovQR&Param.1=" + i_lot;
	qParams = qParams + "&Param.2=" + i_oper;
	qParams = qParams + "&Param.3=" + currentCID;
	qParams = qParams + "&Param.4=" + i_setval;
	fnGetAjaxData(qParams, false);

	return i_setval;
}


function fnCheckCID_approv(CID) {
	var qXml = qDataPath + "GetSAPEmployeeAcknoQR&Param.1=" + CID;
	var rCid = fnGetAjaxVal(qXml, "MESSAGE_RET", false);

	if (rCid === undefined) {
		fnMessageOK("Attenzione: CID non corretto!");
		return false;
	}

	if ($.trim(rCid) !== "") {
		fnMessageOK("Attenzione: CID non corretto!");
		return false;
	} else {
		return CID;
	}
}

//	--------------------------------------------------------------------------------------------
