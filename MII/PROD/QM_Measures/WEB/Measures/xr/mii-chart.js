
var $ = jQuery.noConflict();
var qBasePath = "Content-Type=text/XML&QueryTemplate=QM_Measures/Measures/";
var XMLset;

//

var Lot_ID = location.search.split('lot_id=')[1].split('&')[0];	
//alert(Lot_ID);

var Oper_ID = location.search.split('oper_id=')[1].split('&')[0];
//alert(Oper_ID);

var Char_ID = location.search.split('char_id=')[1];	
//alert(Char_ID);

///

var qParams = qBasePath + "GetSAPQMParametersQR&Param.1=" + Lot_ID;
qParams += "&Param.2=" + Oper_ID;
qParams += "&Param.3=" + Char_ID;
var data = fnGetAjaxData (qParams, false );

var Cp = parseFloat($(data).find("Row").children('CP').text()).toFixed(3).replace(".", ",");
var Cpk = parseFloat($(data).find("Row").children('CPK').text()).toFixed(3).replace(".", ",");  //$("#idCPK").text
var Cm = parseFloat($(data).find("Row").children('CM').text()).toFixed(3).replace(".", ",");  //$("#idCM").text
var Cmk = parseFloat($(data).find("Row").children('CMK').text()).toFixed(3).replace(".", ",");  //$("#idCMK").text
var Tnat  = parseFloat($(data).find("Row").children('TNAT').text()).toFixed(3).replace(".", ",");  //$("#idTnat").text

var Rmean = parseFloat($(data).find("Row").children('RMEAN').text()).toFixed(3); //.replace(".", ",");  //$("#idRMed").text
var Xmean = parseFloat($(data).find("Row").children('XMEAN').text()).toFixed(3); //.replace(".", ",");  //$("#idXMed").text

var UpInnerLim = parseFloat($(data).find("Row").children('UP_LMT_1').text()).toFixed(3); //.replace(".", ","); //$("#pLimCtrlSup").text
var LowInnerLim = parseFloat($(data).find("Row").children('LW_LMT_1').text()).toFixed(3); //.replace(".", ","); //$("#pLimCtrlInf").text
var UpLim = parseFloat($(data).find("Row").children('UP_TOL_LMT').text()).toFixed(3);	//.replace(".", ","); //$("#pLimCtrlSup").text
var LowLim = parseFloat($(data).find("Row").children('LW_TOL_LMT').text()).toFixed(3); //.replace(".", ","); //$("#pLimCtrlInf").text
var TargetVal = parseFloat($(data).find("Row").children('TARGET_VAL').text()).toFixed(3); //.replace(".", ","); //$("#pLimCtrlInf").text

var OrderNum = parseFloat($(data).find("Row").children('PRODORD_NUM').text());
var MatNum = $(data).find("Row").children('MATERIAL_ID').text();
var MatDesc = $(data).find("Row").children('MATERIAL_DESC').text();
var OperDesc = $(data).find("Row").children('OPERATN_DESC').text();
var CharDesc = $(data).find("Row").children('CHAR_DESCR').text();

////

var T1   = "Carta XR - Camozzi S.p.A. - Ufficio Tecnico di Lumezzane";
var T2   = "Ordine di Produzione: " + OrderNum + " / " +  Oper_ID + " - Car.: " + Char_ID  + "- " + CharDesc;
var T3   = "Materiale: " + MatNum + " - " + MatDesc;
var T4   = "Cp: " + Cp + " - Cpk: " + Cpk;
//var T5   = "Cm: " + Cm + " - Cmk: " + Cmk + " - Tnat = " + Tnat;

var ordP     = OrderNum + " / " +  Oper_ID;
var codArt   = MatNum;
var descArt  = MatDesc;
var caratt   = Char_ID;
var descrCar = CharDesc;

var today  = new Date();
var dataStampa = today.toLocaleDateString("it-IT");

///

qParams = qBasePath + "GetResultsChartQR&Param.1=" + Lot_ID;
qParams += "&Param.2=" + Oper_ID;
qParams += "&Param.3=" + Char_ID;
var dataXML = fnGetAjaxData (qParams, false );

///
var pslti = dataXML.getElementsByTagName("PSLTI")[0].textContent;
var pslts = dataXML.getElementsByTagName("PSLTS")[0].textContent;
var pft = dataXML.getElementsByTagName("PFT")[0].textContent;
var pet = dataXML.getElementsByTagName("PET")[0].textContent;

var T5   = "Cm: " + Cm + " - Cmk: " + Cmk + " - Tnat = " + Tnat + " - Pslti = " + pslti + " - Pslts = " + pslts + " - Pft = " + pft + " - Pet = " + pet;

var AXD  = []; //[["", "13/01/2017", "11:07"],["", "13/01/2017", "11:10"],["", "13/01/2017", "11:13"],["", "13/01/2017", "11:16"],["", "13/01/2017", "11:20"],["", "13/01/2017", "11:25"],["", "13/01/2017", "11:30"],];
XMLset = dataXML.getElementsByTagName("MEAS_DATE");
var XMLset2 = dataXML.getElementsByTagName("MEAS_TIME");
$(XMLset).each(function(index, event)  { AXD.push( 
[ "", "",
index + 1, 
$(event).text().substr(0,5),
XMLset2[index].innerHTML.substr(0,5)
] 
)}); 					//parseFloat($(event).text())).toFixed();});


var AY1L = "Xm = " + Xmean;
var AY1D = [];
XMLset = dataXML.getElementsByTagName("XDATA");
$(XMLset).each(function(index, event)  {AY1D.push($(event).text());});


var AY1T = [];
XMLset = dataXML.getElementsByTagName("SAMPLE_NOTES");
$(XMLset).each(function(index, event)  {AY1T.push($(event).text());});


var AY2L = "LTS = " + UpLim;
var AY2D = [];		//[13.20, 13.20, 13.20, 13.20, 13.20, 13.20, 13.20];
XMLset = dataXML.getElementsByTagName("INSPSAMPLE");
$(XMLset).each(function(index, event)  {AY2D.push(UpLim);});


var AY3L = "LCS = " + UpInnerLim;
var AY3D = [];		//[13.19, 13.19, 13.19, 13.19, 13.19, 13.19, 13.19];
XMLset = dataXML.getElementsByTagName("INSPSAMPLE");
$(XMLset).each(function(index, event)  {AY3D.push(UpInnerLim);});


var AY4L = "Val.rif = " + TargetVal;
var AY4D = [];		//[13.15, 13.15, 13.15, 13.15, 13.15, 13.15, 13.15];
//XMLset = dataXML.getElementsByTagName("INSPSAMPLE");
$(XMLset).each(function(index, event)  {AY4D.push(TargetVal);});

var AY5L = "LCI = " + LowInnerLim;
var AY5D = [];		//13.11, 13.11, 13.11, 13.11, 13.11, 13.11, 13.11];
//XMLset = dataXML.getElementsByTagName("INSPSAMPLE");
$(XMLset).each(function(index, event)  {AY5D.push(LowInnerLim);});


var AY6L = "LTI = " + LowLim;		//13,10";
var AY6D = [];		//13.10, 13.10, 13.10, 13.10, 13.10, 13.10, 13.10];
//XMLset = dataXML.getElementsByTagName("INSPSAMPLE");
$(XMLset).each(function(index, event)  {AY6D.push(LowLim);});


var BXD  = AXD;


var BY1L = "Rm = " + Rmean;
var BY1D = [];	//[0.03, 0.04, 0.03, 0.06, 0.05, 0.07, 0.04];
XMLset = dataXML.getElementsByTagName("RDATA");
$(XMLset).each(function(index, event)  {BY1D.push($(event).text());});


var BY2L = "LCS = " + ((UpLim - LowLim)/2).toFixed(2);		//= 0,07";
var BY2D = [];	//0.07, 0.07, 0.07, 0.07, 0.07, 0.07, 0.07];
//XMLset = dataXML.getElementsByTagName("RDATA");
$(XMLset).each(function(index, event)  {BY2D.push((UpLim - LowLim)/2).toFixed(2);});

var CXD  = [];	//"13,11", "13,12", "13,13", "13,14", "13,15", "13,16", "13,17",];
XMLset = dataXML.getElementsByTagName("XDATA_B");
$(XMLset).each(function(index, event)  {CXD.push($(event).text());});

var CY1L = [];
CY1L.push( 
    [ "Tnat = " + Tnat, "Pslti = " + pslti,
    "Pslts = " + pslts, "Pft = " + pft,
    "Pet = " + pet
    ] 
);
//var CY1L =  "Tnat = " + Tnat;
var CY1D = [];	//2, 8, 13, 20, 20, 8, 1];
XMLset = dataXML.getElementsByTagName("DCOUNT_B");
$(XMLset).each(function(index, event)  {CY1D.push($(event).text());});



		window.chartColors = {
			dred: 'rgb(255, 0, 0)',
			red: 'rgb(255, 132, 132)',
			orange: 'rgb(255, 159, 64)',
			yellow: 'rgb(255, 205, 86)',
			green: 'rgb(54, 235, 162)',
			blue: 'rgb(54, 162, 235)',
			dblue: 'rgb(0, 0, 255)',
			purple: 'rgb(153, 102, 255)',
			grey: 'rgb(231,233,237)',
			black: 'rgb(128,128,128)'
		};	
	
	
////////////////////////////	
	
        var config = {
            type: 'line',
            data: {
                labels: AXD,
                datasets: [{
                    label: AY1L,	
                    fill: false,
                    backgroundColor: window.chartColors.black,
                    borderColor: window.chartColors.black,
					lineTension: 0,
                    pointRadius: 5,
                    pointHoverRadius: 8,					
                    data: AY1D,
                }, {
                    label: AY2L,
                    fill: false,
                    backgroundColor: window.chartColors.dred,
                    borderColor: window.chartColors.dred,
                    borderDash: [5, 5],
                    pointRadius: 0,
                    pointHoverRadius: 0,					
                    data: AY2D,
                }, {
                    label: AY3L,
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    borderDash: [5, 5],
                    pointRadius: 0,
                    pointHoverRadius: 0,					
                    data: AY3D,
                }, {
                    label: AY4L,
                    fill: false,
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    borderDash: [5, 5],
                    pointRadius: 0,
                    pointHoverRadius: 0,					
                    data: AY4D,
                }, {
                    label: AY5L,
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    borderDash: [5, 5],
                    pointRadius: 0,
                    pointHoverRadius: 0,					
                    data: AY5D,
                }, {
                    label: AY6L,
                    fill: false,
                    backgroundColor: window.chartColors.dred,
                    borderColor: window.chartColors.dred,
                    borderDash: [5, 5],
                    pointRadius: 0,
                    pointHoverRadius: 0,					
                    data: AY6D,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display: true,
                    text: T4,
                },
                legend: {
                    position: 'right',
                },				
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Data/Ora'
                        },
		ticks: { 
			 minRotation: 0, // ML 15/05/2019 
		   	 maxRotation: 90 // ML 15/05/2019
		       }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Xm'
                        }
             	
                    }]
                }
            }
        };
		
///////////////////////////////	


        var config2 = {
            type: 'line',
            data: {
                labels: BXD,
                datasets: [{
                    label: BY1L,
                    fill: false,
                    backgroundColor: window.chartColors.black,
                    borderColor: window.chartColors.black,
					lineTension: 0,
                    pointRadius: 5,
                    pointHoverRadius: 8,					
                    data: BY1D,
                }, {
                    label: BY2L,
                    fill: false,
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    borderDash: [5, 5],
                    pointRadius: 0,
                    pointHoverRadius: 0,					
                    data: BY2D,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display: false,
                    text:'Carta XR',
                },
                legend: {
					display: true,
                    position: 'right',
                },				
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Data/Ora'
                        },
 		  ticks: { 
			 minRotation: 0, // ML 15/05/2019 
		   	 maxRotation: 90 // ML 15/05/2019
		       }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Rm'
                        },
						ticks: {
                            min: 0
		    
                        }
                    }]
                }
            }
        };


////////////////////////////////////////////////////////////////////		


       var config3 = {
            type: 'bar',
            data: {
                labels: CXD,
                datasets: [{
                    label: CY1L,
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    data: CY1D,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display: true,
                    text: T5,
                },
                legend: {
					display: false,
                    position: 'right',
                },				
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Xm'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Count'
                        },
			ticks: {
                            min: 0
 	              
                        }
                    }]
                }
            }
        };
		
		
////////////////////////////////////////////////////////////////////		


// Extra notes managing

        Chart.plugins.register({
            afterDatasetsDraw: function(chartInstance, easing) {
                // To only draw at the end of animation, check for easing === 1
				var ctx = chartInstance.chart.ctx;
				var dataset = chartInstance.data[0];

				var meta = chartInstance.getDatasetMeta(0);
                if (!meta.hidden) {
					
					AY1T.forEach(function(ptlabel, index) {
						var element = meta.data[index];
                        // Draw the text with the specified font
                        ctx.fillStyle = 'rgb(255, 0, 0)';

                        var fontSize = 12; /* ML 15/05/2019 */
                        var fontStyle = 'bold';
                        var fontFamily = 'Arial Black';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                        // Just naively convert to string for now
                        var dataString = "";
						if (ctx.canvas.id == "chart-x")
						{
						  dataString = ptlabel;
						}

                        // Make sure alignment settings are correct
                        ctx.textAlign = 'left';	//'center';
                        ctx.textBaseline = 'middle';

                        var padding = 5;
		if (element != null) {
                        var position = element.tooltipPosition();
                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding); }
                    });
                }
            }
        }); 


function fnSaveAddText (i_lot, i_oper, i_char, i_sample, i_text )
{
//	alert(i_lot + " " + i_oper + " " + i_char + " " + i_sample + " " + i_text);

var qParams = qBasePath + "SetXRNoteQR&Param.1=" + i_lot;
qParams += "&Param.2=" + i_oper;
qParams += "&Param.3=" + i_char;
qParams += "&Param.4=" + i_sample;
qParams += "&Param.5=" + i_text;

if ( i_sample != "") {
	var data = fnGetAjaxData (qParams, false );
   }

}
	

////


function fnGetAjaxData (query, basync ) {
var DateRet;
$.ajax({
"url": "/XMII/Illuminator",
"data": query ,
 "async": basync,	//User: giancarlo  Date: 24/10/2016
// "async": true,	//User: giancarlo  Date: 24/10/2016
"dataType": "xml",
"type":"POST",
"cache": false,
"success": function(data, textStatus){
DateRet = data;
}
})
	.fail(function() {
//	fnMessageOK("Premere CTRL+F5 per aggiornare la pagina.", "MII");
//	fnMessageOK("Errore di comunicazione con il server MII. Verificare la connettività", "Errore di comunicazione");
});
return DateRet;
}
		
		
		
		
		



