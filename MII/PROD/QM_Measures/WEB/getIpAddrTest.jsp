
<%@ page import="java.io.*,java.util.*" %>

<%
    response.setHeader("Pragma", "No-cache");
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.setDateHeader("Expires", 0);
%>
<html>
<head>
<title>HTTP Header Request Example</title>
</head>
<body>
<center>
<h2>HTTP Header Request Example</h2>
<table width="100%" border="1" align="center">
<tr bgcolor="#949494">
<th>Header Name</th><th>Header Value(s)</th>
</tr>
<%

   Enumeration headerNames = request.getHeaderNames();
   while(headerNames.hasMoreElements()) {
      String paramName = (String)headerNames.nextElement();
      out.print("<tr><td>" + paramName + "</td>\n");
      String paramValue = request.getHeader(paramName);
      out.println("<td> " + paramValue + "</td></tr>\n");


   }


  out.print("<tr><td>RemoteHost</td>\n");
    String IP = request.getRemoteHost();
  out.print("<td> " + IP + "</td></tr>\n");
  out.print("<tr><td>Gatway</td>\n");
    String IP2 = request.getHeader("VIA") ;
  out.print("<td> " + IP2 + "</td></tr>\n");
  out.print("<tr><td>X-Forwarded-For</td>\n");
        String IP3 = request.getHeader("X-Forwarded-For");
  out.print("<td> " + IP3 + "</td></tr>\n");
    out.print("<tr><td>Proxy-Client-IP</td>\n");
        IP3 = request.getHeader("Proxy-Client-IP");
  out.print("<td> " + IP3 + "</td></tr>\n");
    out.print("<tr><td>WL-Proxy-Client-IP</td>\n");
        IP3 = request.getHeader("WL-Proxy-Client-IP");
  out.print("<td> " + IP3 + "</td></tr>\n");
    out.print("<tr><td>HTTP_CLIENT_IP</td>\n");
        IP3 = request.getHeader("HTTP_CLIENT_IP");
  out.print("<td> " + IP3 + "</td></tr>\n");
    out.print("<tr><td>X-HTTP_X_FORWARDED_FOR</td>\n");
        IP3 = request.getHeader("HTTP_X_FORWARDED_FOR");
  out.print("<td> " + IP3 + "</td></tr>\n");
    out.print("<tr><td>RemoteAddr</td>\n");
        IP3 = request.getRemoteAddr(); 
  out.print("<td> " + IP3 + "</td></tr>\n");
            //RETURNS GATEWAY     

%>
</table>
</center>
</body>
</html>