// Variabili globali a livello pagina
var localIP = "";
var currentIP = "";
var currentID_oper = "";
var qBasePath = "Content-Type=text/XML&QueryTemplate=QM_Measures/DataCollector/";
var wsTable;
var cronoTable;
var misTable;

// Funzione per confertire i dati XML in formato JSON per le tabelle DataTables
function fnGetJSONData ( sSource, aoData, fnCallback, oSettings ) {
     var params = {};
     
     for ( var i=0 ; i <aoData.length ; i++ ) {
          var entry = aoData[i];
          params[entry.name] = entry.value;
     }

     $.ajax({
          "url": "/XMII/Illuminator" ,
          "dataType": "xml",
          "data": sSource,
          "type": "POST",
          "success": function(data){
               var jData = $( data );					
               var json = {"sEcho": params["sEcho"],"aaData" : []};
               json.iTotalRecords = jData.find("[nodeName='opensearch:totalResults']").text();
               json.iTotalDisplayRecords = json.iTotalRecords;		
               
               var $events = $(data).find("Row");
               $events.each(function(index, event){
                    var $event = $(event),
                    addData = []; 
                    $event.children().each(function(i, child){
                         addData.push($(child).text());
                    });
                    json.aaData.push(addData);   
                    json.iTotalRecords = index;
                    //aoData.push(addData);    
               });
          json.iTotalDisplayRecords = json.iTotalRecords+1;		
          fnCallback(json);
          }
     });
}

// Funzione per esecuzione Query MII centralizzata
function exeQuery (qParams, tSuccess, tError ) {
     $.ajax({
     "url": "/XMII/Illuminator",
     "data": qParams ,
     "dataType": "xml",
     "type":"POST",
     "cache": false,
     "complete": function(data, textStatus){
     if (textStatus == "success")  fnMessageOK(tSuccess);
     else  fnMessageOK(tError);
     }
     });     
}

// Funzione per recuperare la lista postazioni
function fnWSList ( ) {
     wsTable = $('#wslist').dataTable( {
	     "bFilter": false,
	     "bInfo": false,
               "bDestroy":true,
               "bProcessing": true,
               "bServerSide": true,
               "bJQueryUI": true,
               "bSort": false,
               "bPaginate": true,
               "bStateSave": true,
               "sAjaxSource": qBasePath + "GetWsListQR",
               "fnServerData": fnGetJSONData,
	     "oLanguage": {
	      "sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
          });
}

// Funzione per recuperare i dati della cronologia stati
function fnCronoData ( IP ) {
     var params =  qBasePath + "GetWsCronoQR&Param.1=" + IP;

     $('#wscrono').dataTable( {
	"bFilter": false,
	"bInfo": false,
          "bDestroy":true,
          "bProcessing": true,
          "bServerSide": true,
          "bJQueryUI": true,
          "bSort": false,
          "bPaginate": true,
          "bStateSave": true,
          "sAjaxSource": params,
          "fnServerData": fnGetJSONData,
          "oLanguage": {
          "sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
     } );
}

// Funzione per recuperare i dati della lista misure
function fnMeasData ( IP ) {
     var params =  qBasePath + "GetMisValueListQR&Param.1=" + IP;

     $('#wsmis').dataTable( {
	"bFilter": false,
	"bInfo": false,
	"bDestroy":true,
	"bProcessing": true,
	"bServerSide": true,
	"bJQueryUI": true,
	"bSort": false,
	"bPaginate": true,
	"bStateSave": true,
	"sAjaxSource": params,
	"fnServerData": fnGetJSONData,
	"oLanguage": {
	"sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
     } );
}

// Funzione per registrare la misura manuale
function fnRegMis (  ) {
     var qreg = qBasePath + "SetMisValueQR&Param.1=" + $.trim(localIP) + "&Param.2=" + $('#txtValue').val();
     qreg = qreg +"&Param.3=" + $('#txtChannel').val()
     /* $.ajax({
          "url": "/XMII/Illuminator",
          "data": qreg ,
          "dataType": "xml",
          "type":"POST",
          "cache": false,
          "success": function(data){
          alert("misura inviata");
          }
     }); */
     exeQuery (qreg, "Misura inviata", "Errore in invio misura" )
}


// check me!


// Funzione per recuperare i dati della lista misure
function fnOperData ( IP ) {
     var params =  qBasePath + "GetOperValueListQR&Param.1=" + IP;

     $('#wsoper').dataTable( {
	"bFilter": false,
	"bInfo": false,
	"bDestroy":true,
	"bProcessing": true,
	"bServerSide": true,
	"bJQueryUI": true,
	"bSort": false,
	"bPaginate": true,
	"bStateSave": true,
	"sAjaxSource": params,
	"fnServerData": fnGetJSONData,
	"oLanguage": {
	"sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
     } );
}

// Funzione per inserire manualmente una operazione
function fnRegOper (  ) {
     var qreg = qBasePath + "SetOperValueQR&Param.1=" + $.trim(currentIP) + "&Param.2=" + $('#txtOrderNum').val() + "&Param.3=" + $('#txtOperation').val();
     /* $.ajax({
          "url": "/XMII/Illuminator",
          "data": qreg ,
          "dataType": "xml",
          "type":"POST",
          "cache": false,
          "success": function(data){
          alert("misura inviata");
          }
     }); */
     exeQuery (qreg, "Operazione aggiunta", "Errore in aggiunta operazione" )
}

// end of check me!



// Funzione visualizzare messaggi di esecuzione
function fnMessageOK ( msgtext  ) {
     if ( msgtext != null && $.trim(msgtext)  != "")  $( "#Result P" ).text(msgtext);
     $( "#Result" ).dialog({
          resizable: false,
          height:180,
          modal: true,
          buttons: {
                    OK: function() {
                    $( this ).dialog( "close" );
               }
          }
     });
}

// Funzione visualizzare messaggi di conferma
function fnConfirm ( msgtext  ) {
     if ( msgtext != null && $.trim(msgtext)  != "")  $( "#PopupConfirm P" ).text(msgtext);
     var bres = false;
             $( "#PopupConfirm" ).dialog({
                     resizable: false,
                     height:200,
                     modal: true,
                     buttons: {
                             Sì: function() {
			      bres = true;
                                     $( this ).dialog( "close" );
			   return bres;
                             },
                             No: function() {
			    bres = false;
                                     $( this ).dialog( "close" );
			   return bres;
                             },
                     }
             });
           //alert(bres);
	//return bres;
}

$(document).ready( function() {

     // Carica l'IP della postazione e lo visualizza nel DIV Info      
     $.ajax({
          "url": "/XMII/CM/QM_Measures/getIpAddr.jsp" ,
          "dataType": "html",
          "type":"GET",
          "cache": false,
          "success": function(data){
               $("#info" ).html($.trim(data));
                    localIP = $.trim(data);
	         currentIP = localIP;
                    fnCronoData($.trim(localIP));
                    fnMeasData($.trim(localIP));
                    fnOperData($.trim(localIP));
               }
     });
     
     // Carica la tabella con la lista delle postazioni     
     fnWSList();
     /*wsTable = $('#wslist').dataTable( {
               "bProcessing": true,
               "bServerSide": true,
               "bJQueryUI": true,
               "bSort": false,
               "bPaginate": true,
               "bStateSave": true,
               "sAjaxSource": qBasePath + "GetWsListQR",
               "fnServerData": fnGetJSONData,
	     "oLanguage": {
	      "sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
          });*/
   
     //******* Gestione degli eventi click vari *****
     
     /* Evento click sulle righe della tabella postazioni */
     $('#wslist tbody tr').live( 'click', function () {
          var IP = $(this).find('td:nth(1)').text();

          //$(this).toggleClass('row_selected');
		  if ( $(this).hasClass('row_selected') ) {
			   $(this).removeClass('row_selected');
		  }
		  else {
			   wsTable.$('tr.row_selected').removeClass('row_selected');
			   $(this).addClass('row_selected');
		  }	
	currentIP = $.trim(IP);
          fnCronoData($.trim(IP));
          fnMeasData($.trim(IP));
          fnOperData($.trim(IP));
     });



// check me !!

     /* Evento click sulle righe della tabella operazioni */
     $('#wsoper tbody tr').live( 'click', function () {
          var ID = $(this).find('td:nth(0)').text();

          //$(this).toggleClass('row_selected');
		  if ( $(this).hasClass('row_selected') ) {
			   $(this).removeClass('row_selected');
		  }
		  else {
			   wsTable.$('tr.row_selected').removeClass('row_selected');
			   $(this).addClass('row_selected');
		  }	
	currentID_oper = $.trim(ID);
//	alert(currentID_oper);
     });

// end of check me!

  

     /* Pulsante aggiorna postazioni */     
     $('#tb_refr').click( function () {
         fnWSList();
	fnCronoData(currentIP);
	fnMeasData(currentIP);
	fnOperData(currentIP);
     });

     /* Pulsante aggiorna stati */     
     $('#crono_refr').click( function () {
	fnCronoData(currentIP);
     });

     /* Pulsante aggiorna misure */     
     $('#mis_refr').click( function () {
	fnMeasData(currentIP);
     });


// check me!

     /* Pulsante aggiorna operazioni */     
     $('#oper_refr').click( function () {
	fnOperData(currentIP);
     });

// end of check me!


     /* Pulsante registra postazione */     
     $('#reg_ws').click( function () {
          var qreg = qBasePath + "RegisterWsQR&Param.1=" + $.trim(localIP) + "&Param.2=VirtualHost";
          exeQuery (qreg, "Postazione registrata", "Errore in registrazione" );
          /*$.ajax({
          "url": "/XMII/Illuminator",
          "data": qreg ,
          "dataType": "xml",
          "type":"POST",
          "cache": false,
          "complete": function(data, textStatus){
          if (textStatus == "success")  fnMessageOK("Postazione registrata");
          else  fnMessageOK("Errore in registrazione");
          }
          }); */
     });
 

// check me!

      /* Pulsante inserisci operazione */      
     $('#reg_oper').click( function () {
     
             $( "#RegOper" ).dialog({
                     resizable: false,
                     height:200,
                     modal: true,
                     buttons: {
                             Annulla: function() {
                                     $( this ).dialog( "close" );
                             },
                             Registra: function() {
                                      fnRegOper();
                                     $( this ).dialog( "close" );
                             }
                     }
             });
     });

// end of check me!


      /* Pulsante registra misura */      
     $('#reg_mis').click( function () {
     
             $( "#RegMis" ).dialog({
                     resizable: false,
                     height:200,
                     modal: true,
                     buttons: {
                             Annulla: function() {
                                     $( this ).dialog( "close" );
                             },
                             Registra: function() {
                                      fnRegMis();
                                     $( this ).dialog( "close" );
                             }
                     }
             });
     });

     /* Pulsante cancella misure */     
   	 $('#clear_mis').click( function () {
  	 var qreg = qBasePath + "ClearWsInfoQR&Param.1=" + "MEAS_" +$.trim(currentIP);
	 $( "#PopupConfirm P" ).text('Cancellare le misure?');
	 $( "#PopupConfirm" ).dialog({
		  resizable: false,
		  height:180,
		  modal: true,
		  buttons: {
			  Sì: function() {
				   exeQuery (qreg, "Misure cancellate correttamente", "Errore in eliminazione" );
				  fnMeasData(currentIP);
				   $( this ).dialog( "close" );
					  },
			  No: function() {
				  $( this ).dialog( "close" );
					  }
			  }
		  });
	  }); 


// check me!


     /* Pulsante cancella operazioni */     
   	 $('#clear_oper').click( function () {
  	 var qreg = qBasePath + "ClearWsInfoQR&Param.1=" + "OPER_" +$.trim(localIP);
	 $( "#PopupConfirm P" ).text('Cancellare le operazioni?');
	 $( "#PopupConfirm" ).dialog({
		  resizable: false,
		  height:180,
		  modal: true,
		  buttons: {
			  Sì: function() {
				   exeQuery (qreg, "Operazioni cancellate correttamente", "Errore in eliminazione" );
				  fnOperData(currentIP);
				   $( this ).dialog( "close" );
					  },
			  No: function() {
				  $( this ).dialog( "close" );
					  }
			  }
		  });
	  }); 

// end of check me!



     /* Pulsante cancella lista postazioni */     
   	 $('#clear_all').click( function () {
  	 var qreg = qBasePath + "ClearWsListQR" ;
	 $( "#PopupConfirm P" ).text('Cancellare le lista postazioni? Sicuro?');
	 $( "#PopupConfirm" ).dialog({
		  resizable: false,
		  height:180,
		  modal: true,
		  buttons: {
			  Sì: function() {
				   exeQuery (qreg, "Lista postazioni eliminata.", "Errore in eliminazione" );
      				   fnWSList();
				fnCronoData(currentIP);
				fnMeasData(currentIP);		
				   $( this ).dialog( "close" );
					  },
			  No: function() {
				  $( this ).dialog( "close" );
					  }
			  }
		  });
	  }); 


   	 /* Pulsante elimina singola postazione */
   	 $('#del_ws').click( function () {
  	 var qreg = qBasePath + "ClearWsItemQR" + "&Param.1=" + $.trim(currentIP);
	 $( "#PopupConfirm P" ).text('Cancellare la postazione selezionata? Sicuro?');
	 $( "#PopupConfirm" ).dialog({
		  resizable: false,
		  height:180,
		  modal: true,
		  buttons: {
			  Sì: function() {
				   exeQuery (qreg, "Postazione eliminata.", "Errore in eliminazione" );
      				   fnWSList();
                 			   fnCronoData(currentIP);
				   fnMeasData(currentIP);	
				   fnOperData(currentIP);	
				   $( this ).dialog( "close" );
					  },
			  No: function() {
				  $( this ).dialog( "close" );
					  }
			  }
		  });
	  }); 


// check me!


   	 /* Pulsante elimina singola operazione */
   	 $('#clear_oper_single').click( function () {
  	 var qreg = qBasePath + "ClearSingleItemQR" + "&Param.1=OPER_" + $.trim(currentIP) + "&Param.2=" + currentID_oper;
	 $( "#PopupConfirm P" ).text('Cancellare l operazione selezionata?');
	 $( "#PopupConfirm" ).dialog({
		  resizable: false,
		  height:180,
		  modal: true,
		  buttons: {
			  Sì: function() {
//				alert(qreg);
				   exeQuery (qreg, "Operazione eliminata.", "Errore in eliminazione" );
				   fnOperData(currentIP);	
				   $( this ).dialog( "close" );
					  },
			  No: function() {
				  $( this ).dialog( "close" );
					  }
			  }
		  });
	  });              



// end of check me!



	 /* Pulsante cancella crono stati */     
		  $('#clear_one').click( function () {
		  var qreg = qBasePath + "ClearWsInfoQR&Param.1=" + $.trim(currentIP);
		  $( "#PopupConfirm P" ).text('Cancellare la cronologia?');
		  $( "#PopupConfirm" ).dialog({
				  resizable: false,
				  height:180,
				  modal: true,
				  buttons: {
						  Sì: function() {
						   exeQuery (qreg, "Stati cancellate correttamente", "Errore in eliminazione" );
						   fnCronoData(currentIP);
						   $( this ).dialog( "close" );
						  },
						  No: function() {
								  $( this ).dialog( "close" );
						  }
				  }
				  });
		  }); 





     //******* Fine gestione eventi click  *****


     //******* Inizializzazione oggetti pagina  *****     
     $( "#tb_refr" ).button();
     $( "#crono_refr" ).button();
     $( "#mis_refr" ).button();
     $( "#oper_refr" ).button();
     $( "#clear_all" ).button();
     $( "#clear_one" ).button();
     $( "#clear_mis" ).button();
     $( "#clear_oper" ).button();
     $( "#clear_oper_single" ).button();
     $( "#del_ws" ).button();
     $( "#reg_mis" ).button();
     $( "#reg_oper" ).button();
     $( "#reg_ws" ).button();
     $( "#tabs" ).tabs();


} );