/* jshint -W117, -W098, -W069 */
// Funzioni di base per gestione dati e query standardizzate
//**  dataFunctons.js
//*******************************************
//**  Autore: Luca Adanti - IT-Link
//**
//**  Ver: 4.4.0
//**
//*******************************************
//** Ulteriori interventi di:
//** Giancarlo Pisoni
//** Bruno Rosati
//*******************************************
var ajaxTimeout = 200000;
$.ajaxSetup({
  timeout: ajaxTimeout
});



function zeroPad(nr, base) {
  var len = (String(base).length - String(nr).length) + 1;
  return len > 0 ? new Array(len).join('0') + nr : nr;
}



// Funzione per convertire i dati XML in formato JSON per le tabelle DataTables
function fnGetJSONData(sSource, aoData, fnCallback, oSettings) {
  var params = {};

  for (var i = 0; i < aoData.length; i++) {
    var entry = aoData[i];
    params[entry.name] = entry.value;
  }
    
    ajaxTimeout = 200000;
    $.ajaxSetup({
        timeout: ajaxTimeout
    });

  $.ajax({
      "url": "/XMII/Illuminator",
      "dataType": "xml",
      "data": sSource,
      "type": "POST",
      "success": function (data) {
        var jData = $(data);
        var json = {
          "sEcho": params["sEcho"],
          "aaData": []
        };
        json.iTotalRecords = jData.find("[nodeName='opensearch:totalResults']").text();
        json.iTotalDisplayRecords = json.iTotalRecords;

        var $events = $(data).find("Row");
        $events.each(function (index, event) {
          var $event = $(event),
            addData = [];
          $event.children().each(function (i, child) {
            addData.push($(child).text());
          });
          json.aaData.push(addData);
          json.iTotalRecords = index;
          //aoData.push(addData);
        });
        json.iTotalDisplayRecords = json.iTotalRecords + 1;
        fnCallback(json);
      }
    })
    .fail(function () {
      operationFailed();
    });
  //fnMessageOK("Errore di comunicazione con il server MII. Verificare la connettività", "Errore di comunicazione");});
}



// Funzione per aggiungere i dati XML direttamente alle tabelle DataTables
function fnGetXMLData(sSource, dataTable, bClear, bRedraw) {
  //if ($(dataTable).dataTable().lenght > 0)
  // if (window.console) console.log( "fnGetXMLData -> sSource: " +  sSource );
  // if (window.console) console.log( "fnGetXMLData -> dataTable: " +  dataTable );
  // if (window.console) console.log( "fnGetXMLData -> bClear: " +  bClear );
  // if (window.console) console.log( "fnGetXMLData -> bRedraw: " +  bRedraw );
  if (bClear && $(dataTable).dataTable().size() > 0) {
    $(dataTable).dataTable().fnClearTable();
  }
    
    ajaxTimeout = 200000;
    $.ajaxSetup({
        timeout: ajaxTimeout
    });
    
  $.ajax({
      "url": "/XMII/Illuminator",
      "dataType": "xml",
      "data": sSource,
      "async": false, //User: giancarlo  Date: 24/10/2016
      //"async": true,		//User: giancarlo  Date: 24/10/2016
      "type": "POST",
      "success": function (data) {
        var jData = $(data);

        var $events = $(data).find("Row");
        $events.each(function (index, event) {
          var $event = $(event),
            addData = [];
          $event.children().each(function (i, child) {
            addData.push($(child).text());
            // sdebug = sdebug + i + "=" + $(child).text() + " \t";
          });
          $(dataTable).dataTable().fnAddData(addData);
          //sdebug = sdebug + "\n";
        });
        if (bRedraw) {
          $(dataTable).dataTable().fnDraw(true);
        }
        if (bRedraw) {
          $(dataTable).dataTable().fnAdjustColumnSizing();
        }
      }
    })
    .fail(function () {
      operationFailed();
    });
  //	fnMessageOK("Errore di comunicazione con il server MII. Verificare la connettività", "Errore di comunicazione");});
}



// Funzione per aggiungere i dati XML direttamente alle tabelle DataTables EVOLUZIONE
function fnGetXMLDataEx(sSource, dataTable, bClear, bRedraw, bPreserve) {
  //if ($(dataTable).dataTable().lenght > 0)
  if (window.console) {
    console.log("fnGetXMLDataEx -> Rows: " + $(dataTable).dataTable().fnSettings().fnRecordsDisplay());
  }

  var nRows = $(dataTable).dataTable().fnSettings().fnRecordsDisplay();
  var nCount = 0;
  if (bClear && !bPreserve) {
    $(dataTable).dataTable().fnClearTable();
  }
    
    ajaxTimeout = 200000;
    $.ajaxSetup({
        timeout: ajaxTimeout
    });
    
  $.ajax({
    "url": "/XMII/Illuminator",
    "dataType": "xml",
    "data": sSource,
    "async": false, //User: giancarlo  Date: 24/10/2016
    //"async": true,	 	//User: giancarlo  Date: 24/10/2016
    "type": "POST",
    "success": function (data) {
      var jData = $(data);
      tabData = [];
      var $events = $(data).find("Row");
      $events.each(function (index, event) {
        var $event = $(event),
          addData = [];
        $event.children().each(function (i, child) {
          addData.push($(child).text());
          // sdebug = sdebug + i + "=" + $(child).text() + " \t";
        });
        if (!bPreserve) {
          $(dataTable).dataTable().fnAddData(addData);
        } else {
          tabData.push(addData);
        }
        nCount = nCount + 1;
      });
      if (bPreserve) {
        if (nCount != nRows || nCount == 1) {
          if (nRows > 0) {
            $(dataTable).dataTable().fnClearTable();
          }
          $(dataTable).dataTable().fnAddData(tabData);
        }
      }
    }
  }).fail(function () {
    operationFailed();
  });
}

// Restituisce un parametro da una query Ajax
function fnGetAjaxVal(query, paramName, basync) {
  var DateRet;
    
    ajaxTimeout = 200000;
    $.ajaxSetup({
        timeout: ajaxTimeout
    });
    
  $.ajax({
      "url": "/XMII/Illuminator",
      "data": query,
      "async": basync, //User: giancarlo  Date: 24/10/2016
      // "async": true,	//User: giancarlo  Date: 24/10/2016
      "dataType": "xml",
      "type": "POST",
      "cache": false,
      "success": function (data, textStatus) {
        DateRet = $(data).find('Row').children(paramName).text();
      }
    })
    .fail(function () {
      operationFailed();
      return "Error";
    });
  return DateRet;
}

// Restituisce un parametro da una query Ajax
function fnGetAjaxValLawTimeOut(query, paramName, basync) {
  var DateRet;
    
    ajaxTimeout = 3000;
    $.ajaxSetup({
        timeout: ajaxTimeout
    });
    
  $.ajax({
      "url": "/XMII/Illuminator",
      "data": query,
      "async": basync, //User: giancarlo  Date: 24/10/2016
      // "async": true,	//User: giancarlo  Date: 24/10/2016
      "dataType": "xml",
      "type": "POST",
      "cache": false,
      "success": function (data, textStatus) {
        DateRet = $(data).find('Row').children(paramName).text();
      }
    })
    .fail(function () {
      operationFailed(true);
      return "Error";
    });
  return DateRet;
}

// Restituisce i dati in tabella da una query Ajax
function fnGetAjaxData(query, basync) {
  var DateRet;
    
    ajaxTimeout = 200000;
    $.ajaxSetup({
        timeout: ajaxTimeout
    });
    
  $.ajax({
      "url": "/XMII/Illuminator",
      "data": query,
      "async": basync, //User: giancarlo  Date: 24/10/2016
      // "async": true,	//User: giancarlo  Date: 24/10/2016
      "dataType": "xml",
      "type": "POST",
      "cache": false,
      "success": function (data, textStatus) {
        DateRet = data;
      }
    })
    .fail(function () {
      operationFailed();
    });
  return DateRet;
}



// Funzione per esecuzione Query MII centralizzata con gestione messaggi esecuzione
function exeQuery(qParams, tSuccess, tError, basync) {
  if (basync === null) {
    basync = true;
  }
    
    ajaxTimeout = 200000;
    $.ajaxSetup({
        timeout: ajaxTimeout
    });

  $.ajax({
      "url": "/XMII/Illuminator",
      "data": qParams,
      "dataType": "xml",
      "type": "POST",
      "async": basync, //User: giancarlo  Date: 24/10/2016
      // "async": true,	//User: giancarlo  Date: 24/10/2016
      "cache": false,
      "complete": function (data, textStatus) {
        if (textStatus == "success") {
          if ($.trim(tSuccess) !== '') {
            fnMessageOK(tSuccess);
          }
        } else {
          if ($.trim(tError) !== '') {
            fnMessageOK(tError);
          }
        }
      }
    })
    .fail(function () {
      operationFailed();
    });
}

function operationFailed(bNoInstantPopup){
    
    if(bNoInstantPopup === undefined)
        bNoInstantPopup = false
    
    tConnectionCheckingTime = 2000;
    
    document.getElementById("connection_err").style.display = "block";
    document.getElementById("connection_ok").style.display = "none";
    
    if(!bNoInstantPopup)
        noConnectionTimes = 3; //viene messo a 3 per far apparire istantaneamente la popup
    
    objConnectionTimer.tAutoContinue.stop();
    objConnectionTimer.tAutoContinue = $.timer(function () {
        objConnectionTimer.function();
    }, tConnectionCheckingTime, false);
    objConnectionTimer.tAutoContinue.play();
}