/* jshint -W117, -W098 */
// Script base di automazione pagina acquisizione misure
//**  home.js
//*******************************************
//**  Autore: Luca Adanti - IT-Link
//**
//**  Ver: 4.4.0
//**
//*******************************************
//** Ulteriori interventi di:
//** Giancarlo Pisoni
//** Bruno Rosati
//*******************************************

var $ = jQuery.noConflict();

// Variabili globali a livello pagina
var localIP = "";
var currentIP = "";
var qGenericPath = "Content-Type=text/XML&QueryTemplate=QM_Measures/";
var qDataPath = "Content-Type=text/XML&QueryTemplate=QM_Measures/DataCollector/";
var qBasePath = "Content-Type=text/XML&QueryTemplate=QM_Measures/Measures/";
var qEquipPath = "Content-Type=text/XML&QueryTemplate=QM_Measures/Equipment/";
var wsLocked = false;
var bfnCancel = false;
var wsTable;
var cronoTable;
var misTable;
var selChar;
var currentCID;
var CAT_TYPE;
var PSEL_SET;
var SEL_SET;
var minSamples = 0;
var selChan = 0;
var selCharDesc = "";
var selKeyMethod = "";
var selMethod = "";
var selTolInf = "";
var selTolSup = "";
var dButtons = [];
var selMisID = "";
var cicRows = [];
var charSelected = "";
var TolPerc = 0;
var lTimeOut = 0;
var charsGrpSelected = ""; //aggiunta per gestire import da file da Sap.
var tTimeOut = $.timer(function () {
    if (window.console) {
        console.log("Timer TimeOut-> init");
    }
}, 1000, false);

var objTimer = {
    id: "timer"
    , function: function () {
        console.log("Timer: " + objTimer.id + " " + new Date());
    }
    , tAutoContinue: $.timer(function () {
            objTimer.function();
        }, 500, false) // CAMBIARE IL SECONDO PARAMETRO PER VARIARE IL TEMPO DI ESECUZIONE DEL TIMER
};
var DocPath = "";
var bMeasCount = false;
var selEquip = "";
var boolWork = false;
var locTheme = "cupertino";
var dragged = false;

var tConnectionCheckingTime = 30000;
var noConnectionTimes = 0;
var objConnectionTimer = {
    id: "connectionTimer"
    , function: function () {
        fnCheckConnection(currentCID);
    }
    , tAutoContinue: $.timer(function () {
        objConnectionTimer.function();
    }, tConnectionCheckingTime, false)
};
objConnectionTimer.tAutoContinue.play();

var bOpenPopUp = true;
var tClosingTime = 1;
var tClosingTimeCalculated = fnCalculateClosingTime(tClosingTime);
var objPopupTimer = {
    id: "popupTimer"
    , function: function () {
        if (bOpenPopUp)
            fnCreatePopup();
    }
    , tAutoContinue: $.timer(function () {
            objPopupTimer.function();
        }, tClosingTimeCalculated, false) // CAMBIARE IL SECONDO PARAMETRO PER VARIARE IL TEMPO DI ESECUZIONE DEL TIMER
};

//objPopupTimer.tAutoContinue.once(tClosingTime);

function fnCreatePopup() {
    Noty.closeAll();

    var params = "Content-Type=text/XML&QueryTemplate=QM_Measures/Messages/" + "getMessagesFromSapQR";
    params = params + "&Param.1=" + currentCID;
    params = params + "&Param.2=" + localIP;
    params = params + "&Param.3=" + tClosingTime;
    var result = fnGetAjaxData(params, false);

    var oOutput = result.getElementsByTagName('OUTPUT');
    var tNewTime = parseInt($(oOutput).find("NEXT_TQUERY").text());
    tClosingTime = tNewTime;
    var tNewTimeCalculated = fnCalculateClosingTime(tNewTime);

    var bBackground = (currentCID === "" || currentCID === 'undefined') ? true : false;
    var sTheme = /*bBackground ? 'bootstrap-v4' : 'metroui'*/ 'mint';

    var iMaxVisibleLength = result.getElementsByTagName('item').length + 1;
    Noty.setMaxVisible(iMaxVisibleLength);

    $.each(result.getElementsByTagName('item'), function (index) {
        var sType;
        var sMessageId = $(this).find('MESSAGE_ID').text();
        switch (sMessageId) {
        case '3000':
            sType = "warning";
            break;
        case '4000':
            sType = "error";
            break;
        default:
            break;
        }

        let ord = $(this).find('INSPORDER').text();
        let oper = $(this).find('INSPOPERATN').text();
        let matid = $(this).find('MATERIALE').text();
        let matdesc = $(this).find('MATDESC').text();
        let qty = $(this).find('QTA').text();
        let lotID = $(this).find('LOTTO').text();
        let listID = $(this).find('ID').text();
        let workc_id = $(this).find('WORKC_ID').text();

        var sText = $(this).find('MESSAGE_TX').text();
        sText = sText + "<br/><br/>" + Number(listID) + " - " + workc_id + " - " + matid + " - " + Number(ord) + '/' + Number(oper);
        sText = sText + "<br/>" + matdesc;
        /*sText = sText +  "<br/><br/>Postazione: " + localIP;
        sText = sText +  "<br/>Operatore: " + currentCID;*/

        let bClosedByButton = false;
        new Noty({
            theme: sTheme
            , type: sType
            , text: sText
            , progressBar: true
            , timeout: tNewTimeCalculated - 1000
            , closeWith: ['click']
            , width: '2500px'
            , buttons: bBackground ? false : [
								Noty.button(
                    'Chiudi'
                    , 'btn btn-error'
                    , function ($noty) {
                        bClosedByButton = true;
                        $noty.close();
                    }
								)
						]
        }).on('onClose', function () {
            if (!bBackground && !bClosedByButton) {
                fnDisplayOrder(Number(ord), oper, lotID, matid, matdesc.replace(/\"/g, '#'), qty, false);
                switch (sMessageId) {
                case '3000':
                    $('#Processo').click();
                    break;
                case '4000':
                    $('#Delibera').click();
                    break;
                default:
                    $('#ALL').click();
                    break;
                }
            }
        }).show();
    });

    objPopupTimer.tAutoContinue.stop();
    objPopupTimer.tAutoContinue = $.timer(function () {
        objPopupTimer.function();
    }, tNewTimeCalculated, false);

    objPopupTimer.tAutoContinue.play();
}

function fnCalculateClosingTime(timeInMinutes) {
    var returningTime = timeInMinutes * 60 * 1000;
    return returningTime;
}

// User: giancarlo  Date: 10/07/2014  Purpose: managing of shortcut keys
var UpperPanelShown;
var LeftPanelShown;
var MaxCharIndex = 0;
var MinCharIndex = 0;
var CurrCharIndex = 0;
// End of modification

$.ajaxSetup({
    async: false
});
//$.getScript("/XMII/CM/QM_Measures/Measures/js/dataFunction.js");
//$.getScript("/XMII/CM/QM_Measures/Measures/js/utilBase.js");
//$.getScript("/XMII/CM/QM_Measures/Measures/js/measureBase.js");
//$.getScript("/XMII/CM/QM_Measures/Measures/js/measureMulti.js");
$.ajaxSetup({
    async: true
});

Libraries.load(
	[
        "/XMII/CM/Common/js/ColReorder",
        "/XMII/CM/QM_Measures/Measures/js/dataFunction",
        "/XMII/CM/QM_Measures/Measures/js/styleswitch",
        "/XMII/CM/QM_Measures/Measures/js/utilBase",
        "/XMII/CM/QM_Measures/Measures/js/measureBase",
        "/XMII/CM/QM_Measures/Measures/js/measureMulti",
        "/XMII/CM/Common/script/noty/noty",
        "/XMII/CM/QM_Measures/Measures/js/shortcut",
        "/XMII/CM/QM_Measures/Measures/js/gui_events",
        "/XMII/CM/QM_Measures/Measures/js/gui_hotkeys"
    ]
    , function () {
        $(document).ready(function () {

            // User: giancarlo  Date: 10/07/2014  Purpose: managing of shortcut keys
            $("#Material").val("-");
            $("#MatDesc").val("-");
            $("#LotId").val("-");
            $("#Qty").val("-");
            $("#Order").val("-");
            $("#Oper").val("-");
            $("#Approv").val("-"); //	User: giancarlo  Date: 10/04/2017	[8017]
            $("#selChar").val("-");
            $("#CharDesc").val("-");
            fnEnableHK_MainWindow();
            // End of modification

            if (noConnectionTimes === 0) {
                document.getElementById("connection_ok").style.display = "block";
                document.getElementById("connection_err").style.display = "none";
            } else {
                document.getElementById("connection_err").style.display = "block";
                document.getElementById("connection_ok").style.display = "none";
            }

            var matched, browser;
            // Use of jQuery.browser is frowned upon.
            // More details: http://api.jquery.com/jQuery.browser
            // jQuery.uaMatch maintained for back-compat
            jQuery.uaMatch = function (ua) {
                ua = ua.toLowerCase();
                var match = /(chrome)[ \/]([\w.]+)/.exec(ua) || /(webkit)[ \/]([\w.]+)/.exec(ua) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) || /(msie) ([\w.]+)/.exec(ua) || ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) || [];
                return {
                    browser: match[1] || ""
                    , version: match[2] || "0"
                };
            };
            matched = jQuery.uaMatch(navigator.userAgent);
            browser = {};
            if (matched.browser) {
                browser[matched.browser] = true;
                browser.version = matched.version;
            }
            // Chrome is Webkit, but Webkit is also Safari.
            if (browser.chrome) {
                browser.webkit = true;
            } else if (browser.webkit) {
                browser.safari = true;
            }
            jQuery.browser = browser;
            locTheme = $.cookie('WsTheme');
            if ($.trim(locTheme) === "") {
                locTheme = "hotsneaks";
            } //cupertino";
            $('link[rel*=style][title]').each(function (i) {
                this.disabled = true;
                if (this.getAttribute('title') === locTheme) {
                    this.disabled = false;
                }
            });
            fnOpenWait();
            // Carica l'IP della postazione e lo visualizza nel DIV Info
            var ipForced = $.cookie('WsIpAddr');
            if ($.trim(ipForced) === "") {
                //code
                $.ajax({
                    "url": "/XMII/CM/QM_Measures/getIpAddr.jsp"
                    , "dataType": "html"
                    , "type": "GET"
                    , "cache": false
                    , "success": function (data) {
                        $("#info").html($.trim(data));
                        localIP = $.trim(data);
                        currentIP = localIP;
                        fnOrOpList();
                        //fnMeasData($.trim(localIP));
                    }
                });
            } else {
                $("#info").html(ipForced);
                localIP = ipForced;
                currentIP = ipForced;
                fnOrOpList();
            }
            //******* Gestione degli eventi click vari *****
            /* Evento click sulle righe delle caratteristiche */



            // User: giancarlo  Date: 10/07/2014  Purpose: managing of shortcut keys
            $('#chars tbody tr').live('click', function () {
                if ($(this).hasClass('row_selected')) {
                    //         $(this).removeClass('row_selected');
                    //         $('.baction').button("disable");
                    //         charSelected = "";
                    //         selChar = "";
                } else {
                    $('#chars tr.row_selected').removeClass('row_selected');
                    $(this).addClass('row_selected');
                    var sline = $('#chars').dataTable().fnGetData(this);

                    CurrCharIndex = this._DT_RowIndex;
                    charSelected = CurrCharIndex;
                    fnSelectCharLine(sline);

                    //         selChar = sline[0]; // $(this).find('td:nth(0)').text();
                    //         if (sline[7] != "Si") $('#misSingle').button("enable");
                    //         if (sline[18].length > 8) {
                    //            selKeyMethod = sline[18];
                    //            $('#getMet').button("enable");
                    //         }
                    //         charSelected = this;
                    //         $("#selChar").val(sline[0]);
                    //         $( "#CharDesc" ).val(sline[1]);
                    //         CurrCharIndex = this._DT_RowIndex;
                    //         if (sline[27].indexOf("FILE") > 0) $('#getFile').button("enable");
                    //         if (sline[3] == "Misura") $('#resChart').button("enable");
                    //         //fnChartResult ($( "#LotId" ).val(), $( "#Oper" ).val(), charSelected[0] , charSelected[1] ) ;
                    //         //fnCharResult ($( "#LotId" ).val(), $( "#Oper" ).val(), selChar ) ;
                }
            });
            // End of modification

            /* Evento click sulle righe delle misure */
            $('#wsmis tbody tr').live('click', function () {
                if ($(this).hasClass('row_selected')) {
                    $(this).removeClass('row_selected');
                    selMisID = "";
                    $('#del-mis').button("disable");
                } else {
                    $('#chars tr.row_selected').removeClass('row_selected');
                    $(this).addClass('row_selected');
                    selMisID = $(this).find('td:nth(0)').text();
                    $('#del-mis').button("enable");
                }
            });
            /* Evento click sulle righe degli Equipment */
            $('#tabEquip tbody tr').live('click', function () {
                if ($(this).hasClass('row_selected')) {
                    $(this).removeClass('row_selected');
                    selEquip = "";
                    $('#del-Eq').button("disable");
                    $('#mod-Eq').button("disable");
                } else {
                    $('#tabEquip tr.row_selected').removeClass('row_selected');
                    $(this).addClass('row_selected');
                    selEquip = this;
                    $('#del-Eq').button("enable");
                    $('#mod-Eq').button("enable");
                }
            });
            /* Evento click sulle righe delle fasi */
            $('#operlist tbody tr').live('click', function () {
                var Oper = $(this).find('td:nth(0)').text();
                //$(this).toggleClass('row_selected');
                if ($(this).hasClass('row_selected')) {
                    $(this).removeClass('row_selected');
                } else {
                    $('#operlist').dataTable().$('tr.row_selected').removeClass('row_selected');
                    $(this).addClass('row_selected');
                    //evento click
                }
                var OrderOper = zeroPad($("#txtOrder").val(), 100000000000) + Oper;
                //alert(OrderOper);
                $("#txtOrdOpe").val(OrderOper);
                fnGetChar(OrderOper);
                $("#orderSearch").accordion({
                    active: 1
                });
                $("#charlist").dataTable().fnDraw();
            });
            $('#idTar').live('change', function (e) {
                $("#eqID-ok").button("enable");
            });
            /* Evento click sulle righe dei punti misura */
            $('#tabEqpoint tbody tr').live('click', function () {
                var objId = $(this).find('td:nth(0)').text();
                //$(this).toggleClass('row_selected');
                if ($(this).hasClass('row_selected')) {
                    $(this).removeClass('row_selected');
                    $("#getTar-ok").button("disable");
                    $("#getTar-man").button("disable");
                } else {
                    $('#tabEqpoint').dataTable().$('tr.row_selected').removeClass('row_selected');
                    fnSetChan($('#idPort').val(), "STARTM", "EQUIP");
                    $(this).addClass('row_selected');
                    $("#getTar-ok").button("enable");
                    $("#getTar-man").button("enable");
                }
                $("#idEqPoint").val(objId);
                //$("#eqValRif").val($(this).find('td:nth(2)').text().replace(".", ","));
                $("#eqTolSup").val($(this).find('td:nth(4)').text().replace(".", ","));
                $("#eqTolInf").val($(this).find('td:nth(3)').text().replace(".", ","));
            });
            $("#idPort").spinner({
                spin: function (event, ui) {
                    if (ui.value > 99) {
                        $(this).spinner("value", 1);
                        fnSetChan(1, "STARTM", "EQUIP");
                        return false;
                    } else if (ui.value < 1) {
                        $(this).spinner("value", 99);
                        fnSetChan(99, "STARTM", "EQUIP");
                        return false;
                    } else {
                        fnSetChan(ui.value, "STARTM", "EQUIP");
                    }
                }
            });

            $('#charDocs tbody tr a').live('click', function (e) {
                e.preventDefault();
            });
            $('#orderList a').live('click', function (e) {
                e.preventDefault();
            });
            /* Pulsante aggiungi ordine a lista di lavoro */
            $('#addOOList').click(function () {
                fnAddOrOp($("#txtOrdOpe").val());


                // User: giancarlo  Date: 10/07/2014  Purpose: managing of shortcut keys
                fnDisableHK_OrderSearchPanel();
                LeftPanelShown = "X";
                fnEnableHK_WorkListPanel();
                // End of modification
            });
            /* Pulsante aggiorna lista di lavoro */
            $('#sx_refr').click(function () {
                fnOrOpList();
            });
            /* Pulsante per aprire o chiudere il pannello dx*/
            $('.dx_pan').click(function () {
                $("#dxpanel").toggle("slide", {
                    direction: 'right'
                });
            });
            $('#itlink').dblclick(function () {
                $("#theme").dialog({
                    resizable: true
                    , title: "Cambia tema"
                    , height: 350
                    , width: 280
                    , modal: true
                    , buttons: [
                        {
                            id: "dblclick-close"
                            , text: "Chiudi"
                            , click: function () {

                                $(this).dialog("close");
                                $.cookie('WsTheme', $.trim(locTheme), {
                                    expires: 365
                                });
                            }
				}
			]
                });
                $("#dblclick-close").bind('touchend', function (e) {
                    $("#dblclick-close").click();
                    e.preventDefault();
                    return;
                });
            });
            /* Pulsante per aprire popup grafico e risultati*/
            $('#resChart').click(function () {
                fnShowResult();
            });
            /* Pulsante per aprire o chiudere il pannello sx*/
            $('.sx_pan').click(function () {

                // User: giancarlo  Date: 10/07/2014  Purpose: managing of shortcut keys
                //      $("#sxpanel").toggle("slide", {
                //         direction: 'left'
                //      });
                //      $("#del-item").height($("#sx_del").height() - 4);
                fnPanelLeftToggle();
            });
            /* Pulsante per aprire o chiudere il pannello alto*/
            $('.t_pan').click(function () {
                //      $("#orderSearch").accordion({
                //         active: 1
                //      });
                //      $("#operlist").dataTable().fnDraw();
                //      $("#charlist").dataTable().fnDraw();
                //      $("#operlist").dataTable().fnClearTable();
                //      $("#charlist").dataTable().fnClearTable();
                //      $("#ui-accordion-orderSearch-panel-1").css("height", "auto");
                //      $("#ui-accordion-orderSearch-panel-0").css("height", "auto");
                //      $("#ui-accordion-orderSearch-panel-2").css("height", "auto");
                //      $("#ui-accordion-orderSearch-panel-3").css("height", "auto");
                //      $("#tpanel").toggle("slide", {
                //         direction: 'up'
                //      });
                //      $("#txtOrdOpe").val("");
                //      $("#txtOrdOpe").focus();
                fnPanelUpToggle();
                // End of modification

            });
            /* Pulsante cerca lotto */
            $('#getLot').click(function () {

                //	User: giancarlo  Date: 21/10/2016
                //		$("#txtOrder").val(zeroPad($("#txtOrder").val(), 100000000000));
                //	End of modification

                fnGetLot($("#txtOrder").val());
            });

            /* Pulsante cerca Caratteristiche */
            $('#getChar').click(function () {

                //	User: giancarlo  Date: 21/10/2016
                //		var zeroAdj = zeroPad($("#txtOrdOpe").val(), 1000000000000000);
                //		//alert(zeroAdj);
                //		$("#txtOrdOpe").val(zeroAdj);
                //	End of modification

                fnGetChar($("#txtOrdOpe").val());
            });

            /* Pulsante azioni singole */
            $('#misSingle').click(function () {
                fnSelectSingle(charSelected);
            });
            /* Pulsante misure da File */
            $('#getFile').click(function () {
                fngetFile();
            });
            $('#sx_del').click(function () {
                fnclearWL();
            });
            /* Pulsante acquisizione dati ciclica */
            $('#misCycle').click(function () {
                fnCycleChars();
            });
            /* Pulsante scadenze */
            $('#btTime').click(function () {
                fnGetTiming();
            });
            /* Pulsante stampa */
            //$("#printChart").click(function()
            //);
            $("#comment").click(function () {
                document.Chart1.showUpperChartComments(); //getChartObject()
            });

            /* Pulsante Nome postazione */
            $('#ipAddr').click(function () {

                Noty.closeAll();
                objPopupTimer.tAutoContinue.stop();
                objPopupTimer.tAutoContinue = $.timer(function () {
                    objPopupTimer.function();
                }, 250, false);

                $("#setWsAddr").dialog({
                    resizable: false
                    , height: 190
                    , width: 330
                    , modal: true
                    , buttons: [
                        {
                            id: "setWsAddr-delete"
                            , text: "Elimina"
                            , click: function () {
                                $(this).dialog("close");
                                $.removeCookie('WsIpAddr');
                                $.ajax({
                                    "url": "/XMII/CM/QM_Measures/getIpAddr.jsp"
                                    , "dataType": "html"
                                    , "type": "GET"
                                    , "cache": false
                                    , "success": function (data) {
                                        $("#info").html($.trim(data));
                                        localIP = $.trim(data);
                                        currentIP = localIP;
                                        fnOrOpList();
                                        //fnMeasData($.trim(localIP));
                                        fnOrOpList();
                                        fnDisplayOrder("", "", "", "", "", 0, false);
                                        objPopupTimer.tAutoContinue.play();
                                    }
                                });
                            }
							}
                        , {
                            id: "setWsAddr-save"
                            , text: "Salva"
                            , click: function () {
                                $(this).dialog("close");
                                var wsvalue = $('#idAddr').val();
                                if ($.trim(wsvalue) !== '') {
                                    $.cookie('WsIpAddr', $.trim(wsvalue), {
                                        expires: 365
                                    });
                                    $("#info").html(wsvalue);
                                    localIP = wsvalue;
                                    currentIP = wsvalue;
                                    fnOrOpList();
                                    fnDisplayOrder("", "", "", "", "", 0, false);
                                    objPopupTimer.tAutoContinue.play();
                                }
                            }
							}
                        , {
                            id: "setWsAddr-cancel"
                            , text: "Annulla"
                            , click: function () {
                                $(this).dialog("close");
                                fnOrOpList();
                                objPopupTimer.tAutoContinue.play();
                            }
                }
            ]
                });

                $("#setWsAddr-delete").bind('touchend', function (e) {
                    $("#setWsAddr-delete").click();
                    e.preventDefault();
                    return;
                });
                $("#setWsAddr-save").bind('touchend', function (e) {
                    $("#setWsAddr-save").click();
                    e.preventDefault();
                    return;
                });
                $("#setWsAddr-cancel").bind('touchend', function (e) {
                    $("#setWsAddr-cancel").click();
                    e.preventDefault();
                    return;
                });
            });


            //  Pulsante "Svuota buffer dati"		// User: giancarlo  Date: 17/02/2015  Purpose: additional button for buffer cleaning
            $('#btClnBuf').click(function () {
                fnClnBuf();
            });

            /* Pulsante Equipment */
            $('#btEquip').click(function () {
                fnEquipment();
            });

            /* Pulsante trova matricola */
            $('#fndSerial').click(function () {
                fnFindSerial();
            });
            $("#fndSerial").bind('touchend', function (e) {
                $("#fndSerial").click();
                e.preventDefault(e);
                return;
            });

            /* Pulsante acquisici taratura*/
            $('#chkTar').click(function () {
                //fnGetTar();
            });

            $('.btnFilter').on('click', function () {
                var valFilter = $(this).attr("id");

                // User: giancarlo  Date: 10/07/2014  Purpose: managing of shortcut keys
                fnButtonFilter(valFilter);
            });
            // End of modification

            /* Pulsante testo metodo */
            $('#getMet').click(function () {
                var txtMeth = fngetMethodText(selKeyMethod);
                if (txtMeth === "") {
                    return;
                }
                $("#Result P").text(txtMeth);
                $("#Result").dialog({
                    resizable: false
                    , height: 380
                    , width: 450
                    , title: selMethod
                    , modal: true
                    , buttons: [
                        {
                            id: "getMet-OK"
                            , text: "OK"
                            , click: function () {

                                $(this).dialog("close");
                            }
				}
			]
                });
                $("#getMet-OK").bind('touchend', function (e) {
                    $("#getMet-OK").click();
                    e.preventDefault();
                    return;
                });
            });

            $('#logout').click(function () {
                $("#PopupConfirm P").text("Effetturare il logout dalla postazione?");
                $("#PopupConfirm").dialog({
                    resizable: false
                    , height: 200
                    , modal: true
                    , buttons: [
                        {
                            id: "btnConfYes"
                            , text: "Sì"
                            , click: function () {

                                $(this).dialog("close");
                                $('#sx_pan').focus();
                                fnGetCID();
                            }
				}
                        , {
                            id: "btnConfNo"
                            , text: "No"
                            , click: function () {



                                $(this).dialog("close");
                            }
				}
			]
                });
                /* Work-around per eventi touch */
                $("#btnConfYes").bind('touchend', function (e) {
                    $("#btnConfYes").click();
                    e.preventDefault(e);
                    return;
                });
                $("#btnConfNo").bind('touchend', function (e) {
                    $("#btnConfNo").click();
                    e.preventDefault(e);
                    return;
                });
            });

            // ******* Fine gestione eventi click  *****


            // ******* Inizializzazione oggetti pagina  *****
            $(".jbutton").button();
            $("#filterChars").buttonset();
            $(".channels").button();
            //$( "#c0" ).checked = true;
            $("#c0").button("refresh");
            $('.channels').button("enable");
            $(".channels").click(function (event) {
                event.preventDefault();
                if (window.console) {
                    console.log("Click su pulsante: " + $(this).attr('id') + " valore: " + $(this).attr('id').substr(1));
                }
                if ($(this).attr('id').substr(1) == "0") {
                    $("#txtChannel").val("A");
                } else {
                    $("#txtChannel").val($(this).attr('id').substr(1));
                }
                fnSetChan($(this).attr('id').substr(1), "MANCH", $("#selChar").val());
            });
            $(".bclose").button({
                icons: {
                    primary: "ui-icon-close"
                }
                , text: false
            });
            $(".bdelete").button({
                icons: {
                    primary: "ui-icon-trash"
                }
                , text: false
            });
            $('.bdelete').button("enable");
            $(".brefresh").button({
                icons: {
                    primary: "ui-icon-refresh"
                }
                , text: false
            });
            $("#orderSearch").accordion({
                collapsible: true
                , heightStyle: "content"
            });
            $("#desktop").accordion({
                collapsible: true
                , heightStyle: "content"
                , activate: function (event, ui) {
                    if (ui.newPanel.attr('id') == 'ui-accordion-desktop-panel-1') {
                        $('#charDocs').dataTable().fnDraw();
                    }
                }
            });
            $("#resChartpp_block").accordion({
                collapsible: true
                , heightStyle: "content"
                , activate: function (event, ui) {
                    $("#charResult").dataTable().fnDraw(true);
                }
            });
            $("#orderList").sortable({
                placeholder: "ui-state-highlight"
            }).disableSelection();
            $(".locked").attr("readonly", "false");
            $('.baction').button("disable");
            $("#del-item").droppable({
                accept: ".ui-sortable li"
                , hoverClass: "ui-state-hover"
                , drop: function (event, ui) {
                    //$( this )
                    //.addClass( "ui-state-highlight" )
                    //.find( "p" )
                    //.html( "Dropped!" );
                    //alert ("Droppato" + ui.draggable.attr("OrdID"));
                    dragged = true;
                    $("#PopupConfirm P").text("Eliminare l'elemento " + ui.draggable.text() + "?");
                    $("#PopupConfirm").dialog({
                        resizable: false
                        , height: 200
                        , width: 500
                        , modal: true
                        , buttons: [
                            {
                                id: "PopupConfirm-Yes"
                                , text: "Sì"
                                , click: function () {

                                    $(this).dialog("close");
                                    fnDelWLItem(ui.draggable.attr("ListID"));
                                }
					}
                            , {
                                id: "PopupConfirm-No"
                                , text: "No"
                                , click: function () {
                                    dragged = false;



                                    $(this).dialog("close");
                                }
					}
				]


                    });

                    $("#PopupConfirm-Yes").bind('touchend', function (e) {
                        $("#PopupConfirm-Yes").click();
                        e.preventDefault(e);
                        return;
                    });
                    $("#PopupConfirm-No").bind('touchend', function (e) {
                        $("#PopupConfirm-No").click();
                        e.preventDefault(e);
                        return;
                    });
                }
            });
            $('.styleswitch').click(function () {
                locTheme = $(this).attr("rel");
            });
            //  Pulsanti per cancellazione caselle di ricerche

            $('#cid_del').bind('touchend click', function (e) {

                $("#txtCID").val("");
                $("#txtCID").focus();
            });
            $('#em_del').click(function (e) {
                $("#accOrder").val("");
                $("#accOrder").focus();
            });
            $('#char_del').click(function (e) {
                $("#txtOrdOpe").val("");
                $("#txtOrdOpe").focus();
            });
            $('#lot_del').click(function (e) {
                $("#txtOrder").val("");
                $("#txtOrder").focus();
            });
            $('#meas_del').click(function (e) {
                $("#txtValue").val("");
                $("#txtValue").focus();
            });
            $('#txtCID').live('keydown', function (e) {
                //if (window.console) console.log( e.type + ': ' + e.which );
                if (e.which == 13) {
                    if (fncheckCID($("#txtCID").val())) {
                        $("#cid-logon").dialog("close"); //code
                        e.preventDefault();
                    }
                }
            });
            //   inizializza le tabelle
            //initDT();
            $.getScript("/XMII/CM/QM_Measures/Measures/js/datatables.js", function () {
                initDT();
            });
            fnCloseWait();
            fnGetCID();

            //	User: giancarlo  Date: 12/04/2017  [7916]

            $('.number-only').keyup(function (e) {
                    if (this.value != '-')
                        while (isNaN(this.value)) {
                            this.value = this.value.split('').reverse().join('').replace(/[\D]/i, '')
                                .split('').reverse().join('');
                        }
                })
                .on("cut copy paste", function (e) {
                    e.preventDefault();
                });

            //	--------------------------------------------------------

        });
    });