/* jshint -W117, -W098 */
// Script gestione gesture touch
//**  gui_hotkey.js
//*******************************************
//**  Autore: Giancarlo Pisoni - Camozzi
//**
//**  Ver: 3.2.0
//**
//*******************************************
//** Ulteriori interventi di:
//** Luca Adanti - IT-Link
//** Bruno Rosati - IT-Link
//*******************************************

function shortcut_bind(iShortCut, iFuncDef, iPostCleanUp) {
  shortcut.remove(iShortCut);
  ////	shortcut.add(iShortCut, function(){if (iPostCleanUp) shortcut.remove(iShortCut); iFuncDef() });
}


function click_button(iButtonId) {
  if ($(iButtonId).is(':enabled')) {
    if ($(iButtonId).is(':visible')) {
      $(iButtonId).click();
    }
  }
}


function fnDisableHK_MainWindow() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  shortcut.remove("Ctrl+1");
  shortcut.remove("Ctrl+2");
  shortcut.remove("Ctrl+3");
  shortcut.remove("Ctrl+4");
  shortcut.remove("Ctrl+5");
  shortcut.remove("Ctrl+6");
  shortcut.remove("Ctrl+7");
  shortcut.remove("Ctrl+8");
  shortcut.remove("Ctrl+9");
  shortcut.remove("Ctrl+0");

  shortcut.remove("Ctrl+Up");
  shortcut.remove("Ctrl+Down");
  shortcut.remove("Ctrl+Left");
  shortcut.remove("Ctrl+Right");
}


function fnEnableHK_MainWindow() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  fnDisableHK_MainWindow();
  shortcut_bind("Ctrl+1", function () {
    fnPanelLeftToggle();
  }, false);
  shortcut_bind("Ctrl+2", function () {
    fnPanelUpToggle();
  }, false);
  shortcut_bind("Ctrl+3", function () {
    fnLogoutAsk();
  }, true);
  shortcut_bind("Ctrl+4", function () {
    fnGetTiming();
  }, true);
  shortcut_bind("Ctrl+5", function () {
    fnEquipment();
  }, true);
  shortcut_bind("Ctrl+6", function () {
    fnSetWorkstationName();
  }, true);
  shortcut_bind("Ctrl+7", function () {
    click_button('#misSingle');
  }, true);
  shortcut_bind("Ctrl+8", function () {
    click_button('#getFile');
  }, true);
  shortcut_bind("Ctrl+9", function () {
    click_button('#misCycle');
  }, true);
  shortcut_bind("Ctrl+0", function () {
    click_button('#resChart');
  }, true);

  shortcut_bind("Ctrl+Up", function () {
    fnButtonCtrlUp();
  }, false);
  shortcut_bind("Ctrl+Down", function () {
    fnButtonCtrlDown();
  }, false);
  shortcut_bind("Ctrl+Left", function () {
    fnButtonFilterSHL();
  }, false);
  shortcut_bind("Ctrl+Right", function () {
    fnButtonFilterSHR();
  }, false);

}


function fnDisableHK_OrderSearchPanel() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  shortcut.remove("Delete");
  shortcut.remove("Enter");
  shortcut.remove("Ctrl+0");
  shortcut.remove("Ctrl+1");
  shortcut.remove("Ctrl+2");
  fnEnableHK_MainWindow();
}


function fnEnableHK_OrderSearchPanel() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  fnDisableHK_MainWindow();
  shortcut.remove("Delete");
  shortcut.remove("Enter");
  shortcut.remove("Ctrl+0");
  shortcut.remove("Ctrl+1");
  shortcut.remove("Ctrl+2");
  shortcut_bind("Delete", function () {
    click_button('#char_del');
  }, false);
  shortcut_bind("Ctrl+0", function () {
    click_button('#getChar');
  }, false);
  shortcut_bind("Ctrl+1", function () {
    click_button('#addOOList');
  }, true);
  shortcut_bind("Ctrl+2", function () {
    fnPanelUpToggle();
  }, true);
}


function fnDisableHK_WorkListPanel() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  shortcut.remove("1");
  shortcut.remove("2");
  shortcut.remove("3");
  shortcut.remove("4");
  shortcut.remove("5");
  shortcut.remove("6");
  shortcut.remove("7");
  shortcut.remove("8");
  shortcut.remove("9");

  shortcut.remove("Alt+1");
  shortcut.remove("Alt+2");
  shortcut.remove("Alt+3");
  shortcut.remove("Alt+4");
  shortcut.remove("Alt+5");
  shortcut.remove("Alt+6");
  shortcut.remove("Alt+7");
  shortcut.remove("Alt+8");
  shortcut.remove("Alt+9");
}


function fnEnableHK_WorkListPanel() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  fnDisableHK_WorkListPanel();
  shortcut_bind("1", function () {
    $('#orderList').find('#Li-a-0').click();
  }, true);
  shortcut_bind("2", function () {
    $('#orderList').find('#Li-a-1').click();
  }, true);
  shortcut_bind("3", function () {
    $('#orderList').find('#Li-a-2').click();
  }, true);
  shortcut_bind("4", function () {
    $('#orderList').find('#Li-a-3').click();
  }, true);
  shortcut_bind("5", function () {
    $('#orderList').find('#Li-a-4').click();
  }, true);
  shortcut_bind("6", function () {
    $('#orderList').find('#Li-a-5').click();
  }, true);
  shortcut_bind("7", function () {
    $('#orderList').find('#Li-a-6').click();
  }, true);
  shortcut_bind("8", function () {
    $('#orderList').find('#Li-a-7').click();
  }, true);
  shortcut_bind("9", function () {
    $('#orderList').find('#Li-a-8').click();
  }, true);

  shortcut_bind("Alt+1", function () {
    fnAskDeleteWorklistItem("1");
  }, false);
  shortcut_bind("Alt+2", function () {
    fnAskDeleteWorklistItem("2");
  }, false);
  shortcut_bind("Alt+3", function () {
    fnAskDeleteWorklistItem("3");
  }, false);
  shortcut_bind("Alt+4", function () {
    fnAskDeleteWorklistItem("4");
  }, false);
  shortcut_bind("Alt+5", function () {
    fnAskDeleteWorklistItem("5");
  }, false);
  shortcut_bind("Alt+6", function () {
    fnAskDeleteWorklistItem("6");
  }, false);
  shortcut_bind("Alt+7", function () {
    fnAskDeleteWorklistItem("7");
  }, false);
  shortcut_bind("Alt+8", function () {
    fnAskDeleteWorklistItem("8");
  }, false);
  shortcut_bind("Alt+9", function () {
    fnAskDeleteWorklistItem("9");
  }, false);
}


function fnDisableHK_EquipmentWindow() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  shortcut.remove("Ctrl+1");
  shortcut.remove("Ctrl+2");
  shortcut.remove("Ctrl+3");
  shortcut.remove("Ctrl+4");
  shortcut.remove("Ctrl+5");
  shortcut.remove("Ctrl+6");
  shortcut.remove("Ctrl+7");
  shortcut.remove("Ctrl+8");
  shortcut.remove("Ctrl+9");
  fnEnableHK_MainWindow();
}


function fnEnableHK_EquipmentWindow() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  fnDisableHK_MainWindow();
  shortcut.remove("Ctrl+1");
  shortcut.remove("Ctrl+2");
  shortcut.remove("Ctrl+3");
  shortcut.remove("Ctrl+4");
  shortcut.remove("Ctrl+5");
  shortcut.remove("Ctrl+6");
  shortcut.remove("Ctrl+7");
  shortcut.remove("Ctrl+8");
  shortcut.remove("Ctrl+9");

  shortcut_bind("Ctrl+1", function () {
    fnEqItem(0);
  }, false);
  shortcut_bind("Ctrl+2", function () {
    fnEqItem(1);
  }, false);
  shortcut_bind("Ctrl+3", function () {
    fnDelEqItem();
  }, false);
  shortcut_bind("Ctrl+4", function () {
    fnClearEquipment();
  }, false);
  shortcut_bind("Ctrl+5", function () {
    fnDisableHK_EquipmentWindow();
    $("#Equipment").dialog("close");
  }); //$( this ).dialog( "close" ); }, true);

  shortcut_bind("Ctrl+6", function () {
    click_button('#getTar-man');
  }, false);
  shortcut_bind("Ctrl+7", function () {
    click_button('#getTar-ok');
  }, false);
  shortcut_bind("Ctrl+8", function () {
    click_button('#eqID-ok');
  }, false);
  shortcut_bind("Ctrl+9", function () {
    click_button('#eqID-esc');
  }, false);

  // [3956] User: giancarlo  Date: 28/11/2014  Purpose: managing of shortcut keys

  if ($("#EqItem").is(':visible')) {

    if (window.console) {
      console.log("Calling: ..EqItem visible!");
    }

    shortcut.remove("Alt+1");
    shortcut.remove("Alt+2");
    shortcut.remove("Alt+3");
    shortcut.remove("Alt+4");
    shortcut.remove("Alt+5");
    shortcut.remove("Alt+6");
    shortcut.remove("Alt+7");
    shortcut.remove("Alt+8");
    shortcut.remove("Alt+9");

    $('#tabEqpoint tbody tr').each(function (index, Row) {
      shortcut_bind("Alt+" + (index + 1), function () {
        var $Row = $(Row);

        var objId = $Row.find('td:nth(0)').text();

        if ($Row.hasClass('row_selected')) {
          $Row.removeClass('row_selected');
          $("#getTar-ok").button("disable");
          $("#getTar-man").button("disable");
        } else {
          $('#tabEqpoint tr.row_selected').removeClass('row_selected');
          fnSetChan($('#idPort').val(), "STARTM", "EQUIP");
          $Row.addClass('row_selected');
          $("#getTar-ok").button("enable");
          $("#getTar-man").button("enable");
        }

        $("#idEqPoint").val(objId);
        //$("#eqValRif").val($Row.find('td:nth(2)').text().replace(".", ","));
        $("#eqTolSup").val($Row.find('td:nth(4)').text().replace(".", ","));
        $("#eqTolInf").val($Row.find('td:nth(3)').text().replace(".", ","));

      }, false);

    });

  } else {

    if (window.console) {
      console.log("Calling: ..EqItem NOT visible!");
    }

    shortcut.remove("Alt+1");
    shortcut.remove("Alt+2");
    shortcut.remove("Alt+3");
    shortcut.remove("Alt+4");
    shortcut.remove("Alt+5");
    shortcut.remove("Alt+6");
    shortcut.remove("Alt+7");
    shortcut.remove("Alt+8");
    shortcut.remove("Alt+9");

    $('#tabEquip tbody tr').each(function (index, Row) {
      shortcut_bind("Alt+" + (index + 1), function () {
        var $Row = $(Row);

        if ($Row.hasClass('row_selected')) {
          $Row.removeClass('row_selected');
          selEquip = "";
          $('#del-Eq').button("disable");
          $('#mod-Eq').button("disable");
        } else {
          $('#tabEquip tr.row_selected').removeClass('row_selected');
          $Row.addClass('row_selected');
          selEquip = Row;
          $('#del-Eq').button("enable");
          $('#mod-Eq').button("enable");
        }
      }, false);
    });
  } // test
}


function fnEnableHK_SingolaDialog() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  fnDisableHK_MainWindow();
  shortcut.remove("Ctrl+1");
  shortcut.remove("Ctrl+2");
  shortcut.remove("Ctrl+3");
  shortcut_bind("Ctrl+1", function () {
    click_button('#man-mis');
  }, false);
  shortcut_bind("Ctrl+2", function () {
    click_button('#button-ok');
  }, true);
  shortcut_bind("Ctrl+3", function () {
    click_button('#button-esc');
  }, true);
}


function fnDisableHK_SingolaDialog() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  shortcut.remove("Ctrl+1");
  shortcut.remove("Ctrl+2");
  shortcut.remove("Ctrl+3");

  if ($("#charsSeq").is(':visible')) {
    fnEnableHK_MultiCharStep();
  } else {
    fnEnableHK_MainWindow();
  }
}


function fnEnableHK_AccettDialog(data) {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  var $catlist = $(data).find("Row");

  fnDisableHK_MainWindow();
  shortcut.remove("Ctrl+0");
  shortcut_bind("Ctrl+0", function () {
    if ($('#btn_cnl').is(':enabled')) {
      $("#btn_cnl").click();
    }
  }, true);

  $catlist.each(function (index, cat) {
    shortcut.remove("Ctrl+" + (index + 1));
    shortcut_bind("Ctrl+" + (index + 1), function () {
      if ($('#btn_conf_' + (index + 1)).is(':enabled')) {
        $("#btn_conf_" + (index + 1)).click();
      }
    }, true);
  });
}


function fnDisableHK_AccettDialog(data) {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  var $catlist = $(data).find("Row");

  shortcut.remove("Ctrl+0");

  $catlist.each(function (index, cat) {
    shortcut.remove("Ctrl+" + index);
  });

  fnEnableHK_MainWindow();
}


function fnEnableHK_MultiDialog() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  fnDisableHK_MainWindow();
  shortcut.remove("Ctrl+1");
  shortcut.remove("Ctrl+2");
  shortcut.remove("Ctrl+3");
  shortcut_bind("Ctrl+1", function () {
    click_button('#multi-camp');
  }, true);
  shortcut_bind("Ctrl+2", function () {
    click_button('#multi-char');
  }, true);
  shortcut_bind("Ctrl+3", function () {
    click_button('#multi-canc');
  }, true);
}


function fnDisableHK_MultiDialog() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  shortcut.remove("Ctrl+1");
  shortcut.remove("Ctrl+2");
  shortcut.remove("Ctrl+3");
  fnEnableHK_MainWindow();
}


function fnEnableHK_MultiCampDialog() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  fnDisableHK_MultiCampStep();

  shortcut.remove("Ctrl+1");
  shortcut.remove("Ctrl+2");
  shortcut.remove("Ctrl+3");
  shortcut.remove("Enter"); //   shortcut.remove("Ctrl+4");
  shortcut.remove("Ctrl+5");
  shortcut_bind("Ctrl+1", function () {
    click_button('#del-mis');
  }, false);
  shortcut_bind("Ctrl+2", function () {
    click_button('#multi-camp-can');
  }, false);
  shortcut_bind("Ctrl+3", function () {
    click_button('#multi-camp-man');
  }, false);
  shortcut_bind("Enter", function () {
    click_button('#button-ok');
  }, false); //Ctrl+4
  shortcut_bind("Ctrl+5", function () {
    click_button('#multi-camp-ann');
  }, true);

}


function fnDisableHK_MultiCampDialog() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  shortcut.remove("Ctrl+1");
  shortcut.remove("Ctrl+2");
  shortcut.remove("Ctrl+3");
  shortcut.remove("Enter"); //Ctrl+4
  shortcut.remove("Ctrl+5");
}


function fnEnableHK_MultiCampStep() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  shortcut.remove("Ctrl+7");
  shortcut.remove("Enter"); //Ctrl+8
  shortcut.remove("Ctrl+9");
  shortcut_bind("Ctrl+7", function () {
    click_button('#multi-step-blc');
  }, false);
  shortcut_bind("Enter", function () {
    click_button('#next-ok');
  }, true); //Ctrl+8
  shortcut_bind("Ctrl+9", function () {
    click_button('#multi-step-ann');
  }, true);
}


function fnDisableHK_MultiCampStep() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  shortcut.remove("Ctrl+7");
  shortcut.remove("Enter"); //Ctrl+8
  shortcut.remove("Ctrl+9");
}


function fnEnableHK_MultiCharStep() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  shortcut.remove("Ctrl+7");
  shortcut.remove("Enter"); //Ctrl+8
  shortcut.remove("Ctrl+9");
  shortcut_bind("Ctrl+7", function () {
    click_button('#multi-stepchar-blc');
  }, false);
  shortcut_bind("Enter", function () {
    click_button('#next-ok-c');
  }, true); //Ctrl+8
  shortcut_bind("Ctrl+9", function () {
    click_button('#next-cancel');
  }, true);
}


function fnDisableHK_MultiCharStep() {
  if (window.console) {
    console.log("Calling: " + arguments.callee.name + "..");
  }

  shortcut.remove("Ctrl+7");
  shortcut.remove("Enter"); //Ctrl+8
  shortcut.remove("Ctrl+9");
}