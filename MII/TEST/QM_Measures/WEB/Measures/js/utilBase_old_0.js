// Apre la finestra di attesa
function fnOpenWait (bLate) {

if (bLate == undefined) bLate = false;
if (window.console) console.log( "fnOpenWait -> bLate: " +  bLate );
if (bLate) {
//var timer = $.timer(function() {
$( "#waiting" ).dialog({
autoOpen: false,
resizable: true,
height:150,
modal: true,
position: "center",
closeOnEscape: false,
draggable: false,
//					hide: {
//						 effect: "fadeout",
//						 duration: 400
//						 }
});
$( "#waiting" ).dialog("open");
//});
//timer.once(250);
}
else {
$( "#waiting" ).dialog({
autoOpen: false,
resizable: true,
height:150,
modal: false,
position: "center",
closeOnEscape: false,
draggable: false,
//		  show: {
//			   effect: "fadein",
//			   duration: 50
//			   },
//		  hide: {
//			   effect: "fadeout",
//			   duration: 200
//			   }
});

$( "#waiting" ).dialog("open");
}
}

// Chiude la finestra di attesa
function fnCloseWait (bLate) {
if (bLate == null) bLate = false;
if (window.console) console.log( "fnCloseWait -> bLate: " +  bLate );
if (bLate) {
var timer = $.timer(function() {
$( "#waiting" ).dialog( "close" );
});
timer.once(250);
}
else $(  "#waiting" ).dialog( "close" );
}



// Funzione per aggiungere ordine alla lista di lavoro
function fnAddOrOp (OrderOper) {
var Order = OrderOper.substr(0,12);
var OperId = OrderOper.substr(12,4);
var qParams = qDataPath + "SetOperValueQR&Param.1=" + localIP;
qParams = qParams + "&Param.2=" + Order + "&Param.3=" + OperId;
$.ajax({
"url": "/XMII/Illuminator",
"data": qParams ,
"dataType": "xml",
"type":"POST",
"cache": false,
"success": function(data, textStatus){
fnOrOpList();
//		  $( "#sxpanel" ).toggle();  //"slide" , {direction: 'left'} );
//		  $( "#tpanel" ).toggle(); //"slide"  ,{direction: 'up'} );
fnPanelLeftToggle();      // [3956] User: giancarlo  Date: 01/12/2014  Purpose: managing of shortcut keys
fnPanelUpToggle();       // [3956] User: giancarlo  Date: 01/12/2014  Purpose: managing of shortcut keys
}
});
}

// Funzione per recuperare la lista di lavoro degli ordini di Produzione
function fnOrOpList ( ) {
var qParams = qDataPath + "GetOperValueListQR&Param.1=" + localIP;
$.ajax({
"url": "/XMII/Illuminator",
"data": qParams ,
"dataType": "xml",
"type":"POST",
"cache": false,
"success": function(data, textStatus){

var $orlist = $(data).find("Row");
//Cicla per tutte le righe
$('#orderList').html("");

$orlist.each(function(index, order){
var $order = $(order);
var ord = $order.children('ORDINE').text();
var oper = $order.children('OPERAZIONE').text();
var matid = $order.children('MATERIALE').text();
var matdesc = $order.children('MATDESC').text();
var qty = $order.children('QTA').text();
var lotID = $order.children('LOTTO').text();
var listID = $order.children('ID').text();
var sApo = String.fromCharCode(39);
var LiCount = Number(index)+1;

$('#orderList').append('<li id="Li-a-' + index + '" class="ui-state-default" ListID="' + listID +'">' + LiCount + ' - ' + matid + '  -  ' + Number(ord) + '/' + Number(oper)  + '<br/>' +  matdesc + '</li>' );
$('#orderList').find('#Li-a-' + index).click(function(e){
e.preventDefault();
if (dragged) {
dragged = false;
return;
}
fnDisplayOrder(Number(ord),oper,lotID,matid,matdesc.replace(/\"/g, '#') ,qty);});
});

}
});

}

// Funzione per recuperare il testo esteso del metodo della caratteristica
function fngetMethodText ( MethID ) {
var qParams = qDataPath + "GetSAPTextQR&Param.2=" + MethID + "I";

var data = fnGetAjaxData (qParams, false );
var $orlist = $(data).find("Row");
var txtMeth = "";
//Cicla per tutte le righe

$orlist.each(function(index, order){
var $order = $(order);
txtMeth += $order.children('TDLINE').text() + " ";

});
return txtMeth;
}

// Funzione per recuperare tutti i dati da mostrare nel desktop
function fnDisplayOrder (Order,  OperId, LotID, MatID, MatDesc, Qty) {

// User: giancarlo  Date: 10/07/2014  Purpose: managing of shortcut keys
//	$( "#sxpanel" ).toggle( );
$( "#sxpanel" ).hide();
LeftPanelShown = " ";
fnDisableHK_WorkListPanel();
fnEnableHK_MainWindow();
fnOpenWait(true);
// -- End of modification --

if (window.console) console.log( "fnDisplayOrder -> Order: " +  Order );

//controllo autorizzazione ordine/operatore
var params =  qBasePath + "getOrderCIDQR&Param.1=" + Order+ "&Param.2=" + currentCID;
var resAuth = fnGetAjaxVal (params, "Result", false  );
if (resAuth != "OK") {
fnMessageOK ( "Non si dispone l'autorizzazione per operare sul documento selezionato","Operazione Interrotta");
fnCloseWait(true);
return;
}

$( "#Material" ).val( MatID);
$( "#MatDesc" ).val(MatDesc.replace(/\#/g, '"') );
$( "#LotId" ).val(LotID);
$( "#Qty" ).val( parseFloat(Qty));
$( "#Order" ).val(Order);
$( "#Oper" ).val(OperId);

// User: giancarlo  Date: 10/07/2014  Purpose: managing of shortcut keys
$( "#selChar" ).val("-");
$( "#CharDesc" ).val("-");
// -- End of modification --

params =  qBasePath + "GetCharQR&Param.1=" + $( "#LotId" ).val() + "&Param.2=" + OperId;
fnGetXMLData(params, '#chars', true, true);
$('#chars').dataTable().fnSort( [[1,'asc']] );
$('#chars').dataTable().fnSort( [[0,'asc']] );

// User: giancarlo  Date: 10/07/2014  Purpose: managing of shortcut keys
MinCharIndex = 0;
MaxCharIndex = $('#chars').dataTable().fnGetData().length - 1;
CurrCharIndex = -1;
// -- End of modification --

$("#chars").dataTable().fnFilter( '' );
$('#misCycle').button("disable");
$('.baction').button("disable");

// Lista documenti
params =  qDataPath + "GetDocListQR&Param.1=" + zeroPad(Order, 100000000000) + "&Param.2=" + OperId;
//fnGetXMLData(params, '#charDocs', true);
if ( $('#charDocs').dataTable().fnGetData().length>0) $('#charDocs').dataTable().fnClearTable();
var data = fnGetAjaxData(params, false);
var $docs = $(data).find("Row");
$docs.each(function(index, doc){
var $doc = $(doc);
var dID = $doc.children('DOC_ID').text();
var dFile = $doc.children('DOC_FILENAME').text();
var dPath = $doc.children('DOC_FILEPATH').text();
var doc_type	= $doc.children('DOC_TYPE').text();
var doc_ver	= $doc.children('DOC_VER').text();
var doc_sub	= $doc.children('DOC_SUB').text();
var doc_fileid = $doc.children('DOC_FILEID').text();

var full_path = '~' + doc_type + '~' + dID + '~' + doc_ver + '~' + doc_sub;
full_path = DocPath + full_path + '/' + dFile;
//full_path = 'file://' + DocPath + full_path + '/' + dFile;
var tabData = [];
var sApo = String.fromCharCode(39);
tabData.push(dID, '<a href="#" target="_blank" onmousedown="{fnShowDoc(' + sApo + full_path + sApo +', ' + sApo + dID + sApo + ', ' + sApo + doc_type + sApo +', '+ sApo + doc_sub + sApo+','+ sApo+doc_ver+ sApo+','+ sApo+doc_fileid+ sApo+');}" >' + dFile + '</a>',
doc_type, doc_sub,doc_ver,doc_fileid);

$('#charDocs').dataTable().fnAddData(tabData);
});

if ( $('#charDocs').dataTable().fnGetData().length>0) $('#charDocs').dataTable().fnSort( [[1,'asc']] );

//impostazioni generali
$('#ALL').prop('checked', 'checked');
$("#filterChars").buttonset('refresh');

$( "#desktop" ).accordion({ active: 0 });
$('#chars').dataTable().fnDraw();
fnCloseWait(true);
}

function fnShowDoc (path,doc_id,doc_type, doc_sub,doc_ver,doc_fileid) {
var qParams = qDataPath + "RunSAPDMSDownloadQR&Param.1=" + doc_id;
qParams += "&Param.2=" + doc_type;
qParams += "&Param.3=" + doc_sub;
qParams += "&Param.4=" + doc_ver;
qParams += "&Param.5=" + doc_fileid;
fnGetAjaxData (qParams, false );

if (path.indexOf(".JPG") >0 || path.indexOf(".PNG") >0 || path.indexOf(".BMP") >0 || path.indexOf(".TIF") >0 || path.indexOf(".GIF") >0 ) {
//$('#fileDoc').html = "";
//$('#fileDoc').load("showimg.irpt?Img="+ path);
var iv1 = $("#viewer").iviewer({
src: path,
update_on_resize: true,
zoom_animation: true,
mousewheel: true,
onMouseMove: function(ev, coords) { },
onStartDrag: function(ev, coords) { return false; }, //this image will not be dragged
onDrag: function(ev, coords) { }
});
$("#fileDoc" ).dialog({
resizable: true,
title: "Visualizzazione documento " ,
height:900,
width:1150,
modal: true,
buttons: {  Chiudi: function() {
$( this ).dialog( "close" );
}
}
});
iv1.iviewer('set_zoom', 100);
}
else window.open( path, "_blank");

}

function fnChartResult (LotId, OperId, CharId, CharDesc )  {
//var qObj = document.Chart1.getQueryObject();
var qCrt = $("#Chart2e");
//var qObj = appletMethod("getQueryObject");
//var qObj = getApplet().getQueryObject();
//qObj.setParam(1, LotId);
//var applet = document.getElementsByName('Chart1')[0];
/*
document.Chart1.getQueryObject().setParam(1, LotId);
document.Chart1.getQueryObject().setParam(2, OperId);
document.Chart1.getQueryObject().setParam(3, CharId);
document.Chart1.updateChart(true);

qObj = document.Chart2.getQueryObject();
qObj.setParam(1, LotId);
qObj.setParam(2, OperId);
qObj.setParam(3, CharId);
document.Chart2.updateChart(true);*/
$(".chartTitle").text (CharDesc);

var varPars = '<PARAM NAME="Param.1" VALUE="' + LotId + '"><PARAM NAME="Param.2" VALUE="' + OperId + '"><PARAM NAME="Param.3" VALUE="' + CharId + '"></APPLET>';
var gCrtL = '<APPLET NAME="Chart1" id="Chart1" CODEBASE="/XMII/Classes" CODE="iSPCChart" ARCHIVE="illum8.zip" WIDTH="610" HEIGHT="320" MAYSCRIPT="true" scriptable="true">' +
'<PARAM NAME="QueryTemplate" VALUE="QM_Measures/Measures/GetResultsChartQR">' +
'<PARAM NAME="DisplayTemplate" VALUE="QM_Measures/Measures/GetMisuresCR">' +
'<PARAM NAME="Title" VALUE="Carta SPC x/r"><PARAM NAME="ChartType" VALUE="XBAR-RANGE">';
'<PARAM NAME="Sample Size" VALUE="' + minSamples +'">';
var gCrtR = '<APPLET NAME="Chart2" id="Chart2" CODEBASE="/XMII/Classes" CODE="iSPCChart" ARCHIVE="illum8.zip" WIDTH="320" HEIGHT="280" MAYSCRIPT="true" scriptable="true">' +
'<PARAM NAME="QueryTemplate" VALUE="QM_Measures/Measures/GetResultsChartQR">' +
'<PARAM NAME="DisplayTemplate" VALUE="QM_Measures/Measures/GetMisuresCR">' +
'<PARAM NAME="Title" VALUE="Istogramma Misure"><PARAM NAME="ChartType" VALUE="HISTOGRAM">';

$(".ChartL").html = "";
$(".ChartL").html(gCrtL + varPars);
$(".ChartR").html = "";
$(".ChartR").html(gCrtR + varPars);

var qParams = qBasePath + "GetCPQR&Param.1=" + LotId;
qParams += "&Param.2=" + OperId;
qParams += "&Param.3=" + CharId;

var data = fnGetAjaxData (qParams, false );
$("#idCP").text ( $(data).find("Row").children('CP').text());
$("#idCPK").text ($(data).find("Row").children('CPK').text());

}

function getApplet()
{
/*@cc_on return document.getElementById("testapplet-object") @*/
return document.getElementById("Chart2");
}

function fnCharResult (LotId, OperId, CharId )  {
params =  qBasePath + "GetResultsQR&Param.1=" + LotId + "&Param.2=" + OperId + "&Param.3=" + CharId;

fnGetXMLData(params, '#charResult', true);
}

// Funzione per recuperare Lotto Controllo e tutti i dati collegati
function fnGetLot (order ) {
var qParams = qBasePath + "GetInspLotInfoQR&Param.1=" + order;
fnOpenWait();
$.ajax({
"url": "/XMII/Illuminator",
"data": qParams ,
"async": false,
"dataType": "xml",
"type":"POST",
"cache": false,
"success": function(data, textStatus){
$( "#InspLot #iMat" ).text( "Materiale: " + $(data).find('Row').children('Material').text());
$( "#InspLot #iDesc" ).text($(data).find('Row').children('Mat_Text').text() );
$( "#InspLot #iLot" ).text("Lotto: " + $(data).find('Row').children('InspLot').text() );
$( "#InspLot #iQty" ).text("Quantità: " + $(data).find('Row').children('LOT_QTY').text() );
fnOperList($(data).find('Row').children('InspLot').text());
$( "#operlist" ).dataTable().fnDraw();
$( "#charlist" ).dataTable().fnDraw();
fnCloseWait();
}
});
}

// Funzione per recuperare il solo di Lotto Controllo
function fnGetInspLot (order ) {
var qParams = qBasePath + "GetInspLotQR&Param.1=" + order;
var lotId;
$.ajax({
"url": "/XMII/Illuminator",
"data": qParams ,
"async": false,
"dataType": "xml",
"type":"POST",
"cache": false,
"success": function(data, textStatus){
lotId = $(data).find('Row').children('InspLot').text() ;
}
});
return  lotId;
}


// Funzione per recuperare la lista operazioni
function fnOperList (LotId ) {
var qParams = qBasePath + "GetOperQR&Param.1=" + LotId;
fnGetXMLDataEx ( qParams, "#operlist", true , true , false );
}

// Funzione per cancellare gli stati
function fnClearState () {
var qParams = qDataPath + "ClearWsInfoQR&Param.1=" + $.trim(currentIP);;
exeQuery (qParams, "", "Errore in sincronizzazione" );
}

// Funzione per recuperare i dati delle caratteristiche in popup
function fnGetChar ( OrderOper ) {
var Order = OrderOper.substr(0,12);
var OperId = OrderOper.substr(12,4);
var qParams = qBasePath + "GetInspLotQR&Param.1=" + Order;
var lotId = fnGetAjaxVal (qParams, "InspLot", false  );

if ($.trim(lotId) == "") {
fnMessageOK ( "Impossibile recuperare il lotto di controllo"  );
return;
}

qParams = qBasePath + "GetCharQR&Param.1=" + lotId + "&Param.2=" + OperId;
fnGetXMLDataEx ( qParams, "#charlist", true , true , false );
$( "#operlist" ).dataTable().fnDraw();
$( "#charlist" ).dataTable().fnDraw();
}

// Funzione per impostare il canale della misura manuale
function fnSetChan ( Channel, cType, InspChar ) {
if (window.console) console.log( "fnSetChan -> Canale: " +  Channel + " -> Char.: " + InspChar );
if (Channel == 10) {
$('#channel_text').text('Acquisizione manuale' );
return;
}
var qreg = qDataPath + "SetWsStateQR&Param.1=" + $.trim(localIP) + "&Param.2=" + cType;
qreg = qreg +"&Param.3=" + Channel +"&Param.4=" + InspChar;
$('#txtChannel').val(Channel);
$('#channel_text').text('Acquisire da canale: ' + Channel);
if (charSelected != "") {
$('#channel_text').text('Acquisire da canale: ' + Channel + ' - ' +$('#chars').dataTable().fnGetData(charSelected)[22]);
}
exeQuery (qreg, "", "Errore in invio canale" );
}

// Funzione per registrare la misura manuale
function fnRegMis ( verbose , cChar) {
var qreg = qDataPath + "SetMisValueQR&Param.1=" + $.trim(localIP) + "&Param.2=" + $('#txtValue').val();
qreg = qreg + "&Param.4=" + cChar;
qreg = qreg +"&Param.3=10" ;
if (verbose) exeQuery (qreg, "", "Errore in accodamento misura" );
else exeQuery (qreg, "Misura accodata", "Errore in accodamento misura" );
}



// Funzione Richiesta codice operatore
function fncheckCID (  CID ) {

if (wsLocked) {
if (currentCID != CID) {
fnMessageOK("Attenzione: La postazione è bloccata da un altro operatore. Sbloccare con quel codice operatore.");
return;
}
else wsLocked = false;
}
var qXml = qreg = qDataPath + "GetSAPEmployeeAcknoQR&Param.1=" + CID;
var rCid = fnGetAjaxVal(qXml, "MESSAGE_RET", false);
if ($.trim(rCid) == "") {
qXml = qreg = qDataPath + "GetSAPEmployeeDataQR&Param.1=" + CID;
var infoCid = fnGetAjaxData(qXml, false);
$( "#CID" ).html($(infoCid).find('Row').children('NAME').text() + " - " + $(infoCid).find('Row').children('POSTXT').text());
currentCID = CID;
fngetParams();
var cTiming = $.timer(function() {fnGetTiming();}, 500, false);
cTiming.once();
return true;
}
else  {
fnMessageOK("Attenzione: CID non corretto!");
return false;
}
}


// Funzione per salvare in SAP le misure da file
function fnSaveFiles (  ) {
//alert(selChar);
var params =  qBasePath + "SaveFileQR&Param.1=" + $("#LotId").val();
params =  params + "&Param.2=" + $("#Oper").val();
params =  params + "&Param.3=" + currentCID;
params =  params + "&Param.4=" + $.trim(localIP);


var result = fnGetAjaxData(params, false);
if ($(result).find('Row').children("CodeResult").text() == "00") {
fnMessageOK("Operazione confermata con successo");
fnSetChan( "9", "DELFL", "" );
}
else fnMessageOK($(result).find('Row').children("CodeDescription").text());

}

// Funzione per confermare in SAP il campione qualitativo da catalogo
function fnSaveSamplesComplete (Eval, Group, Code, Remark, cCount ) {
//alert(selChar);
var params =  qBasePath + "SaveSamplesCompleteQR&Param.1=" + $("#LotId").val();
params =  params + "&Param.2=" + $("#Oper").val();
params =  params + "&Param.3=" + $("#selChar").val();
params =  params + "&Param.4=" + currentCID;
params =  params + "&Param.5=" + Eval;
params =  params + "&Param.6=" + Group;
params =  params + "&Param.7=" + Code;
params =  params + "&Param.8=" + Remark;
params =  params + "&Param.9=" + cCount;

var result = fnGetAjaxData(params, false);
if ($(result).find('Row').children("CodeResult").text() == "00")
{ fnMessageOK("Operazione confermata con successo");
}
else fnMessageOK($(result).find('Row').children("CodeDescription").text());

}


// Funzione per leggere dati via FILE
function fngetFile (  ) {
fnClearMeasures();
$('#wsmis').dataTable().fnClearTable();
fnSetChan( "9", "STARTFL", "" );
bfnCancel = false;
var qXml = qBasePath + "GetCatListQR&Param.1=" + $.trim(localIP) ;

var fileRefr = $.timer(function() {fnFileData();}, 950, false);
fileRefr.set({ autostart : false });
fileRefr.once(950);
if ($('#wsfile').dataTable().lenght > 0) $('#wsfile').dataTable().fnClearTable();
$( "#misFile" ).dialog({
resizable: false,
title: "Acquisizione misure da File" ,
height:450,
width:600,
modal: true,
buttons:  [{ id: "file-ok",
text: "Salva",
click: function() {
//fileRefr.stop();
fnSaveFiles();
$( this ).dialog( "close" );
} },
{ text: "Annulla",
click: function() {
bfnCancel = true;
fnClearMeasures();
$( this ).dialog( "close" );
} }
]
});
$("#file-ok").button("disable");
}

// Funzione per aprire la misura manuale
function fnMeasManual ( verbose, cChar, mTimer) {
if (mTimer != null) mTimer.pause();
$('#txtValue').unbind('keydown');

$('#txtValue').bind('keydown',function(e){
//if (window.console) console.log( e.type + ': ' + e.which );

if (e.which == 13 && $( "#txtValue" ).val().length>0 ) {
fnRegMis(verbose, cChar);
$( "#RegMis" ).dialog( "close" );
if (mTimer != undefined) mTimer.play();
e.preventDefault();
}
});

$( "#txtValue" ).val("");
$( "#txtValue" ).focus();
$( "#RegMis" ).dialog({
resizable: false,
height:200,
modal: true,
buttons: {
Annulla: function() {
$( this ).dialog( "close" );
if (mTimer != null) mTimer.play();
},
Registra: function() {
fnRegMis(verbose, cChar);
$( this ).dialog( "close" );
if (mTimer != undefined) mTimer.play();
}
}
});
}

// Funzione per aprire la misura manuale
function fnEquipManual ( ) {

$('#txtValue').unbind('keydown');

$('#txtValue').bind('keydown',function(e){
//if (window.console) console.log( e.type + ': ' + e.which );

if (e.which == 13 && $( "#txtValue" ).val().length>0 ) {
fnRegMis(True, cChar);
$( "#RegMis" ).dialog( "close" );
if (mTimer != undefined) mTimer.play();
e.preventDefault();
}
});

$( "#txtValue" ).val("");
$( "#txtValue" ).focus();
$( "#RegMis" ).dialog({
resizable: false,
height:200,
modal: true,
buttons: {
Annulla: function() {
$( this ).dialog( "close" );
},
Registra: function() {
fnRegMis(true, "EQUIP");
fnGetTar();
$( this ).dialog( "close" );
}
}
});
}

// Funzione per recuperare i dati della lista misure
function fnMeasData (  ) {
bMeasCount = false;
var params =  qDataPath + "GetMisValueListQR&Param.1=" + $.trim(localIP);

fnGetXMLDataEx(params,'#wsmis', true, false, true);
var nCount = $('#wsmis').dataTable().fnSettings().fnRecordsDisplay() ;
//alert (nCount + " - " + minSamples);
if ( nCount >= minSamples){
$("#button-ok").button("enable");
if ( nCount > minSamples) bMeasCount = true;
}
else $("#button-ok").button("disable");
if ( nCount > 0 && selTolInf != "" ) {
// alert (selTolInf);
var infVal = parseFloat(selTolInf.replace(",", ".")) - (parseFloat(selTolInf) / 100 * TolPerc);
var supVal = parseFloat(selTolSup.replace(",", ".")) + (parseFloat(selTolSup) / 100 * TolPerc);
if (TolPerc > 0) {
infVal = infVal - (infVal / 100 * TolPerc);
supVal = supVal + (supVal / 100 * TolPerc);
}
if (window.console) console.log( "Tolleranza Inf.: -> " + infVal);
if (window.console) console.log( "Tolleranza Sup.: -> " + supVal);
for (var i=0;i<nCount;i++)
{
var nNode = $('#wsmis').dataTable().fnGetNodes(i);
var mis = parseFloat($('#wsmis').dataTable().fnGetData($(nNode).find('td')[2]).replace(",", "."));
if ( mis < infVal || mis > supVal )  $(nNode).addClass('outTollerance');
if (window.console) console.log( "Misura: -> " + mis);
}
}
}

// Funzione per verificare la fine lettura da file
function fnFileData ( fileRefr ) {
if (bfnCancel) return;
params1 =  qDataPath + "GetMisValueListQR&Param.1=" + $.trim(localIP);
fnGetXMLDataEx(params1,'#wsFile', true, false, true);
var params2 =  qDataPath + "GetWsStateQR&Param.1=" + $.trim(localIP);
data = fnGetAjaxData (params2, false )
if ($(data).find('Row').children("State").text() == "STOPFL"){
if ($(data).find('Row').children("InspChar").text() == "FILE_ERR") fnMessageOK("Errore in lettura file misure!");
else $("#file-ok").button("enable");
this.stop;
//fnGetXMLDataEx(params1,'#wsFile', true, false, false);
}
else {
$("#file-ok").button("disable");
var fileRefr = $.timer(function() {fnFileData();},600,false);
//fileRefr.set({ autostart : false });
fileRefr.once(600);
}
}




// Funzione per calcolare la misura dalle altre
function fnCalcMis (curSample, Samples, curRow, Rows, curChar, sFormula ) {
var vFormula = sFormula;
var tRow = Rows * curSample + curRow ;

// Effettua la sostituzione delle formule parziali
var qFRepl = qBasePath + "getFormulaRepQR" ;
var data = fnGetAjaxData(qFRepl, false);
var $forRepls = $(data).find("Row");
$forRepls.each(function(index, map){
var $map = $(map);
var find = $map.children('name').text();
var re = new RegExp(find, 'g');
vFormula = vFormula.replace(re, $map.children('value').text());
});

while (vFormula.indexOf("A0") >= 0)
{  var iX = vFormula.indexOf("A0");
var sChar = vFormula.substring(iX+2,iX+6);
var iStart = (Rows * curSample);
for (var i=iStart; i<tRow; i++)
{  if ( $('#tabCycle').dataTable().fnGetData(i, 0) == (curSample+1) && $('#tabCycle').dataTable().fnGetData(i, 1) == sChar)
{
var rChar = "A0" + sChar ;
var rValue = $('#tabCycle').dataTable().fnGetData(i, 3);
vFormula = vFormula.replace(rChar, rValue.replace(",", "."));
}
}
}
var rFormula = eval(vFormula);
$('#tabCycle').dataTable().fnUpdate( rFormula.toFixed(2).toString().replace(".",","), tRow, 3 );

alert("Quota calcolata = " + rFormula.toFixed(2).toString().replace(".",","));		//  [3956] User: giancarlo  Date: 01/12/2014

$('#tabCycle').dataTable().fnUpdate( "Calcolato", tRow, 4 );
var qreg = qDataPath + "SetMisValueQR&Param.1=" + $.trim(localIP) + "&Param.2=" + rFormula.toString().replace(".",",");
qreg = qreg + "&Param.4=" + curChar +"&Param.3=0" ;
exeQuery (qreg, "", "Errore in accodamento misura" );
}



// Funzione per recuperare i dati della lista misure singola
function fnMeasDataSingle ( curChar, curSample ) {
if (window.console) console.log( "Caratteristica filtro:" + curChar);
$('#wsmis').dataTable().fnFilter( curChar, 4 );
var params =  qDataPath + "GetMisValueListQR&Param.1=" + $.trim(localIP);
fnGetXMLDataEx(params,'#wsmis', true, false, true);
$('#wsmis').dataTable().fnFilter( curChar, 4 );
var nCount = $('#wsmis tbody tr').length;
var data = $('#wsmis tbody tr:first').children(0).text();
if (data == 'Nessun dato presente nella tabella' || data == 'La ricerca non ha portato alcun risultato.') nCount = 0;

//alert (nCount + " - " + minSamples);
if ( nCount >= curSample+1) $("#button-ok").button("enable");
else $("#button-ok").button("disable");
if ( nCount > 0 && selTolInf != "" ) {
// alert (selTolInf);
var infVal = parseFloat(selTolInf.replace(",", "."));
var supVal = parseFloat(selTolSup.replace(",", "."));
//for (var i=0;i<nCount;i++)
//{
$('#wsmis tbody tr').each(function(index, nNode) {
//var nNode = $('#wsmis').dataTable().fnGetNodes(i);
if ($('#wsmis').dataTable().fnGetData(nNode,4) == curChar) {
var mis = parseFloat($('#wsmis').dataTable().fnGetData($(nNode).find('td')[2]).replace(",", "."));
if ( mis < infVal || mis > supVal )  $(nNode).addClass('outTollerance');
else  $(nNode).removeClass('outTollerance');
}
});
}
}




// Funzione per bloccare temporaneamente la postazione
function fnLockWs (   ) {

$( "#PopupConfirm P" ).text("Effetturare il blocco della postazione?");

$( "#PopupConfirm" ).dialog({
resizable: false,
height:200,
modal: true,
buttons: {
Sì: function() {
$( this ).dialog( "close" );
wsLocked = true;
tTimeOut.stop();
fnGetCID (   );
},
No: function() {
$( this ).dialog( "close" );
}
}
});


}

// Funzione Richiesta codice operatore
function fnGetCID (   ) {
if (wsLocked) var sTitle = "Postazione bloccata";
else var sTitle = "Identificazione operatore";

$( "#txtCID" ).val("");
$( "#cid-logon" ).dialog({
resizable: false,
position: [((window.innerWidth/2) - 150),10],	//  [3956] User: giancarlo  Date: 01/12/2014
height: 180,
width: 300,
modal: true,
closeOnEscape: false,
title: sTitle,
buttons: {
OK: function() {
if (fncheckCID($( "#txtCID" ).val())) {
$( this ).dialog( "close" );
}
}
}
});
}

// Funzione visualizzare messaggi di esecuzione
function fnMessageOK ( msgtext , stitle ) {
if ( msgtext != null && $.trim(msgtext)  != "")  $( "#Result P" ).text(msgtext);
if (stitle == undefined || stitle == '') {
stitle = "Operazione completata";
}
$( "#Result" ).dialog({
resizable: false,
height: 300,
width: 500,
modal: true,
title: stitle,
autoOpen: false,
draggable: true,
///          buttons: {
///                    OK: function() {
///                    $( this ).dialog( "close" );
///               }
///          }

buttons: [
{
id: "btnOK",
text: "OK",
click: function(){
$("#Result").dialog( "close" );
shortcut.remove("Enter");
}
}
],
});
//$("#btnOK").focus();
shortcut.add("Enter", function()  { $("#btnOK").click(); });
$( "#Result" ).dialog('open');
}


// Funzione visualizzare messaggi di conferma
function fnConfirm ( msgtext  ) {
if ( msgtext != null && $.trim(msgtext)  != "")  $( "#PopupConfirm P" ).text(msgtext);
var bres = false;
$( "#PopupConfirm" ).dialog({
resizable: false,
height:300,
width:500,
modal: true,
buttons: {
Sì: function() {
bres = true;
$( this ).dialog( "close" );
return bres;
},
No: function() {
bres = false;
$( this ).dialog( "close" );
return bres;
}
}
});
//alert(bres);
//return bres;
}

function fngetParams (  ) {
var params =  qDataPath + "GetParamsQR";
var data = fnGetAjaxData(params, false);
TolPerc = parseFloat($(data).find('Row').children("TolPerc").text());
lTimeOut = parseInt($(data).find('Row').children("TimeOut").text());
DocPath = $(data).find('Row').children("DocPath").text()
//alert("time out di " + lTimeOut + " secondi");
if (lTimeOut > 0) {
tTimeOut = $.timer(function(){fntimeOut();}, lTimeOut * 1000, false);
tTimeOut.play();
$(document).bind('keydown',function(e){fntimeClear();});
$(document).bind('click',function(e){fntimeClear();});
$(document).click(function(e){fntimeClear();});

}
}

function fntimeOut (  ) {
// if (window.console) console.log( "Timer TimeOut-> End Time");
//if (bTimeOut)
if (boolWork) {
wsLocked = true;
tTimeOut.stop();
fnGetCID ( );
}
else location.reload();

}

function fntimeClear (  ) {
//if (window.console) console.log( "Timer TimeOut-> Clear Time");
tTimeOut.stop();
//if (window.console) console.log( "Timer TimeOut-> Remaining Time:" + tTimeOut.remaining);
//tTimeOut.resetTimeOut();
tTimeOut.play(true);
}

/* Pulsante cancella operazioni */
function fnclearWL(  )  {
var qreg = qDataPath + "ClearWsInfoQR&Param.1=" + "OPER_" +$.trim(currentIP);
$( "#PopupConfirm P" ).text('Cancellare la lista di lavoro?');
$( "#PopupConfirm" ).dialog({
resizable: false,
height:200,
width:500,
modal: true,
buttons: {
Sì: function() {
exeQuery (qreg, "Lista cancellata correttamente", "Errore in eliminazione" ,false);
$( this ).dialog( "close" );
fnOrOpList();
$( "#sxpanel" ).toggle( "slide" , {direction: 'left'} );
},
No: function() {
$( this ).dialog( "close" );
}
}
});
}


// Funzione per recuperare i dati delle caratteristiche in popup
function fnGetTiming ( ) {
var qParams = qEquipPath + "GetWLTimingQR&Param.1=" + localIP;

fnGetXMLDataEx ( qParams, "#tabTime", true , true , false );

if ($("#tabTime").dataTable().fnGetData().length > 0) {
$( "#Timing" ).dialog({
resizable: true,
width: 700,
height:500,
modal: true,
buttons: {
chiudi: function() {
$( this ).dialog( "close" );
}
}
});
}
$("#tabTime").dataTable().fnDraw();
}


// Funzione per gestire gli Equipment
function fnEquipment ( ) {
var qParams = qEquipPath + "GetEquipListQR&Param.1=" + localIP;

fnGetXMLDataEx ( qParams, "#tabEquip", true , true , false );

if ($('#tabEquip').dataTable().fnSettings().fnRecordsDisplay() > 0) {
cdate = $.datepicker.formatDate('yy-mm-dd', new Date());

$('#tabEquip tbody tr').each(function(index, Row){
var $Row = $(Row);
var edata = $('#tabEquip').dataTable().fnGetData(Row)[1];
if ( edata.indexOf(cdate) <= -1)	{
$('#tabEquip').dataTable().fnUpdate("NO", Row, 4);
$(Row).addClass("outDate");
}
});
}
// if ($("#tabEquip").dataTable().fnGetData().length > 0) {
$( "#Equipment" ).dialog({
resizable: true,
width: 820,
height:450,
modal: true,
closeOnEscape: false,		// [3956] User: giancarlo  Date: 28/11/2014  Purpose: managing of shortcut keys
buttons: [
{text: "1 - Aggiungi",
click : function() {
fnEqItem ( 0 );
}},
{id: "mod-Eq",
text: "2 - Verifica",
click: function() {
fnEqItem (1 );
}},
{id: "del-Eq",
text: "3 - Elimina",
click: function() {
fnDelEqItem();
}},
{text: "4 - Svuota",
click: function() {
fnClearEquipment();
}},
{text: "5 - Chiudi",
click: function() {
// User: giancarlo  Date: 10/07/2014  Purpose: managing of shortcut keys
fnDisableHK_EquipmentWindow();
// -- End of modification --
$( this ).dialog( "close" );
}}]
});

// User: giancarlo  Date: 10/07/2014  Purpose: managing of shortcut keys
fnEnableHK_EquipmentWindow();
// -- End of modification --

// }
$("#tabEquip").dataTable().fnDraw(true);
$("#del-Eq").button("disable");
$("#mod-Eq").button("disable");
}

// Funzione per gestire gli Equipment
function fnEqItem ( item) {

$("#tabEqpoint").dataTable().fnClearTable();
if (item > 0) {
$("#idPort").val($('#tabEquip').dataTable().fnGetData(selEquip)[0]);
$("#idSerial").val($('#tabEquip').dataTable().fnGetData(selEquip)[2]);
$("#eqDesc").val($('#tabEquip').dataTable().fnGetData(selEquip)[3]);
$("#idTar").val($('#tabEquip').dataTable().fnGetData(selEquip)[4]);
$("#idObjid").val($('#tabEquip').dataTable().fnGetData(selEquip)[7]);
$("#idEquipid").val($('#tabEquip').dataTable().fnGetData(selEquip)[6]);
$("#eqClass").val($('#tabEquip').dataTable().fnGetData(selEquip)[8]);
var qParams = qEquipPath + "GetSAPEquipMeasPointQR_ext&Param.1=" + $("#idObjid").val();
fnGetXMLDataEx ( qParams, "#tabEqpoint", true , true , false );
fnSetChan( $('#idPort').val() , "STARTM", "EQUIP");
if ($('#tabEquip').dataTable().fnGetData(selEquip)[4] != "SI") {
$('#tabEqpoint tbody tr').each(function(index, Row){
var $Row = $(Row);
$('#tabEqpoint').dataTable().fnUpdate("", Row, 5);
$('#tabEqpoint').dataTable().fnUpdate("NO", Row, 6);
$(Row).addClass("outDate");
});
}
}
else
{
$("#idPort").val("1");
$("#idSerial").val("");
$("#idTar").val("NO");
$("#eqDesc").val("");
}
$( "#EqItem" ).dialog({
resizable: true,
width: 820,
height: 450,
modal: true,
buttons: [{text: "6 - Manuale",
id: "getTar-man",
click : function() {
fnEquipManual();
}},
{text: "7 - Acquisire",
id: "getTar-ok",
click : function() {
fnGetTar();
}},
{text: "8 - Salva",
id: "eqID-ok",
click : function() {
fnSaveEqItemDef();
fnEnableHK_EquipmentWindow() ;  // [3956] User: giancarlo  Date: 01/12/2014  Purpose: managing of shortcut keys

}},
{text: "9 - Annulla",
id: "eqID-esc",	        // [3956] User: giancarlo  Date: 01/12/2014  Purpose: managing of shortcut keys
click: function() {
$( this ).dialog( "close" );
fnEnableHK_EquipmentWindow() ;  // [3956] User: giancarlo  Date: 01/12/2014  Purpose: managing of shortcut keys
fnSetChan( "0", "STOPM", "" );
}}]
});

fnEnableHK_EquipmentWindow() ;  // [3956] User: giancarlo  Date: 01/12/2014  Purpose: managing of shortcut keys

$("#tabEqpoint").dataTable().fnDraw();
$("#getTar-ok").button("disable");
if ($('#tabEquip').dataTable().fnGetData(selEquip)[4] != "OK") $("#eqID-ok").button("disable");
}

// Funzione per salvare gli Equipment
function fnGetEqPoint () {

var qParams = qEquipPath + "GetSAPEquipMeasPointQR_ext&Param.1=" + $("#idObjid").val();
fnGetXMLDataEx ( qParams, "#tabEqpoint", true , true , false );

/*$( "#eqPoint" ).dialog({
resizable: true,
width: 550,
height:250,
modal: true,
buttons: [{text: "Seleziona",
id: "eqPoint-ok",
click : function() {
//fnSaveEqItemDef();
$("#eqID-ok").button("enable");
$( this ).dialog( "close" );
}},
{text: "Annulla",
click: function() {
$( this ).dialog( "close" );
}}]
}); */
$("#eqPoint-ok").button("disable");
}

// Funzione per salvare gli Equipment
function fnSaveEqItem () {
if ($("#idTar").val() == "") {
fnMessageOK("Manca il valore di taratura");
return;
}

if ($("#idEqPoint").val() == "") {
fnMessageOK("Non è stato selezionato il punto di lettura");
return;
}
}

// Funzione per salvare gli Equipment
function fnSaveEqItemDef () {

//Salva l'equipment
var qParams = qEquipPath + "SetEqItemQR&Param.1=" + localIP;
qParams += "&Param.2=" + $("#idPort").val();
qParams += "&Param.3=" + $("#idSerial").val();
qParams += "&Param.4=" + $("#idTar").val();
qParams += "&Param.5=" + currentCID;
qParams += "&Param.6=" + $("#eqDesc").val();
qParams += "&Param.7=" + $("#idObjid").val();
qParams += "&Param.8=" + $("#idEquipid").val();
qParams += "&Param.9=" + $("#eqClass").val();
exeQuery (qParams, "", "Errore salvataggio equipment.", false );

//Salva la taratura in SAP
$('#tabEqpoint tbody tr').each(function(index, Row){
qParams = qEquipPath + "SaveTarItemQR&Param.1=" + $("#idObjid").val();
qParams += "&Param.2=" + $('#tabEqpoint').dataTable().fnGetData(Row)[0];
qParams += "&Param.3=" + $('#tabEqpoint').dataTable().fnGetData(Row)[5];
qParams += "&Param.4=" + $('#tabEqpoint').dataTable().fnGetData(Row)[6];
qParams += "&Param.5=" + currentCID;
exeQuery (qParams, "", "Errore salvataggio taratura.", false );
//qParams = qEquipPath + "AddSAPEquipMeasDocQR&Param.1=" + $('#tabEqpoint').dataTable().fnGetData(Row)[0];
//qParams += "&Param.2=" + $('#tabEqpoint').dataTable().fnGetData(Row)[5];
//exeQuery (qParams, "", "Errore salvataggio taratura.", false );
});


// Ricarica lista equipment
var qParams = qEquipPath + "GetEquipListQR&Param.1=" + localIP;
fnGetXMLDataEx ( qParams, "#tabEquip", true , true , false );
cdate = $.datepicker.formatDate('yy-mm-dd', new Date());

$('#tabEquip tbody tr').each(function(index, Row){
var $Row = $(Row);
var edata = $('#tabEquip').dataTable().fnGetData(Row)[1];
if ( edata.indexOf(cdate) <= -1)	{
$('#tabEquip').dataTable().fnUpdate("NO", Row, 4);
$(Row).addClass("outDate");
}
})


$(  "#EqItem" ).dialog( "close" );

}

// Funzione per eliminare gli Equipment singolarmente
function fnDelEqItem () {
$( "#PopupConfirm P" ).text("Eliminare l'equipment selezionato?");

$( "#PopupConfirm" ).dialog({
resizable: false,
height:200,
modal: true,
buttons: {
Sì: function() {
$( this ).dialog( "close" );
var qParams = qEquipPath + "ClearEqItemQR&Param.1=" + localIP;
qParams += "&Param.2=" + $('#tabEquip').dataTable().fnGetData(selEquip)[0];
exeQuery (qParams, "", "Errore salvataggio equipment.", false );
qParams = qEquipPath + "GetEquipListQR&Param.1=" + localIP;
fnGetXMLDataEx ( qParams, "#tabEquip", true , true , false );

fnEnableHK_EquipmentWindow() ;  // [3956] User: giancarlo  Date: 01/12/2014  Purpose: managing of shortcut keys

},
No: function() {
$( this ).dialog( "close" );
}
}
});

}

// Funzione per pulire gli Equipment
function fnClearEquipment () {
$( "#PopupConfirm P" ).text("Eliminare tutta la lista corrente?");

$( "#PopupConfirm" ).dialog({
resizable: false,
height:200,
modal: true,
buttons: {
Sì: function() {
$( this ).dialog( "close" );
var qParams = qDataPath + "ClearWsInfoQR&Param.1=EQ_" + localIP;
exeQuery (qParams, "", "Errore salvataggio equipment.", false );
qParams = qEquipPath + "GetEquipListQR&Param.1=" + localIP;
fnGetXMLDataEx ( qParams, "#tabEquip", true , true , false );
fnEnableHK_EquipmentWindow() ;  // [3956] User: giancarlo  Date: 01/12/2014  Purpose: managing of shortcut keys
},
No: function() {
$( this ).dialog( "close" );
}
}
});

}

// Funzione per trovare il serial equipment
function fnFindSerial () {
var fdSerial = $('#idSerial').val();
var qParams = qEquipPath + "GetSAPEquipmentListQR";
var data = fnGetAjaxData(qParams,false);
var serial = "" ;
var $equipments = $(data).find("Row");
//Cicla per tutte le righe
$equipments.each(function(index, equip){
var $equip = $(equip);

// User: giancarlo  Date: 30/01/2014  Purpose: correct managing equipment id
//		    serial = $equip.children('EQUIPMENT_SERIALNO').text();
serial = $equip.children('EQUIPMENT_ID').text();
// End of modification

if (fdSerial == serial) {
$('#eqDesc').val($equip.children('EQUIPMENT_DESC').text());
$('#idObjid').val($equip.children('OBJID').text());
$('#idPort').val($equip.children('EQUIPMENT_PORT').text().substr(1,1));
fnSetChan( $('#idPort').val() , "STARTM", "EQUIP");
$('#eqClass').val($equip.children('EQUIPMENT_HIER').text());
fnGetEqPoint();
//$('#eqID-ok').button("enable");
return false;
}
else serial = "";
});

if (serial == "") {
fnMessageOK("Matricola non trovata!");
}

fnEnableHK_EquipmentWindow() ;  // [3956] User: giancarlo  Date: 01/12/2014  Purpose: managing of shortcut keys
}

// Funzione per leggere la taratura
function fnGetTar () {
fnOpenWait(false);
var eTar = "";
var qParams =  qDataPath + "GetMisValueListQR&Param.1=" + $.trim(localIP);
var i=5;
do {

var data = fnGetAjaxData(qParams,false);

var $measures = $(data).find("Row");

if ($measures.size()>0) {
eTar = $(data).find("Row").children('MISURA').text();
//$('#idTar').val(eTar);

$('#tabEqpoint tbody tr').each(function(index, Row){
if ($(Row).hasClass('row_selected')) {

$('#tabEqpoint').dataTable().fnUpdate( eTar, Row, 5 );
if (parseFloat($("#eqTolSup").val().replace(",", ".")) < parseFloat(eTar.replace(",", ".")) || parseFloat($("#eqTolInf").val().replace(",", ".")) > parseFloat(eTar.replace(",", ".")))  {
$('#tabEqpoint').dataTable().fnUpdate( "NO", Row, 6 );
$(Row).addClass('outTollerance');
$(Row).removeClass('outDate');
}
else {
$(Row).removeClass('outTollerance');
$(Row).removeClass('outDate');
$('#tabEqpoint').dataTable().fnUpdate( "SI", Row, 6 );
}		}
});


i = 0;
fnSetChan( "0", "STOPM", "" );
fnClearMeasures();
var allMeas = true;
var allOk = true;
$('#tabEqpoint tbody tr').each(function(index, Row){
if ($('#tabEqpoint').dataTable().fnGetData(Row)[5].lenght <= 0) allMeas = false;
if ($('#tabEqpoint').dataTable().fnGetData(Row)[6] == "NO") allOk = false;
});
if (allOk) $('#idTar').val("SI");
else $('#idTar').val("NO");
if (allMeas) $("#eqID-ok").button("enable");
}
else i -= 1;


}while (i>0);

fnCloseWait(false);

if (eTar == "") {
fnMessageOK("Taratura non trovata!");
}

}

// Apre la popup con i risultati ed il grafico
function fnShowResult () {


$('#chart-title').text("Grafico caratteristica di controllo: " + + $('#chars').dataTable().fnGetData(charSelected)[0]
+ ' - ' + $('#chars').dataTable().fnGetData(charSelected)[1]);
var url	= "Param1=" + 	$( "#LotId" ).val();
url	+= "&Param2=" + 	$( "#Oper" ).val();
url	+= "&Param3=" + 	$("#selChar").val(); //$('#chars').dataTable().fnGetData(charSelected)[0];
if ( Number($('#Qty').val()) >= 5000) url	+= "&Param4=" + $('#chars').dataTable().fnGetData(charSelected)[5];
else url	+= "&Param4=2" ;

$("#ChartBoxDet").load("chart_popup_old.irpt?"+ url);

$("#pValRif").text ($('#chars').dataTable().fnGetData(charSelected)[19] );
$("#pTolSup").text ($('#chars').dataTable().fnGetData(charSelected)[20] );
$("#pTolInf").text ($('#chars').dataTable().fnGetData(charSelected)[21]);

var qParams = qBasePath + "GetSAPQMParametersQR&Param.1=" + $('#LotId').val();
qParams += "&Param.2=" + $('#Oper').val();
qParams += "&Param.3=" + $('#selChar').val();

var data = fnGetAjaxData (qParams, false );
$("#idCP").text ( parseFloat($(data).find("Row").children('CP').text()).toFixed(2).replace(".", ","));
$("#idCPK").text (parseFloat($(data).find("Row").children('CPK').text()).toFixed(2).replace(".", ","));
$("#idCM").text ( parseFloat($(data).find("Row").children('CM').text()).toFixed(2).replace(".", ","));
$("#idCMK").text (parseFloat($(data).find("Row").children('CMK').text()).toFixed(2).replace(".", ","));
$("#idRMed").text (parseFloat($(data).find("Row").children('RMEAN').text()).toFixed(2).replace(".", ","));
$("#idXMed").text (parseFloat($(data).find("Row").children('XMEAN').text()).toFixed(2).replace(".", ","));
$("#idTnat").text (parseFloat($(data).find("Row").children('TNAT').text()).toFixed(2).replace(".", ","));
qParams =  qBasePath + "GetResultsQR&Param.1=" + $('#LotId').val()
+ "&Param.2=" +  $('#Oper').val()
+ "&Param.3=" +  $('#selChar').val();

// User: giancarlo  Date: 29/01/2014  Purpose: additional notes info in X-R Graph
$("#idXRnotes").text ($(data).find("Row").children('TNOTES').text());
$("#pLimCtrlSup").text (parseFloat($(data).find("Row").children('UP_LMT_1').text()).toFixed(2).replace(".", ","));
$("#pLimCtrlInf").text (parseFloat($(data).find("Row").children('LW_LMT_1').text()).toFixed(2).replace(".", ","));
if ( $("#pLimCtrlSup").val() == "0,00" ) $("#pLimCtrlSup").text ("-");
if ( $("#pLimCtrlInf").val() == "0,00" ) $("#pLimCtrlInf").text ("-");
//

//alert(qParams);
fnGetXMLData(qParams, '#charResult', true, true);
$('#charResult').dataTable().fnSort( [[1,'asc']] );
//$('#charResult').dataTable().fnSort( [[0,'asc'],[1,'asc']] );

$("#resChartpp").dialog({
resizable: true,
title: "Carta x/r per ordine: " + $('#Order').val() + " fase: " +  $('#Oper').val() ,
height:810,
width: 1220,
modal: false,
buttons:{
Interventi: function() {
document.Chart1.showUpperChartComments();
},
Stampa: function() {
fnPrintChart();
},
Chiudi: function() {
$( this ).dialog( "close" );
}
}
});

$("#charResult").dataTable().fnDraw(true);
$("#resChartpp").parent().find(".ui-dialog-titlebar-close").css("visibility","visible");
$('#resChartpp').bind('dialogclose', function(event) {
$("#ChartBoxDet").html("");
});
/* var params = [
'height='+screen.height,
'width='+screen.width,
'fullscreen=yes',
'status=no',
'toolbar=no',
'menubar=no',
'location=no'
].join(',');*/
// window.open("chart_popup.irpt?"+url, "", params);
}

function charsFiltered() {

if ($('#chars_filter input').val() != "") $('#misCycle').button("enable");
else $('#misCycle').button("disable");
}


function fnCheckTool(toolClass, toolDesc) {
var qParams = qEquipPath + "GetEquipListQR&Param.1=" + localIP;

cdate = $.datepicker.formatDate('yy-mm-dd', new Date());

var data = fnGetAjaxData(qParams, false);
var $tools = $(data).find("Row");
var retCode = false;
$tools.each(function(index, tool){
var $tool = $(tool);
if ($tool.children('EQCLASS').text() == toolClass && $tool.children('COMP').text() =='SI' && $tool.children('DATA').text().indexOf(cdate) > -1)	{
retCode = true;
//break;
}
});
if (!retCode) {
var msg  = "Attenzione: per la classe strumenti " + toolDesc + " non e' stata effettuata nessuna calibrazione odierna. Verficare Equipment.";
fnMessageOK(msg, "Impossibile continuare");
}

return retCode;
}

function fnPrintChart() {

var params = [
'height='+screen.height,
'width='+screen.width,
'fullscreen=no',
'status=no',
'toolbar=no',
'menubar=no',
'location=no'
].join(',');

var url = "Param1=" + $('#LotId').val();
url += "&Param2=" + $('#Oper').val();
url += "&Param3=" + $('#selChar').val();
if ( Number($('#Qty').val()) >= 5000) url	+= "&Param4=" + $('#chars').dataTable().fnGetData(charSelected)[5];
else url	+= "&Param4=2" ;
url += "&JSESSIONID=" + $.cookie('JSESSIONID');
url += "&ValRif=" + $('#pValRif').text();
url += "&TolSup=" + $('#pTolSup').text();
url += "&TolInf=" + $('#pTolInf').text();
url += "&idCP=" + $('#idCP').text();
url += "&idCPK=" + $('#idCPK').text();
url += "&idCM=" + $('#idCM').text();
url += "&idCMK=" + $('#idCMK').text();
url += "&idXMed=" + $('#idXMed').text();
url += "&idRMed=" + $('#idRMed').text();
url += "&idTnat=" + $('#idTnat').text();

var order = "Ordine: " + $('#Order').val() + "    Fase: " +  $('#Oper').val()  + " Quantità: " +  $('#Qty').val();
var lotto = "Lotto: " + $('#LotId').val() + "   Caratteristica: " +  $('#selChar').val() + "  " + $('#chars').dataTable().fnGetData(charSelected)[1];
var material = "Materiale: " + $('#Material').val() + "     " + $('#MatDesc').val();
var re = new RegExp(' ', 'g');
order = order.replace(re, '%20');
lotto = lotto.replace(re, '%20');
material = material.replace(re, '%20');
re = new RegExp('"', 'g');
lotto = lotto.replace(re, '%20');
material = material.replace(re, '%22');


if ( Number($('#Qty').val()) >= 5000) url	+= "&Param4=" + $('#chars').dataTable().fnGetData(charSelected)[5];
else url	+= "&Param4=2" ;
url += "&Order=" + order;
url += "&Lotto=" + lotto;
url += "&Material=" + material;
window.open("chart_print.irpt?"+url, "_blank", params);
}

function fnDelWLItem(ListId){
var qParams = qDataPath + "DelOperListItemQR&Param.1=" + localIP + "&Param.2=" + ListId;
exeQuery (qParams, "", "Errore in eliminazione lista" );
fnOrOpList();

}


// Funzione per pulire il buffer dati	// User: giancarlo  Date: 17/02/2015  Purpose: additional button for buffer cleaning
function fnClnBuf() {
var qParams = qDataPath + "ClearWsInfoQR&Param.1=" + localIP;
exeQuery (qParams, "", "Errore durante la pulizia del buffer" );
}


