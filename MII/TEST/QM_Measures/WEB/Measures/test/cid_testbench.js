// Variabili globali a livello pagina
var qBasePath = "Content-Type=text/XML&QueryTemplate=QM_Measures/DataCollector/";
var cidTable;
var cidMess;

// Funzione per confertire i dati XML in formato JSON per le tabelle DataTables
function fnGetJSONData ( sSource, aoData, fnCallback, oSettings ) {
     var params = {};
     
     for ( var i=0 ; i <aoData.length ; i++ ) {
          var entry = aoData[i];
          params[entry.name] = entry.value;
     }

     $.ajax({
          "url": "/XMII/Illuminator" ,
          "dataType": "xml",
          "data": sSource,
          "type": "POST",
          "success": function(data){
               var jData = $( data );					
               var json = {"sEcho": params["sEcho"],"aaData" : []};
               json.iTotalRecords = jData.find("[nodeName='opensearch:totalResults']").text();
               json.iTotalDisplayRecords = json.iTotalRecords;		
               
               var $events = $(data).find("Row");
               $events.each(function(index, event){
                    var $event = $(event),
                    addData = []; 
                    $event.children().each(function(i, child){
                         addData.push($(child).text());
                    });
                    json.aaData.push(addData);   
                    json.iTotalRecords = index;
                    //aoData.push(addData);    
               });
          json.iTotalDisplayRecords = json.iTotalRecords+1;		
          fnCallback(json);
          }
     });
}

// Funzione per esecuzione Query MII centralizzata
function exeQuery (qParams, tSuccess, tError ) {
     $.ajax({
     "url": "/XMII/Illuminator",
     "data": qParams ,
     "dataType": "xml",
     "type":"POST",
     "cache": false,
     "complete": function(data, textStatus){
     if (textStatus == "success")  fnMessageOK(tSuccess);
     else  fnMessageOK(tError);
     }
     });     
}

// Funzione per recuperare la lista CID

function fnCIDAckno ( ) {
     var qreg1 = qBasePath + "GetSAPEmployeeAcknoQR" + "&Param.1=" + $('#i_cid').val();
    cidMess   = "";
          $.ajax({
   	  "url": "/XMII/Illuminator",
    	 "data": qreg1 ,
    	 "dataType": "xml",
    	 "type":"POST",
     	"cache": false,
    	 "success": function(data, textStatus){
		cidMess = $(data).find('Row').children('MESSAGE_RET').text();
		if (cidMess != "") alert(cidMess);
   	}  
          });
}


function fnCIDList ( ) {
     var qreg2 = qBasePath + "GetSAPEmployeeDataQR" + "&Param.1=" + $('#i_cid').val();
     cidTable = $('#cidlist').dataTable( {
	     "bFilter": false,
	     "bInfo": false,
               "bDestroy":true,
               "bProcessing": true,
               "bServerSide": true,
               "bJQueryUI": true,
               "bSort": false,
               "bPaginate": true,
               "bStateSave": true,
               "sAjaxSource": qreg2,
               "fnServerData": fnGetJSONData,
	     "oLanguage": {
	      "sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
          });
}


// Funzione visualizzare messaggi di esecuzione

function fnMessageOK ( msgtext  ) {
     if ( msgtext != null && $.trim(msgtext)  != "")  $( "#Result P" ).text(msgtext);
     $( "#Result" ).dialog({
          resizable: false,
          height:180,
          modal: true,
          buttons: {
                    OK: function() {
                    $( this ).dialog( "close" );
               }
          }
     });
}

//

$(document).ready( function() {

     // Carica l'IP della postazione e lo visualizza nel DIV Info      
     $.ajax({
          "url": "/XMII/CM/QM_Measures/getIpAddr.jsp" ,
          "dataType": "html",
          "type":"GET",
          "cache": false,
          "success": function(data){
               $("#info" ).html($.trim(data));
                    localIP = $.trim(data);
	         currentIP = localIP;
                    fnCronoData($.trim(localIP));
                    fnMeasData($.trim(localIP));
                    fnOperData($.trim(localIP));
               }
     });
     
  
     //******* Gestione degli eventi click vari *****

     /* Pulsante leggi CID */     
     $('#bt_getcid').click( function () {
	fnCIDAckno ( ) ;
	fnCIDList ( ) ;
     });
 

     //******* Fine gestione eventi click  *****


     //******* Inizializzazione oggetti pagina  *****     
     $( "#bt_getcid" ).button();


} );