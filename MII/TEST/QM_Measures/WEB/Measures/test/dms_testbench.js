// Variabili globali a livello pagina
var qBasePath = "Content-Type=text/XML&QueryTemplate=QM_Measures/DataCollector/";
var dmsTable;
var dmsMess;

var dataDMS;

var textMeth;


// Funzione per confertire i dati XML in formato JSON per le tabelle DataTables
function fnGetJSONData ( sSource, aoData, fnCallback, oSettings ) {
     var params = {};
     
     for ( var i=0 ; i <aoData.length ; i++ ) {
          var entry = aoData[i];
          params[entry.name] = entry.value;
     }

     $.ajax({
          "url": "/XMII/Illuminator" ,
          "dataType": "xml",
          "data": sSource,
          "type": "POST",
          "success": function(data){
               var jData = $( data );					
               var json = {"sEcho": params["sEcho"],"aaData" : []};
               json.iTotalRecords = jData.find("[nodeName='opensearch:totalResults']").text();
               json.iTotalDisplayRecords = json.iTotalRecords;		
               
               var $events = $(data).find("Row");
               $events.each(function(index, event){
                    var $event = $(event),
                    addData = []; 
                    $event.children().each(function(i, child){
                         addData.push($(child).text());
                    });
                    json.aaData.push(addData);   
                    json.iTotalRecords = index;
                    //aoData.push(addData);    
               });
          json.iTotalDisplayRecords = json.iTotalRecords+1;		
          fnCallback(json);
          }
     });
}

// Funzione per esecuzione Query MII centralizzata
function exeQuery (qParams, tSuccess, tError ) {
     $.ajax({
     "url": "/XMII/Illuminator",
     "data": qParams ,
     "dataType": "xml",
     "type":"POST",
     "cache": false,
     "complete": function(data, textStatus){
     if (textStatus == "success")  fnMessageOK(tSuccess);
     else  fnMessageOK(tError);
     }
     });     
}


// Restituisce i dati da una query Ajax
function fnGetAjaxData (query, basync) {
     var DateRet;
	 $.ajax({
     "url": "/XMII/Illuminator",
     "data": query ,
	 "async": basync,	 
     "dataType": "xml",
     "type":"POST",
     "cache": false,
     "success": function(data, textStatus){
		  DateRet = data;
     }
   });
	 return DateRet;
}


// Funzione per recuperare la lista DMS

function fnDMSList ( ) {
     var qreg2 = qBasePath + "GetSAPDMSListQR";
     qreg2 = qreg2 + "&Param.1=" + $('#i_werks').val();
     qreg2 = qreg2 + "&Param.2=" + $('#i_merknr').val();
     qreg2 = qreg2 + "&Param.3=" + "";
     qreg2 = qreg2 + "&Param.4=" + $('#i_qpmethode').val();
     qreg2 = qreg2 + "&Param.5=" + $('#i_matnr').val();
     qreg2 = qreg2 + "&Param.6=" + "";
     qreg2 = qreg2 + "&Param.7=" + $('#i_vornr').val();
     qreg2 = qreg2 + "&Param.8=" + $('#i_prueflos').val();
     qreg2 = qreg2 + "&Param.9=" + "";
     qreg2 = qreg2 + "&Param.10=" + $('#i_aufnr').val();
     qreg2 = qreg2 + "&Param.11=" + "";
     qreg2 = qreg2 + "&Param.12=" + "";

     dmsTable = $('#dmslist').dataTable( {
	     "bFilter": false,
	     "bInfo": false,
               "bDestroy":true,
               "bProcessing": true,
               "bServerSide": true,
               "bJQueryUI": true,
               "bSort": false,
               "bPaginate": true,
               "bStateSave": true,
               "sAjaxSource": qreg2,
               "fnServerData": fnGetJSONData,
	     "oLanguage": {
	      "sUrl": "/XMII/CM/Common/js/dataTable_IT.txt"}
          });

    dataDMS = fnGetAjaxData(qreg2, false ); 
}


// Funzione per trasferire il file da SAP DMS a server, se necessario

function fnDMSFileDownload (i_doknr, i_file_idx, i_doktl, i_dokvr, i_dokar) {
     var qreg1 = qBasePath + "RunSAPDMSDownloadQR";
     qreg1 = qreg1 + "&Param.1=" + i_doknr;
     qreg1 = qreg1 + "&Param.2=" + i_file_idx;
     qreg1 = qreg1 + "&Param.3=" + "\\\\svrfile\\SAPUSER\\DMSTMP\\";
     qreg1 = qreg1 + "&Param.4=" + i_doktl;
     qreg1 = qreg1 + "&Param.5=" + i_dokvr;
     qreg1 = qreg1 + "&Param.6=" + i_dokar;
	
//    alert(qreg1);

         $.ajax({
   	  "url": "/XMII/Illuminator",
    	 "data": qreg1 ,
    	 "dataType": "xml",
    	 "type":"POST",
     	"cache": false
          });
}


// Funzione visualizzare messaggi di esecuzione

function fnMessageOK ( msgtext  ) {
     if ( msgtext != null && $.trim(msgtext)  != "")  $( "#Result P" ).text(msgtext);
     $( "#Result" ).dialog({
          resizable: false,
          height:180,
          modal: true,
          buttons: {
                    OK: function() {
                    $( this ).dialog( "close" );
               }
          }
     });
}

//


// Funzione per recuperare il testo esteso del metodo

function fnMethodText( ) {
     var qreg2 = qBasePath + "GetSAPTextQR";
     var link;
     qreg2 = qreg2 + "&Param.1=" + "0";
     qreg2 = qreg2 + "&Param.2=" + "500" + $('#i_werks').val() + $('#i_qpmethode').val() + "000001I";
     qreg2 = qreg2 + "&Param.3=" + "IT";
     qreg2 = qreg2 + "&Param.4=" + "500";
     qreg2 = qreg2 + "&Param.5=" + "QMTT";
     qreg2 = qreg2 + "&Param.6=" + "QPMETHODE";

    textMeth = fnGetAjaxData(qreg2, false ); 
	var $data = $(textMeth).find("Row"); //Cicla per tutte le righe 

	$data.each(function(index, document){
		 var $document = $(document);
		 var tdline	= $document.children('TDLINE').text();
		link = link + tdline; 
		});
    alert(link);
}


//


$(document).ready( function() {

     // Carica l'IP della postazione e lo visualizza nel DIV Info      
     $.ajax({
          "url": "/XMII/CM/QM_Measures/getIpAddr.jsp" ,
          "dataType": "html",
          "type":"GET",
          "cache": false,
          "success": function(data){
               $("#info" ).html($.trim(data));
                    localIP = $.trim(data);
	         currentIP = localIP;
               }
     });
     
  
     //******* Gestione degli eventi click vari *****

     /* Pulsante leggi DMS */     
     $('#bt_getdms').click( function () {
	fnDMSList ( ) ;
     });

     /* Pulsante visualizza file */     
     $('#bt_openfile').click( function () {

	var $data = $(dataDMS).find("Row"); //Cicla per tutte le righe 
		$('#doculink').html(""); 
		var link = "";
		$data.each(function(index, document){

		 var $document = $(document);

		 var doc_type	= $document.children('DOC_TYPE').text();
		 var doc_id	= $document.children('DOC_ID').text();
		 var doc_ver	= $document.children('DOC_VER').text();
		 var doc_sub	= $document.children('DOC_SUB').text();
		 var doc_fileid = $document.children('DOC_FILEID').text();
		 var doc_filename = $document.children('DOC_FILENAME').text();

		fnDMSFileDownload (doc_id, doc_fileid, doc_sub, doc_ver, doc_type) ;

		var full_path = '~' + doc_type + '~' + doc_id + '~' + doc_ver + '~' + doc_sub;
		full_path = '\\\\svrfile\\SAPUSER\\DMSTMP\\TST\\' + full_path + '\\' + doc_filename;

		link = link + '<a alt="' + doc_filename + '"' +' href="' + full_path + ' " target = "_blank">' + doc_filename +'</a><br/>'; 
		$('#doculink').html(link); 
	});
     });

     /* Pulsante visualizza file */     
     $('#bt_textmeth').click( function () {
	fnMethodText( ) ;
     });



 /* Evento click sulle righe della tabella documenti */
/*     $('#dmslist tbody tr').live( 'click', function () {
          
	var dms_doc_type = $(this).find('td:nth(0)').text();
	var dms_doc_id = $(this).find('td:nth(1)').text();
	var dms_doc_ver = $(this).find('td:nth(2)').text();
	var dms_doc_sub = $(this).find('td:nth(3)').text();
	var dms_file_id = $(this).find('td:nth(4)').text();
	var dms_file_name = $(this).find('td:nth(5)').text();
	
	fnDMSFileDownload (dms_doc_id, dms_file_id, dms_doc_sub, dms_doc_ver, dms_doc_type) ;

          //$(this).toggleClass('row_selected');
		  if ( $(this).hasClass('row_selected') ) {
			   $(this).removeClass('row_selected');
		  }
		  else {
			   dmsTable.$('tr.row_selected').removeClass('row_selected');
			   $(this).addClass('row_selected');
		  }	
     });
*/


     //******* Fine gestione eventi click  *****


     //******* Inizializzazione oggetti pagina  *****     
     $( "#bt_getdms" ).button();
     $( "#bt_openfile" ).button();
     $( "#bt_textmeth" ).button();

} );