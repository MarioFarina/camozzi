// Script per pagina anagrafiche stabilimento

/* Variabili della pagina */
var QService = "/XMII/Illuminator?";
var dataCommon = "Content-Type=text/XML&QueryTemplate=Common/BasicComponents/";
var dataCollect = "Content-Type=text/XML&QueryTemplate=QM_Measures/DataCollector/";
var icon16 = "/XMII/CM/Common/icons/16x16/";
var currentIP = "";

/***********************************************************************************************/
// Inizializzazione pagina
/***********************************************************************************************/
$(document).ready(function () {
   
   // crea la tabella lista postazioni
   var oTable1 = createTabWorkstation();
   oTable1.placeAt("MonitorCont");
   
   currentIP = fnGetIP();
	// Crea la TabStrip principale
	var oTabMaster = new sap.ui.commons.TabStrip("TabMaster");
	//oTabMaster.setWidth("1000px");
	//oTabMaster.setHeight("800px");
	oTabMaster.attachClose( function (oEvent) {
		var oTabStrip = oEvent.oSource;
		oTabStrip.closeTab(oEvent.getParameter("index"));
	});
	
   var oTable2 = createTabStates();
	// 1. tab: Lista stati (use createTab)
	var oLayout2 = new sap.ui.commons.layout.MatrixLayout("tabStati", {columns: 1});
	//oLayout2.setWidths(['980px']);
	oLayout2.createRow( oTable2);
	oTabMaster.createTab("Lista Stati",oLayout2);
	
   var oTable3 = createTabOrders();
	// 2. tab: Lista stati (use createTab)
	var oLayout3 = new sap.ui.commons.layout.MatrixLayout("tabOrd", {columns: 1});
	//oLayout2.setWidths(['980px']);
	oLayout3.createRow( oTable3);
	oTabMaster.createTab("Lista Ordini",oLayout3);
	
   var oTable4 = createTabMis();
	// 3. tab: Lista stati (use createTab)
	var oLayout4 = new sap.ui.commons.layout.MatrixLayout("tabMis", {columns: 1});
	//oLayout2.setWidths(['980px']);
	oLayout4.createRow( oTable4);
	oTabMaster.createTab("Lista Misure",oLayout4);
	
	oTabMaster.placeAt("MonitorDett");	
});

/***********************************************************************************************/
// Funzioni ed oggetti per Postazioni
/***********************************************************************************************/

// Functione che crea la tabella Postazioni e ritorna l'oggetto oTable
function createTabWorkstation() {
   
   //Crea L'oggetto Tabella Postazioni
    var oTable = new sap.ui.table.Table(
    {
      title: "Lista Postazioni",
      id: "WsTab",
      visibleRowCount: 6,
	  width: "98%",
      firstVisibleRow: 1,
      selectionMode: sap.ui.table.SelectionMode.Single,
      navigationMode: sap.ui.table.NavigationMode.Paginator,
      rowSelectionChange: function(oControlEvent)
       {
         if ($.UIbyID("WsTab").getSelectedIndex() == -1) {
		    $.UIbyID("btnRegWs").setEnabled(false);
			$.UIbyID("btnDelMach").setEnabled(false);
			refreshTabMis("*");
			refreshTabOrders("*");
			refreshTabState("*");			
			}
         else {
		    $.UIbyID("btnRegWs").setEnabled(true);
			$.UIbyID("btnDelMach").setEnabled(true);
			currentIP = fnGetCellValue("WsTab", "IP");
			refreshTabMis(currentIP);
			refreshTabOrders(currentIP);
			refreshTabState(currentIP);
		 }
        },
      toolbar: new sap.ui.commons.Toolbar(
      {
        items: [
          new sap.ui.commons.Button(
          {
            text: "Pulisci Postazioni",
            icon: icon16 + "blueprint--plus.png",
            press: function ()
            {
              clearAllWs();
            }
          }),
          new sap.ui.commons.Button(
          {
            text: "Registra Postazione",
            id:   "btnRegWs",
            icon: icon16 + "blueprint--pencil.png",            
            enabled: true,
            press: function ()
            {
              fnRegWs();
            }
		  }),
          new sap.ui.commons.Button(
          {
            text: "Elimina selezionata",
            id:   "btnDelMach",
            icon: icon16 + "blueprint--minus.png",            
            enabled: false,
            press: function ()
            {
              fnDelWs();
            }			
          }),
          new sap.ui.commons.Button(
          {
            text: "Aggiorna",
            icon: icon16 + "refresh.png",            
            enabled: true,
            press: function ()
            {
              refreshTabWs();
            }			
          })              
        ]
      })
    });
    
    
    /*var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "ID Postazione"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "ID"),
      sortProperty: "ID",
      filterProperty: "ID",
      width: "50px"
    });
    oTable.addColumn(oColumn);*/
    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Data / ora"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "DATA"),
      sortProperty: "DATA",
      filterProperty: "DATA",
      width: "200px"
    });
    oTable.addColumn(oColumn);
    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "IP / Nome"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "IP"),
      sortProperty: "IP",
      filterProperty: "IP",
      width: "200px"
    });
    oTable.addColumn(oColumn);
    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Nome postazione"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "NAME"),
      sortProperty: "NAME",
      filterProperty: "NAME",
      width: "200px"
    });
    oTable.addColumn(oColumn);
    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Ultimo stato"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "STATE"),
      sortProperty: "STATE",
      filterProperty: "STATE",
      width: "200px"
    });
    oTable.addColumn(oColumn);

    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Canale"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "CHANNEL"),
      sortProperty: "CHANNEL",
      filterProperty: "CHANNEL",
      width: "200px"
    });
    oTable.addColumn(oColumn);
	
    oModel = new sap.ui.model.xml.XMLModel();
	oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabWs()
	return oTable;
}

// Aggiorna la tabella Postazioni
function refreshTabWs() {
  $.UIbyID("WsTab").getModel().loadData(QService + dataCollect + "GetWsListQR");
}

// Function per creare la finestra di dialogo Postazioni
function openMachEdit(bEdit) {
  
  if (bEdit == undefined) bEdit = false;
  
//  Crea la finestra di dialogo  
	var oEdtDlg = new sap.ui.commons.Dialog({modal:  true,
                                          resizable: true,
										  id: "dlgAddMach",
                                          maxWidth:  "600px",
                                            Width:   "600px",
                                  showCloseButton: false});
    
	//oEdtDlg.setTitle("Aggiungi Postazione");

   // contenitore dati lista linee
	var oModel = new sap.ui.model.xml.XMLModel()
	oModel.loadData(QService + dataCollect + "getLinesSQ");

	// Crea la ComboBox per le divisioni
	var oCmbLine = new sap.ui.commons.ComboBox("cmbLine", {selectedKey : bEdit?fnGetCellValue("WsTab", "IDLINE"):""});
	oCmbLine.setModel(oModel);
	var oItemLine = new sap.ui.core.ListItem();
	oItemLine.bindProperty("text", "LINETXT");
	oItemLine.bindProperty("key", "IDLINE");	
	oCmbLine.bindItems("/Rowset/Row", oItemLine);

   // contenitore dati lista fornitori
	var oModel1 = new sap.ui.model.xml.XMLModel()
	oModel1.loadData(QService + dataCollect + "getVendorsSQ");	
	// Crea la ComboBox per i fornitori
	var oCmbVend = new sap.ui.commons.ComboBox("cmbVend", {selectedKey : bEdit?fnGetCellValue("WsTab", "VENDOR"):""});
	oCmbVend.setModel(oModel1);
	var oItemVend = new sap.ui.core.ListItem();
	oItemVend.bindProperty("text", "VENDTXT");
	oItemVend.bindProperty("key", "IDVEND");	
	oCmbVend.bindItems("/Rowset/Row", oItemVend);	

	if (bEdit) {
 // pulizia campo OrdID
	var lordId = fnGetCellValue("WsTab", "ORDID");
	if (Number(lordId) <= 0 ) lordId = 1;
	}
	
    var oLayout1 = new sap.ui.layout.form.GridLayout( {singleColumn: true});
    var oForm1 = new sap.ui.layout.form.Form({
        title: new sap.ui.core.Title({text: "Anagrafica Postazioni", icon: "/images/address.gif", tooltip: "Info Postazione"}),
        width: "98%",
        layout: oLayout1,
        formContainers: [
            new sap.ui.layout.form.FormContainer({
                formElements: [
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({text: "ID:", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                        fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"}),
                                                                    value: bEdit?fnGetCellValue("WsTab", "IDMACH"):"",
																	editable: bEdit?false:true,
																	id: "txtMachId"})
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({text: "Descrizione", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                        fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                                                    value: bEdit?fnGetCellValue("WsTab", "MACHTXT"):"",
																	id: "txtMachTxt"})
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({text: "Linea", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                        fields: oCmbLine
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({text: "Fornitore", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                        fields: oCmbVend
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({text: "Ordine", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                        fields: [new sap.ui.commons.Slider({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                                            value: bEdit?Number(lordId):"1",
															min: 1,
															max: 9,
															smallStepWidth: 1,
															stepLabels : true,
															totalUnits: 8,
															id: "sldOrd"})
                        ]
                    }),					
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({text: "ID Esterno", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                        fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                                                    value: bEdit?fnGetCellValue("WsTab", "EXID"):"",
																	id: "txtEXID"})
                        ]
                    }),
                    new sap.ui.layout.form.FormElement({
                        label: new sap.ui.commons.Label({text: "Note", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                        fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"}),
                                                                    value: bEdit?fnGetCellValue("WsTab", "NOTES"):"",
																	id: "txtNote"})
                        ]
                    })
					]
				})
			]
		});                    
                        
	oEdtDlg.addContent(oForm1);
	oEdtDlg.addButton(new sap.ui.commons.Button({text: "Salva", press:function(){if (bEdit) fnSaveMach(); else fnAddMach()}}));
	oEdtDlg.addButton(new sap.ui.commons.Button({text: "Annulla", press:function(){oEdtDlg.close(); oEdtDlg.destroy()}}));    
	oEdtDlg.open();
}


// Funzione per cancellare tutta la lista postazioni
function clearAllWs () {
	sap.ui.commons.MessageBox.show("Cancellare l'intera lista postazioni?",
			sap.ui.commons.MessageBox.Icon.WARNING,
			"Conferma eliminazione",
			[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO], 
			function(sResult) {
			  if (sResult == 'YES') {
			   var qexe = dataCollect+"ClearWsListQR" ;
               var ret = fnExeQuery(qexe, "Lista postazione eliminata correttamente","Errore in eliminazione postazioni", false); 
			   refreshTabWs();
			  }
			},
			sap.ui.commons.MessageBox.Action.YES);  

} 

// Funzione per registrare la postazione
function fnRegWs () {

	sap.ui.commons.MessageBox.show("Registrare la postazione corrente?",
			sap.ui.commons.MessageBox.Icon.WARNING,
			"Conferma registrazione",
			[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO], 
			function(sResult) {
			  if (sResult == 'YES') {
			   var qexe = dataCollect+"RegisterWsQR&Param.1=" + fnGetIP() + "&Param.2=VirtualHost";
               var ret = fnExeQuery(qexe, "Postazione registrata correttamente","Errore in registrazione postazione", false); 
			   refreshTabWs();
			   $.UIbyID("btnDelMach").setEnabled(false);
			  }
			},
			sap.ui.commons.MessageBox.Action.YES);  

}

// Funzione per cancellare una postazione
function fnDelWs () {
  var valId = fnGetCellValue("WsTab", "IDLINE");
  var valTxt = fnGetCellValue("WsTab", "IP");

	sap.ui.commons.MessageBox.show("Eliminare la postazione selezionata con IP/Nome:\n'"+valTxt+"'?",
			sap.ui.commons.MessageBox.Icon.WARNING,
			"Conferma eliminazione",
			[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO], 
			function(sResult) {
			  if (sResult == 'YES') {
			   var qexe = dataCollect+"ClearWsItemQR&Param.1=" + valTxt ;
               var ret = fnExeQuery(qexe, "Postazione eliminata correttamente","Errore in eliminazione postazione", false); 
			   refreshTabWs();
			   $.UIbyID("btnDelMach").setEnabled(false);
			  }
			},
			sap.ui.commons.MessageBox.Action.YES);  

} 

function fnGetIP () {
	var localIP = "";
	$.ajax({
			 "url": "/XMII/CM/QM_Measures/getIpAddr.jsp",
			 "dataType": "html",
			 "type": "GET",
			 "async": false,
			 "cache": false,
			 "success": function (data) {
				$("#info").html($.trim(data));
				localIP = $.trim(data);
			 }
		  });
	return localIP;
}

/***********************************************************************************************/
// Funzioni ed oggetti gli stati
/***********************************************************************************************/

// Funzione che crea la tabella stati e ritorna l'oggetto oTable
function createTabStates() {
   
   //Crea L'oggetto Tabella Postazioni
    var oTable = new sap.ui.table.Table(
    {
      title: "Stati Postazione",
      id: "StateTab",
      visibleRowCount: 8,
	  width: "98%",
      firstVisibleRow: 1,
      selectionMode: sap.ui.table.SelectionMode.Single,
      navigationMode: sap.ui.table.NavigationMode.Paginator,

      toolbar: new sap.ui.commons.Toolbar(
      {
        items: [
          new sap.ui.commons.Button(
          {
            text: "Pulisci stati",
            icon: icon16 + "blueprint--plus.png",
            press: function ()
            {
              clearStateWs();
            }
          }),
          new sap.ui.commons.Button(
          {
            text: "Aggiorna",
            icon: icon16 + "refresh.png",            
            enabled: true,
            press: function ()
            {
              refreshTabState(currentIP);
            }			
          })              
        ]
      })
    });

    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Data / ora"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "DATA"),
      sortProperty: "DATA",
      filterProperty: "DATA",
      width: "200px"
    });
    oTable.addColumn(oColumn);
    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Ultimo stato"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "STATE"),
      sortProperty: "STATE",
      filterProperty: "STATE",
      width: "200px"
    });
    oTable.addColumn(oColumn);

    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Canale"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "CHANNEL"),
      sortProperty: "CHANNEL",
      filterProperty: "CHANNEL",
      width: "200px"
    });
    oTable.addColumn(oColumn);

    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Caratteristica"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "INSPCHAR"),
      sortProperty: "INSPCHAR",
      filterProperty: "INSPCHAR",
      width: "200px"
    });
    oTable.addColumn(oColumn);	
    oModel = new sap.ui.model.xml.XMLModel();
	oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabState(fnGetIP());
	return oTable;
}

// Aggiorna la tabella stati postazione
function refreshTabState(lIP) {
  $.UIbyID("StateTab").getModel().loadData(QService + dataCollect + "GetWsCronoQR&Param.1="+lIP);
}

// pulisce la tabella stati postazione
function clearStateWs() {

	sap.ui.commons.MessageBox.show("Eliminare la lista stati per IP/Nome:\n'"+currentIP+"'?",
			sap.ui.commons.MessageBox.Icon.WARNING,
			"Conferma eliminazione",
			[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO], 
			function(sResult) {
			  if (sResult == 'YES') {
			   var qexe = dataCollect+"ClearWsInfoQR&Param.1=" + currentIP ;
               var ret = fnExeQuery(qexe, "Lista stati eliminata correttamente","Errore in eliminazione lista stati", false); 
			   refreshTabState(currentIP);
			  }
			},
			sap.ui.commons.MessageBox.Action.YES);  

}

/***********************************************************************************************/
// Funzioni ed oggetti per gli ordini postazione
/***********************************************************************************************/

// Functione che crea la tabella ordini e ritorna l'oggetto oTable
function createTabOrders() {
   
   //Crea L'oggetto Tabella Postazioni
    var oTable = new sap.ui.table.Table(
    {
      title: "Lista di lavoro ",
      id: "OrderTab",
      visibleRowCount: 8,
	  width: "98%",
      firstVisibleRow: 1,
      selectionMode: sap.ui.table.SelectionMode.Single,
      navigationMode: sap.ui.table.NavigationMode.Paginator,

      toolbar: new sap.ui.commons.Toolbar(
      {
        items: [
          new sap.ui.commons.Button(
          {
            text: "Pulisci Lista lavoro",
            icon: icon16 + "blueprint--plus.png",
            press: function ()
            {
              clearOrdersWs();
            }
          }),
          new sap.ui.commons.Button(
          {
            text: "Aggiorna",
            icon: icon16 + "refresh.png",            
            enabled: true,
            press: function ()
            {
              refreshTabOrders(currentIP);
            }			
          })              
        ]
      })
    });

    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Data / ora"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "DATA"),
      sortProperty: "DATA",
      filterProperty: "DATA",
      width: "200px"
    });
    oTable.addColumn(oColumn);
    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "N° Ordine"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "ORDINE"),
      sortProperty: "ORDINE",
      filterProperty: "ORDINE",
      width: "200px"
    });
    oTable.addColumn(oColumn);

    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Operazione"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "OPERAZIONE"),
      sortProperty: "OPERAZIONE",
      filterProperty: "OPERAZIONE",
      width: "200px"
    });
    oTable.addColumn(oColumn);

    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Lotto controllo"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "LOTTO"),
      sortProperty: "LOTTO",
      filterProperty: "LOTTO",
      width: "200px"
    });
    oTable.addColumn(oColumn);

    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Materiale"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "MATERIALE"),
      sortProperty: "MATERIALE",
      filterProperty: "MATERIALE",
      width: "200px"
    });
    oTable.addColumn(oColumn);

    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Descrizione Materiale"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "MATDESC"),
      sortProperty: "MATDESC",
      filterProperty: "MATDESC",
      width: "200px"
    });
    oTable.addColumn(oColumn);
	
    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Quantità"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "QTA"),
      sortProperty: "QTA",
      filterProperty: "QTA",
      width: "200px"
    });
    oTable.addColumn(oColumn);

	
    oModel = new sap.ui.model.xml.XMLModel();
	oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabOrders(fnGetIP());
	return oTable;
}

// Aggiorna la tabella ordini postazione
function refreshTabOrders(lIP) {
  $.UIbyID("OrderTab").getModel().loadData(QService + dataCollect + "GetOperValueListQR&Param.1="+lIP);
}

// pulisce la tabella ordini postazione
function clearOrdersWs() {
	sap.ui.commons.MessageBox.show("Eliminare la lista di lavoro per IP/Nome:\n'"+currentIP+"'?",
			sap.ui.commons.MessageBox.Icon.WARNING,
			"Conferma eliminazione",
			[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO], 
			function(sResult) {
			  if (sResult == 'YES') {
			   var qexe = dataCollect+"ClearWsInfoQR&Param.1=OPER_" + currentIP ;
               var ret = fnExeQuery(qexe, "Lista di lavoro eliminata correttamente","Errore in eliminazione lista di lavoro", false); 
			   refreshTabOrders(currentIP);
			  }
			},
			sap.ui.commons.MessageBox.Action.YES);  
}

/***********************************************************************************************/
// Funzioni ed oggetti per lista misure postazione
/***********************************************************************************************/

// Functione che crea la tabella misure e ritorna l'oggetto oTable
function createTabMis() {
   
   //Crea L'oggetto Tabella Postazioni
    var oTable = new sap.ui.table.Table(
    {
      title: "Lista misure",
      id: "MisTab",
      visibleRowCount: 8,
	  width: "98%",
      firstVisibleRow: 1,
      selectionMode: sap.ui.table.SelectionMode.Single,
      navigationMode: sap.ui.table.NavigationMode.Paginator,

      toolbar: new sap.ui.commons.Toolbar(
      {
        items: [
          new sap.ui.commons.Button(
          {
            text: "Pulisci Lista misure",
            icon: icon16 + "blueprint--plus.png",
            press: function ()
            {
              clearMisWs();
            }
          }),
          new sap.ui.commons.Button(
          {
            text: "Aggiorna",
            icon: icon16 + "refresh.png",            
            enabled: true,
            press: function ()
            {
              refreshTabMis(currentIP);
            }			
          })              
        ]
      })
    });
	
    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Progr."
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "ID"),
      sortProperty: "ID",
      filterProperty: "ID",
      width: "50px"
    });
    oTable.addColumn(oColumn);
	
    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Data / ora"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "DATA"),
      sortProperty: "DATA",
      filterProperty: "DATA",
      width: "200px"
    });
    oTable.addColumn(oColumn);
	
    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Misura"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "MISURA"),
      sortProperty: "MISURA",
      filterProperty: "MISURA",
      width: "200px"
    });
    oTable.addColumn(oColumn);

    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Canale"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "CANALE"),
      sortProperty: "CANALE",
      filterProperty: "CANALE",
      width: "200px"
    });
    oTable.addColumn(oColumn);

    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Caratteristica"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "INSPCHAR"),
      sortProperty: "INSPCHAR",
      filterProperty: "INSPCHAR",
      width: "200px"
    });
    oTable.addColumn(oColumn);

    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "Valutazione"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "EVALUATION"),
      sortProperty: "EVALUATION",
      filterProperty: "EVALUATION",
      width: "200px"
    });
    oTable.addColumn(oColumn);

    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "CODE"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "CODE"),
      sortProperty: "CODE",
      filterProperty: "CODE",
      width: "200px"
    });
    oTable.addColumn(oColumn);
	
    var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "CODE_GRP"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "CODE_GRP"),
      sortProperty: "CODE_GRP",
      filterProperty: "CODE_GRP",
      width: "200px"
    });
    oTable.addColumn(oColumn);

	var oColumn = new sap.ui.table.Column(
    {
      label: new sap.ui.commons.Label(
      {
        text: "REMARK"
      }),
      template: new sap.ui.commons.TextView().bindProperty("text", "REMARK"),
      sortProperty: "REMARK",
      filterProperty: "REMARK",
      width: "200px"
    });
    oTable.addColumn(oColumn);
	
    oModel = new sap.ui.model.xml.XMLModel();
	oTable.setModel(oModel);
    oTable.bindRows("/Rowset/Row");
    refreshTabMis(fnGetIP());
	return oTable;
}

// Aggiorna la tabella misure postazione
function refreshTabMis(lIP) {
  $.UIbyID("MisTab").getModel().loadData(QService + dataCollect + "GetMisValueListQR&Param.1="+lIP);
}

// pulisce la tabella misure postazione
function clearMisWs() {
	sap.ui.commons.MessageBox.show("Eliminare la lista misure per IP/Nome:\n'"+currentIP+"'?",
			sap.ui.commons.MessageBox.Icon.WARNING,
			"Conferma eliminazione",
			[sap.ui.commons.MessageBox.Action.YES, sap.ui.commons.MessageBox.Action.NO], 
			function(sResult) {
			  if (sResult == 'YES') {
			   var qexe = dataCollect+"ClearWsInfoQR&Param.1=MEAS_" + currentIP ;
               var ret = fnExeQuery(qexe, "Lista misure eliminata correttamente","Errore in eliminazione lista misure", false); 
			   refreshTabMis(currentIP);
			  }
			},
			sap.ui.commons.MessageBox.Action.YES); 
}